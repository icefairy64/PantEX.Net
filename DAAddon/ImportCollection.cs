﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Breezy.Iltal.Models;
using Breezy.PantEX.GUI;
using Breezy.Assistant;
using System.Collections.Generic;

namespace Breezy.PantEX.DeviantArt
{
    public class ImportCollection : ICollectionAction, IPreconfigurable
    {
        private Folder Source;
        private Collection Target;
        private IEnumerable<Folder> Collections;
        private int ChunkSize = 10;

        public async Task Preconfigure()
        {
            Collections = await Task.Run(() => DAStatic.Client.GetCollections().ToList());
        }

        public bool Configure(Collection collection)
        {
            var dialog = new SelectorDialog<Folder>(Collections, "Select a folder");
            Source = dialog.Ask();
            Target = collection;
            return Source != null;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            progressHandler(0);
            var feed = Source.Feed.Limit(20).Get().StateSaving();
            var list = new List<Deviation>();

            for (int i = 0; i < Source.Size.Value / ChunkSize; i++)
            {
                await Task.Run(() => list.AddRange(feed.Take(ChunkSize)));
                await Task.Delay(TimeSpan.FromSeconds(2));
                progressHandler((i + 1) * ChunkSize / (double)Source.Size.Value);
            }

            Target.AddImages(list.MapToEXImage());
            progressHandler(1);
        }

        public string Title => "Import DeviantArt collection contents";
    }
}

