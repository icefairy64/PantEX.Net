﻿using System;

namespace Breezy.PantEX.DeviantArt
{
    public class DeviantArtUndiscovered : ImageSourceFactory
    {
        public override void Configure()
        {
        }

        public override ImageSource Instance
        {
            get
            {
                var feed = DAStatic.Client.Browse.Undiscovered.Get();
                return new DeviantArtSource(feed);
            }
        }

        public override string Provider => "DeviantArt";
        public override string Title => "Undiscovered";
    }
}

