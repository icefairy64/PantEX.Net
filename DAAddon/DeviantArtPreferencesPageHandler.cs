﻿using System;
using System.Threading.Tasks;
using Breezy.PantEX.GUI;
using Breezy.PantEX.GUI.Preferences;
using Eto.Forms;
using Eto.Drawing;
using Breezy.Iltal;

namespace Breezy.PantEX.DeviantArt
{
    public class DeviantArtPreferencesPageHandler : AbstractPreferencesPageHandler
    {
        public DeviantArtPreferencesPageHandler()
        {
        }

        public override TabPage CreatePage()
        {
            var layout = new StackLayout { Spacing = 6, Padding = new Padding(8) };
            var page = new TabPage { Text = "DeviantArt", Content = layout };

            var loggedInAsLabel = new Label { Text = "Logged in", Visible = DAStatic.Client.IsLoggedIn };

            if (DAStatic.Client.IsLoggedIn)
            {
                DAStatic.Client.WhoAmI()
                    .ContinueWith(x => Application.Instance.InvokeSafe(() => loggedInAsLabel.Text = $"Logged in as {x.Result.Name}"));
            }

            var loginButton = new Button { Text = "Login", Visible = !DAStatic.Client.IsLoggedIn };
            loginButton.Command = new Command((s, e) =>
                {
                    var auth = new WebAuthorizer();
                    auth.Authorize(DAStatic.AppCode, DAStatic.RedirectUri, Scope.Browse | Scope.User)
                        .ContinueWith(async x => await DAStatic.Client.Login(x.Result)).Unwrap()
                        .ContinueWith(x =>
                            {
                                Console.WriteLine(x.Result);
                                if (x.Result)
                                {
                                    Application.Instance.InvokeSafe(() => 
                                        {
                                            loggedInAsLabel.Visible = true;
                                            loginButton.Visible = false;
                                        });
                                }
                            })
                        .ContinueWith(x => DAStatic.Client.WhoAmI()).Unwrap()
                        .ContinueWith(x => Application.Instance.InvokeSafe(() => loggedInAsLabel.Text =  $"Logged in as {x.Result.Name}"));
                });

            layout.Items.Add(new StackLayoutItem(loggedInAsLabel));
            layout.Items.Add(new StackLayoutItem(loginButton));

            return page;
        }
    }
}

