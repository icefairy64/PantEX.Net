﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Breezy.Iltal;
using Breezy.Assistant;
using Breezy.PantEX.Images;
using Breezy.Iltal.Models;

namespace Breezy.PantEX.DeviantArt
{
    public class DeviantArtSource : ImageSource
    {
        private FeedIterator<Deviation> Feed;
        private IEnumerable<Deviation> FeedEnumerable;
        private int PageSize = 20;

        internal DeviantArtSource(FeedIterator<Deviation> feed)
        {
            Feed = feed;
            FeedEnumerable = feed.StateSaving();
        }

        public DeviantArtSource(ImageSourceDescriptor des)
            : this(new FeedIterator<Deviation>(des.Descriptor))
        {
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            return FeedEnumerable
                .Take(PageSize)
                .MapToEXImage();
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            return await Task.FromResult(Load(page));
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                TypeName = typeof(DeviantArtSource).FullName,
                Descriptor = Feed.UrlTemplate
            };
        }

        public override string Title => "DeviantArt";
    }

    public static class DeviationExtension
    {
        public static IEnumerable<EXImage> MapToEXImage(this IEnumerable<Deviation> enumerable)
        {
            return enumerable
                .Where(x => x.Content?.Source.AbsolutePath != null)
                .Select(x => new HttpImage(x.Content.Source.ToString(), x.Thumbs.First().Source.ToString()));
        }
    }
}

