﻿using System;
using Breezy.PantEX.GUI;
using Breezy.Iltal.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Breezy.PantEX.DeviantArt
{
    public class DeviantArtCollectionSource : ImageSourceFactory, IPreconfigurable
    {
        private Folder Collection;
        private List<Folder> Collections;

        public DeviantArtCollectionSource()
        {
        }

        public async Task Preconfigure()
        {
            Collections = new List<Folder>(DAStatic.Client.GetCollections());
        }

        public override void Configure()
        {
            var dialog = new SelectorDialog<Folder>(Collections, "Select a folder");
            Collection = dialog.Ask();
        }

        public override string Provider
        {
            get
            {
                return "DeviantArt";
            }
        }

        public override string Title
        {
            get
            {
                return "Collection";
            }
        }

        public override ImageSource Instance
        {
            get
            {
                return new DeviantArtSource(Collection.Feed.Mature(true).Get());
            }
        }
    }
}

