﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using Breezy.Iltal;
using Breezy.PantEX.GUI;
using Eto.Forms;
using System.Threading.Tasks;
using NLog;
using System.Net.Http;

namespace Breezy.PantEX.DeviantArt
{
    [Configurable("DeviantArt")]
    public static class DAStatic
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        internal static readonly uint AppCode = 3521;
        internal static readonly string AppSecret = "e7db2e8d74559c922da6d70f7d997f88";
        internal static readonly string RedirectUri = "http://main-icefairy64.rhcloud.com/pantex";

        public static DeviantAPI Client { get; private set; }

        static DAStatic()
        {
            Client = new DeviantAPI(AppCode, AppSecret);
        }

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            var key = dict["Key"] as string;
            if (!String.IsNullOrEmpty(key))
            {
                var parts = key.Split('\n');
                Client.Token = parts[0];
                Client.RefreshToken = parts[1];

                Client.Init()
                    .ContinueWith(x => Logger.Info($"DeviantArt authorization result: {x.Result}"));
            }
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {
            dict["Key"] = Client.Token + '\n' + Client.RefreshToken;
        }
    }
}

