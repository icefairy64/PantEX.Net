﻿using System;
using Breezy.Iltal;
using System.Threading.Tasks;
using Breezy.PantEX.GUI;
using Breezy.Mars;

namespace Breezy.PantEX.DeviantArt
{
    public class WebAuthorizer : IAuthorizer
    {
        private static readonly string EntryPoint = "https://www.deviantart.com/oauth2/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope={2}&state={3}";

        public async Task<AuthResponse> Authorize(uint clientID, string redirectUri, Scope scope)
        {
            var url = String.Format(EntryPoint, clientID, redirectUri, scope.ToScopeString(), (clientID * 2).ToString());

            var prop = new Property<Uri>();
            var closed = new Property<bool>(false);

            var browser = new WebBrowserForm(new Uri(url), "Authorize on DeviantArt", x => x.AbsoluteUri.StartsWith(redirectUri));

            browser.NavigatedToMatchedUrl += (sender, e) => prop.Value = e.Uri;
            browser.Closed += (sender, e) => closed.Value = true;

            browser.Show();

            while (!closed.Value)
                await Task.Delay(200);

            return new AuthResponse(prop.Value, redirectUri);
        }
    }
}

