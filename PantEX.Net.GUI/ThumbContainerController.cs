﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Eto.Forms;
using Eto.Drawing;
using Breezy.PantEX;
using System.Threading.Tasks;
using NLog;

namespace Breezy.PantEX.GUI
{
    public class ThumbContainerController : IDisposable
    {
        public int Padding = 6;
        public int Gap = 6;
        public int LoadThreshold = 100;
        public readonly Property<double> ThumbHeight;
        public readonly Property<int> ThumbsInRow;
        public event EventHandler LoadRequested;

        private int X, Y, ThumbNo;

        private readonly PixelLayout Layout;
        private readonly Scrollable ScrollBox;
        private System.Timers.Timer Timer;
        private readonly List<ThumbBox> Row;
        private readonly Dictionary<ThumbBox, Size> Sizes;
        private readonly List<ThumbBox> Thumbs;

        private readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool UpdatesEnabled
        {
            get { return Timer.Enabled; }
            set { Timer.Enabled = value; }
        }

        public ThumbContainerController(PixelLayout layout, Scrollable scrollBox, double height = 100)
        {
            Layout = layout;
            ScrollBox = scrollBox;
            ScrollBox.SizeChanged += HandleResize;
            Row = new List<ThumbBox>();
            Sizes = new Dictionary<ThumbBox, Size>();
            ThumbHeight = new Property<double>(height);
            ThumbHeight.ValueChanged += (oldValue, newValue) => HandleResize(this, null);
            ThumbsInRow = new Property<int>();
            ThumbsInRow.ValueChanged += (oldValue, newValue) => HandleResize(this, null);
            Thumbs = new List<ThumbBox>();
            Timer = new System.Timers.Timer(30);
            Timer.Elapsed += (sender, e) => HandleTimer();
            Timer.Enabled = true;
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

        private void HandleTimerOnMain()
        {
            try
            {
                var dy = ScrollBox.ScrollPosition.Y - ScrollBox.ScrollSize.Height + ScrollBox.VisibleRect.Height;
                if (dy > -LoadThreshold && LoadRequested != null)
                    LoadRequested(this, null);
            }
            catch (Exception e) {
                Logger.Error("An error has occurred while handling timer tick; disabling timer");
                Logger.Error(e);
                Timer.Enabled = false;
            }
        }

        private void HandleTimer()
        {
            if (this.IsOnUIThread())
                HandleTimerOnMain();
            else
                Application.Instance.Invoke(() => HandleTimerOnMain());
        }

        private void HandleResize(object sender, EventArgs args)
        {
            Row.Clear();
            X = 0;
            Y = 0;
            ThumbNo = 0;
            Layout.SuspendLayout();
            foreach (var c in Thumbs)
                HandleThumb(c);
            foreach (var c in Thumbs)
                c.Size = Sizes[c];
            Layout.ResumeLayout();
        }

        private void HandleThumb(ThumbBox img, bool setSize = false)
        {
            Layout.Move(img, X + Padding + (X > 0 ? Gap : 0), Y + Padding);
            int w = (int)(img.Image.Width * ThumbHeight.Value / img.Image.Height);
            Sizes[img] = new Size(w, (int)ThumbHeight.Value);
            if (setSize)
                img.Size = Sizes[img];
            X += w + (X > 0 ? Gap : 0);
            Row.Add(img);

            if (X > Layout.ClientSize.Width - Padding || ++ThumbNo >= ThumbsInRow)
            {
                double iw = ScrollBox.VisibleRect.Width - 2 * Padding - (Row.Count - 1) * Gap;
                double cw = Row.Aggregate(0, (a, v) => a + Sizes[v].Width);
                double k = iw / cw;
                double h = ThumbHeight.Value * k;
                X = 0;
                foreach (ThumbBox v in Row)
                { 
                    int z = (int)(v.Image.Width * h / v.Image.Height);
                    Sizes[v] = new Size(z, (int)h);
                    Layout.Move(v, X + Padding + (X > 0 ? Gap : 0), Y + Padding);
                    if (setSize)
                        v.Size = Sizes[v];
                    X += z + (X > 0 ? Gap : 0);
                }
                Row.Clear();
                X = 0;
                Y += (int)h + Gap;
                ThumbNo = 0;
            }
        }

        public async Task<ThumbBox> Add(EXImage img, Image thumb = null, bool toFront = false)
        {
            var box = thumb == null ?
                await Application.Instance.InvokeAsync(() => new ThumbBox(img)) :
                await Application.Instance.InvokeAsync(() => new ThumbBox(img, thumb));
            if (thumb == null)
                await box.LoadThumbAsync();
            Layout.Add(box, X + Padding, Y + Padding);
            if (!toFront)
            {
                Thumbs.Add(box);
                HandleThumb(box, true);
            }
            else
            {
                Thumbs.Insert(0, box);
                HandleResize(this, null);
            }
            return box;
        }

        public void Remove(IEnumerable<EXImage> images, bool invalidate = true)
        {
            Layout.Remove(Thumbs.Where(x => images.Contains(x.InnerImage)));
            Thumbs.RemoveAll(x => images.Contains(x.InnerImage));
            if (invalidate)
                HandleResize(this, null);
        }

        public void RemoveAll(bool invalidate = true)
        {
            Layout.RemoveAll();
            Thumbs.Clear();
            if (invalidate)
                HandleResize(this, null);
        }

        public void Redraw()
        {
            HandleResize(this, null);
        }

        public void Clear()
        {
            Row.Clear();
            Layout.RemoveAll();
        }
    }
}

