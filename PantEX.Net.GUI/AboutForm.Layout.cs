﻿using System;
using Eto.Forms;
using Eto.Drawing;
using System.Diagnostics;

namespace Breezy.PantEX.GUI
{
    public partial class AboutForm : Form
    {
        private static readonly string HomeURL = "https://gitlab.com/icefairy64/PantEX.Net";

        private void InitializeLayout()
        {
            MinimumSize = new Size(200, 0);
            Title = "About PantEX";

            var layout = new StackLayout() { Padding = new Padding(8), Spacing = 6, MinimumSize = this.MinimumSize, HorizontalContentAlignment = HorizontalAlignment.Center  };
            layout.Items.Add(new StackLayoutItem(new Label() { Text = "PantEX 0.4", Font = Fonts.Sans(22, FontStyle.Bold) }));
            layout.Items.Add(new StackLayoutItem(new Label() { Text = "Create and organize various image collections", TextAlignment = TextAlignment.Center }));
            layout.Items.Add(new StackLayoutItem(new Label() { Text = "2016, Breezy", TextAlignment = TextAlignment.Center }));
            layout.Items.Add(new StackLayoutItem(new LinkButton() { Text = HomeURL, Command = new Command((s, e) => Process.Start("xdg-open", HomeURL)) }));

            Content = layout;
        }
    }
}

