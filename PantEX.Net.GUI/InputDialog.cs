﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI
{
    public class InputDialog : Dialog
    {
        private TextBox InputBox;

        public string Data
        {
            get { return InputBox.Text; }
        }

        public InputDialog(string query = "Data", string defaultValue = "")
        {
            Title = "Input";

            InputBox = new TextBox { Text = defaultValue };

            DefaultButton = new Button { Text = "OK" };
            DefaultButton.Click += (sender, e) => Close();

            AbortButton = new Button { Text = "C&ancel" };
            AbortButton.Click += (sender, e) => Close();

            var input = new StackLayout
                {
                    Items =
                    {
                        new StackLayoutItem(new Label() { Text = $"{query}: " }),
                        new StackLayoutItem(InputBox)
                    },
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Bottom
                };

            var buttons = new StackLayout
                {
                    Orientation = Orientation.Horizontal,
                    Spacing = 5,
                    Items = { DefaultButton, AbortButton }
                };

            Content = new StackLayout
                {
                    Items =
                    {
                        new StackLayoutItem(input),
                        new StackLayoutItem(buttons, HorizontalAlignment.Right)
                    },
                    Orientation = Orientation.Horizontal,
                    Spacing = 8,
                    Padding = new Padding(8)
                };
        }

        public string Ask()
        {
            ShowModal();
            return Data;
        }

        public async Task<string> AskAsync()
        {
            await ShowModalAsync();
            return Data;
        }
    }
}

