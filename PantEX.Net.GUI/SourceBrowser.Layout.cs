﻿using System;
using System.Linq;
using Eto;
using Eto.Forms;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    public partial class SourceBrowser : Form
    {
        private PixelLayout Container;
        private Scrollable ScrollBox;
        private Button FetchButton;
        private DropDown CollectionSelector;

        private void InitializeLayout()
        {
            Title = "Source Browser";

            Size = new Size(DefaultWindowW.Value, DefaultWindowH.Value);

            if (DefaultWindowX.HasValue)
                Location = new Point(DefaultWindowX.Value, Location.Y);

            if (DefaultWindowY.HasValue)
                Location = new Point(Location.X, DefaultWindowY.Value);

            Container = new PixelLayout();

            ScrollBox = new Scrollable { Content = Container };

            Controller = new ThumbContainerController(Container, ScrollBox);

            Controller.ThumbHeight.Value = DefaultThumbSize.Value;
            Controller.ThumbsInRow.Value = DefaultThumbCount.Value;

            Closing += (sender, e) =>
                {
                    DefaultWindowX = Location.X;
                    DefaultWindowY = Location.Y;
                    DefaultWindowW = Width;
                    DefaultWindowH = Height;
                    DefaultThumbSize = (int)Controller.ThumbHeight;
                    DefaultThumbCount = Controller.ThumbsInRow;
                };

            var thumbSizeBox = new NumericStepper
            {
                MinValue = 100,
                Value = Controller.ThumbHeight,
                Increment = 10,
                MaximumDecimalPlaces = 4
            };
            thumbSizeBox.ValueChanged += (sender, e) => Controller.ThumbHeight.Value = thumbSizeBox.Value;

            var fetchMenu = new ContextMenu();

            fetchMenu.Items.Add(new ButtonMenuItem
            {
                Command = new Command(Fetch),
                Text = "One page"
            });

            fetchMenu.Items.Add(new ButtonMenuItem
            {
                Command = new Command((s, a) => FetchWhile(x => x.All(z => !CurrentCollection.Images.Contains(z)))),
                Text = "Until known"
            });

            fetchMenu.Items.Add(new ButtonMenuItem
            {
                Command = new Command((s, a) => FetchWhile(x => x.Any(z => CurrentCollection.Images.Contains(z)))),
                Text = "While known"
            });

            FetchButton = new Button { Text = "Fetch", Command = new Command((s, a) => fetchMenu.Show(FetchButton)) };
            FetchButton.MouseUp += (sender, e) => 
                {
                    if (e.Buttons.HasFlag(MouseButtons.Alternate))
                    {
                        new InputDialog("Page count", "1")
                            .AskAsync()
                            .ContinueWith(x => ((Action)(() => Fetch(int.Parse(x.Result)))).InvokeOnUI());
                    }
                };
            
            CollectionSelector = new DropDown();
            CollectionSelector.HookTo(CollectionManager.Collections);

            CollectionSelector.SelectedValueChanged += (sender, e) => ChangeCollection((CollectionSelector.SelectedValue as ValueHoldingItem<Collection>).Value);
            Controller.LoadRequested += HandleLoadRequest;

            var des = Source.CreateDescriptor();
            var favButton = new Button 
                { 
                    Text = "Add to favorites", 
                    Enabled = !FavoriteSources.Descriptors.Any(x => x.TypeName.Equals(Source.GetType().FullName) && x.Descriptor.Equals(des.Descriptor)) 
                };
            
            favButton.Click += (sender, e) => 
                {
                    new InputDialog("Source title")
                        .AskAsync()
                        .ContinueWith(x => 
                            {
                                des.Title = x.Result;
                                Application.Instance.AsyncInvoke(() => 
                                    {
                                        FavoriteSources.Add(des);
                                        favButton.Enabled = false;
                                    });
                            });
                };

            CollectionSelector.SelectedIndex = 0;

            var bottomPanel = new StackLayout() { Spacing = 6, Orientation = Orientation.Horizontal, VerticalContentAlignment = VerticalAlignment.Center };

            bottomPanel.Items.Add(new StackLayoutItem(new Label { Text = "Thumbnail size:" }));
            bottomPanel.Items.Add(new StackLayoutItem(thumbSizeBox));
            bottomPanel.Items.Add(null);
            bottomPanel.Items.Add(new StackLayoutItem(CollectionSelector));
            bottomPanel.Items.Add(new StackLayoutItem(favButton));
            bottomPanel.Items.Add(new StackLayoutItem(FetchButton));

            var layout = new StackLayout { Spacing = 6, Padding = new Padding(8), HorizontalContentAlignment = HorizontalAlignment.Stretch };
            layout.Items.Add(new StackLayoutItem(ScrollBox, true));
            layout.Items.Add(new StackLayoutItem(bottomPanel));

            Content = layout;
        }
    }
}

