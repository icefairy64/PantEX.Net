﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Breezy.PantEX.GUI
{
    public delegate void ValueChange<T>(T oldValue, T newValue);

    public class Property<T> : IObservable<T>
    {
        public event ValueChange<T> ValueChanged;

        private readonly List<IObserver<T>> Watchers;
        private T FValue;

        public T Value
        {
            get { return FValue; }
            set
            {
                var v = FValue;
                FValue = value;
                ValueChanged?.Invoke(v, value);
                Watchers.ForEach(x => x.OnNext(value));
            }
        }

        public Property()
        {
            Watchers = new List<IObserver<T>>();
        }

        public Property(T value)
            : this()
        {
            this.FValue = value;
        }

        internal void Unsubscribe(IObserver<T> observer)
        {
            Watchers.Remove(observer);
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            Watchers.Add(observer);
            return new PropertyUnsubscriber<T>(this, observer);
        }

        public void LinkTo(Property<T> property)
        {
            property.ValueChanged += (oldValue, newValue) => Value = newValue;
        }

        public static implicit operator T(Property<T> val)
        {
            return val.Value;
        }
    }

    internal class PropertyUnsubscriber<T> : IDisposable
    {
        private Property<T> Owner;
        private IObserver<T> Observer;

        public PropertyUnsubscriber(Property<T> owner, IObserver<T> observer)
        {
            Owner = owner;
            Observer = observer;
        }

        public void Dispose()
        {
            Owner.Unsubscribe(Observer);
        }
    }
}

