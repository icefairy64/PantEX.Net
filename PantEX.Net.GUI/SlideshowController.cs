﻿using System;
using System.Timers;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI
{
    [Configurable("SlideshowController")]
    public static class SlideshowController
    {
        private static Timer Timer;

        public static bool Enabled
        {
            get { return Timer.Enabled; }
            set { Timer.Enabled = value; }
        }

        public static double Interval
        {
            get { return Timer.Interval; }
            set { Timer.Interval = value; }
        }

        public static bool Randomized { get; set; }

        static SlideshowController()
        {
            Timer = new Timer(5000);
            Randomized = false;

            Timer.Elapsed += (sender, e) =>
                {
                    if (Randomized)
                        ImageViewer.Cursor.Random();
                    else
                        ImageViewer.Cursor.Forward();
                };
        }

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            Interval = (double)dict["Interval"];
            Randomized = dict.ContainsKey("Randomized") ? (bool)dict["Randomized"] : Randomized;
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            dict["Interval"] = Interval;
            dict["Randomized"] = Randomized;
        }
    }
}

