﻿using System;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    public class ThumbBox : Drawable
    {
        private Image FImage;

        public readonly EXImage InnerImage;
        public readonly Property<bool> Selected;
        public readonly Property<Color> BorderColor;

        public Image Image 
        { 
            get { return FImage; }
            set
            {
                FImage = value;
                Invalidate();
            }
        }

        public ThumbBox(EXImage img)
            : base()
        {
            InnerImage = img;
            Selected = new Property<bool>(false);
            Selected.ValueChanged += (oldValue, newValue) => Invalidate();
            BorderColor = new Property<Color>(Color.FromRgb(0x40a0ff));
            BorderColor.ValueChanged += (oldValue, newValue) => Invalidate();
        }

        public ThumbBox(EXImage img, Image thumb)
            : this(img)
        {
            Image = thumb;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            int x = 0;//(int)e.ClipRectangle.Left;
            int y = 0;//(int)e.ClipRectangle.Top;
            int w = Size.Width;
            int h = Size.Height;
            e.Graphics.DrawImage(FImage, x, y, w, h);
            if (Selected.Value)
            {
                e.Graphics.FillRectangle(BorderColor.Value, x, y, w, 4);
                e.Graphics.FillRectangle(BorderColor.Value, x, y, 4, h);
                e.Graphics.FillRectangle(BorderColor.Value, x, y + h - 4, w, 4);
                e.Graphics.FillRectangle(BorderColor.Value, x + w - 4, y, 4, h);
                e.Graphics.FillPolygon(BorderColor.Value, new PointF(x + w - 32, y), new PointF(x + w, y), new PointF(x + w, y + 32));
            }
        }

        public void LoadThumb()
        {
            Image = ImageFormatHelper.LoadImageAsync(InnerImage.Thumb).Result;
        }

        public async Task LoadThumbAsync()
        {
            Image = await ImageFormatHelper.LoadImageAsync(InnerImage.Thumb);
        }
    }
}

