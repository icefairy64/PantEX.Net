﻿using System;
using Breezy.Assistant;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Breezy.PantEX.GUI
{
    [Configurable("FavoriteSources")]
    public static class FavoriteSources
    {
        public static ObservableCollection<ImageSourceDescriptor> Descriptors = new ObservableCollection<ImageSourceDescriptor>();

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            var cont = dict["Descriptors"] as JContainer;
            Descriptors = new ObservableCollection<ImageSourceDescriptor>(cont.Cast<JObject>().Select(x => (ImageSourceDescriptor)x));
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {
            dict["Descriptors"] = Descriptors as IEnumerable<ImageSourceDescriptor>;
        }

        public static void Add(ImageSourceDescriptor des)
        {
            Descriptors.Add(des);
        }

        public static void Remove(ImageSourceDescriptor des)
        {
            Descriptors.Remove(des);
        }
    }
}

