﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.Threading;
using NLog;
using Eto.Forms;
using System.Linq;

namespace Breezy.PantEX.GUI
{
    [Configurable("AutoBackup")]
    public static class AutoBackup
    {
        private static long SavePeriod;
        private static Thread BackupThread;
        private static bool ThreadRunning;
        private static string FileSuffix;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static readonly string BackupDescriptorExtension = ".backup-descriptor";

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            SavePeriod = dict.ContainsKey("SavePeriod") ? (long)dict["SavePeriod"] : 30;
            FileSuffix = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            ThreadRunning = true;
            BackupThread = new Thread(Backup);
            BackupThread.Start();
            Application.Instance.Terminating += (sender, e) =>
            {
                ThreadRunning = false;
                BackupThread.Interrupt();
            };
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {
            dict["SavePeriod"] = SavePeriod;
        }

        private static void Backup()
        {
            while (ThreadRunning)
            {
                try
                {
                    Logger.Debug("Starting automatic collection backup...");

                    var list = new List<Collection>(CollectionManager.Collections.Where(c => c.Modifications.Any()));
                    foreach (var col in list)
                    {
                        Logger.Debug($"Backing up collection '{col.Title}'");
                        var backup = FileManager.GetBackupFile(col.Title + $".auto-backup.{FileSuffix}.json");
                        using (var stream = backup.Create())
                            Collection.DumpModifications(col, stream);
                    }

                    var descriptor = FileManager.GetBackupFile($"{FileSuffix}{BackupDescriptorExtension}");
                    using (var writer = descriptor.CreateText())
                    {
                        foreach (var col in list)
                            writer.WriteLine(col.Title + $".auto-backup.{FileSuffix}.json");
                    }

                    Logger.Debug("Done");

                    Thread.Sleep(TimeSpan.FromSeconds(SavePeriod));
                }
                catch (ThreadInterruptedException e)
                {
                    Logger.Debug("AutoBackup thread interrupted", e);
                }
                catch (Exception e)
                {
                    Logger.Error("Error while performing automatic backup");
                    Logger.Error($"Exception: {e.GetType().Name} - {e.Message}");
                }
            }
        }
    }
}
