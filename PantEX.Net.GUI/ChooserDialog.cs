﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eto.Forms;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    public class ChooserDialog<T> : Dialog
    {
        private DropDown Selector;

        public T ChosenValue
        {
            get { return ((ValueHoldingItem<T>)Selector.Items[Selector.SelectedIndex]).Value; }
        }

        public T Ask()
        {
            Selector.SelectedIndex = 0;
            ShowModal();
            return ChosenValue;
        }

        public ChooserDialog(string title, string query, IEnumerable<T> items)
        {
            Title = title;

            Selector = new DropDown();
            Selector.Items.AddRange(items.Select(x => new ValueHoldingItem<T>(x)));

            DefaultButton = new Button { Text = "OK" };
            DefaultButton.Click += (sender, e) => Close();

            AbortButton = new Button { Text = "C&ancel" };
            AbortButton.Click += (sender, e) => Close();

            var input = new StackLayout
                {
                    Items =
                    {
                        new StackLayoutItem(new Label() { Text = $"{query}: "}),
                        new StackLayoutItem(Selector)
                    },
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Bottom
                };

            var buttons = new StackLayout
                {
                    Orientation = Orientation.Horizontal,
                    Spacing = 5,
                    Items = { DefaultButton, AbortButton }
                };

            Content = new StackLayout
                {
                    Items =
                    {
                        new StackLayoutItem(input),
                        new StackLayoutItem(buttons, HorizontalAlignment.Right)
                    },
                    Orientation = Orientation.Horizontal,
                    Spacing = 8,
                    Padding = new Padding(8)
                };
        }
    }
}

