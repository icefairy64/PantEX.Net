﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI
{
    [Named("Save (obsolete)")]
    public class SaveExporter : ICollectionExporter
    {
        private ICollectionExporter Exporter;

        public string Title
        {
            get { return "Save"; }
        }

        public void Configure()
        {
            throw new NotImplementedException();
        }

        public void Configure(CollectionDescriptor des)
        {
            var type = Plugin.ProvidedTypes[typeof(ICollectionExporter)].Where(x => x.FullName.Equals(des.ImporterTypeName)).First();
            Exporter = Plugin.CreateInstance<ICollectionExporter>(type);
            Exporter.Configure(des);
        }

        public async Task Export(Collection collection, ProgressHandler progHandler)
        {
            await Exporter.Export(collection, progHandler);
        }
    }
}

