﻿using System;
using System.Linq;
using Eto.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI
{
    public partial class SelectorDialog<T> : Dialog
    {
        private bool WasCancelled = false;

        public IReadOnlyList<T> Items { get; private set; }

        public T SelectedItem
        {
            get
            {
                if (List.SelectedIndex < 0)
                    return default(T);
                return (List.Items[List.SelectedIndex] as ValueHoldingItem<T>).Value;
            }
        }

        public SelectorDialog(IEnumerable<T> items, string title)
            : base()
        {
            Items = new List<T>(items);
            InitializeLayout(title);
        }

        internal SelectorDialog(IEnumerable<ValueHoldingItem<T>> items, string title)
            : base()
        {
            Items = new List<T>(items.Select(x => x.Value));
            InitializeLayout(title, items);
        }

        public T Ask()
        {
            ShowModal();
            return WasCancelled ? default(T) : SelectedItem;
        }

        public async Task<T> AskAsync()
        {
            await ShowModalAsync();
            return WasCancelled ? default(T) : SelectedItem;
        }
    }
}

