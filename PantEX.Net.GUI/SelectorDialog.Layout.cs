﻿using System;
using Eto.Forms;
using System.Linq;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI
{
    public partial class SelectorDialog<T> : Dialog
	{
        private ListBox List;

        private void InitializeLayout(string title, IEnumerable<ValueHoldingItem<T>> preparedItems = null)
        {
            List = new ListBox();
            List.Items.AddRange(preparedItems ?? Items.Select(x => new ValueHoldingItem<T>(x)));

            var cancelButton = new Button { Text = "Cancel", Command = new Command((s, e) => 
                {
                    WasCancelled = true;
                    Close(); 
                }) };
            var okButton = new Button { Text = "OK", Command = new Command((s, e) => Close()) };

            var buttonSection = new StackLayout
                {
                    Items =
                        {
                            new StackLayoutItem(okButton),
                            new StackLayoutItem(cancelButton)
                        },
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Stretch,
                    Spacing = 8
                };

            Title = title;
            Width = 400;
            Height = 500;

            Content = new StackLayout
                {
                    Items = 
                        {
                            new StackLayoutItem(List, true),
                            new StackLayoutItem(buttonSection)
                        },
                    Orientation = Orientation.Vertical,
                    HorizontalContentAlignment = HorizontalAlignment.Stretch,
                    Spacing = 8,
                    Padding = 8
                };
        }
	}
}

