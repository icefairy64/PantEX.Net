﻿using System;
using System.Linq;
using System.Reflection;
using Breezy.Assistant.Extensions;
using Eto.Forms;
using Eto.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI
{
	public partial class CollectionManager : Form
	{
        private ListBox CollectionList;
        private ThumbContainerController Controller;
        private PixelLayout ThumbContainer;
        private Scrollable ScrollBox;
        private ProgressBar Progress;

        private List<MenuItem> FilterItems;

        private void InitializeLayout() 
        {
            Size = new Size(DefaultWindowH.Value, DefaultWindowH.Value);

            if (DefaultWindowX.HasValue)
                Location = new Point(DefaultWindowX.Value, Location.Y);

            if (DefaultWindowY.HasValue)
                Location = new Point(Location.X, DefaultWindowY.Value);

            CollectionList = new ListBox();
            ThumbContainer = new PixelLayout();
            ScrollBox = new Scrollable { Content = ThumbContainer };
            Controller = new ThumbContainerController(ThumbContainer, ScrollBox, 150);
            Progress = new ProgressBar { Width = 100 };

            Controller.ThumbHeight.Value = DefaultThumbSize.Value;
            Controller.ThumbsInRow.Value = DefaultThumbCount.Value;

            Closing += (sender, e) =>
                {
                    DefaultWindowX = Location.X;
                    DefaultWindowY = Location.Y;
                    DefaultWindowW = Width;
                    DefaultWindowH = Height;
                    DefaultThumbSize = (int)Controller.ThumbHeight;
                    DefaultThumbCount = Controller.ThumbsInRow;
                };

            var splitter = new Splitter { Position = 200 };
            splitter.Panel1 = new Panel { Content = CollectionList, Padding = new Padding(8, 8, 2, 8) };
            splitter.Panel2 = new Panel { Content = ScrollBox, Padding = new Padding(2, 8, 8, 8) };

            // Importers

            var importers = Plugin.ProvidedTypes[typeof(ICollectionImporter)];
            var importMenu = new ContextMenu(importers.Select(x => new ButtonMenuItem
                { 
                    Text = x.GetCustomAttribute<NamedAttribute>()?.Name ?? x.FullName, 
                    Command = new Command((sender, e) =>
                        {
                            var instance = Plugin.CreateInstance<ICollectionImporter>(x);
                            instance.Configure();
                            var task = new CollectionImportTask(instance);
                            task.ProgressChanged += (s, a) => Progress.Value = (int)(a.Progress * 100);
                            task.ResultProduced += (s, a) => Collections.Add(a.Result);
                            task.Finished += (s, a) => Progress.Value = 0;
                            TaskEX.Schedule(task);
                        })
                }));
            var importButton = new Button { Text = "Import..." };
            importButton.Command = new Command((sender, e) => importMenu.Show(importButton));

            // Filters

            FilterItems = new List<MenuItem>();

            var filtersMenu = new ContextMenu();

            filtersMenu.Items.Add(new ButtonMenuItem(new Command((sender, e) =>
                {
                    var res = new InputDialog("Tag").Ask();
                    AddFilter(filtersMenu, $"Tag: {res}", x => x.Tags.Any(t => t.Title.Equals(res)));
                    ForceReload();
                })) { Text = "Add tag filter..." });

            filtersMenu.Items.Add(new ButtonMenuItem(new Command((sender, e) =>
                {
                    var res = new SelectorDialog<Tag>(CurrentCollection.Tags, "Select a tag").Ask();
                    if (res == null)
                        return;
                    AddFilter(filtersMenu, $"Tag: {res}", x => x.Tags.Contains(res));
                    ForceReload();
                })) { Text = "Add tag filter (from collection)..." });

            filtersMenu.Items.Add(new ButtonMenuItem(new Command((sender, e) =>
                {
                    var tags = Selection.Select(x => (IEnumerable<Tag>)x.Tags)
                        .Aggregate((a, x) => x.Where(z => a.Contains(z)));
                    
                    var res = new SelectorDialog<Tag>(tags, "Select a tag").Ask();
                    if (res == null)
                        return;
                    AddFilter(filtersMenu, $"Tag: {res}", x => x.Tags.Contains(res));
                    ForceReload();
                })) { Text = "Add tag filter (from selection)..." });

            filtersMenu.Items.Add(new ButtonMenuItem(new Command((sender, e) =>
                {
                    var res = new InputDialog("Tag").Ask();
                    AddFilter(filtersMenu, $"Tag exclude: {res}", x => !x.Tags.Any(t => t.Title.Equals(res)));
                    ForceReload();
                })) { Text = "Add tag exclusion filter..." });

            filtersMenu.Items.Add(new ButtonMenuItem(new Command((sender, e) =>
                {
                    Filters.Clear();
                    foreach (var fi in FilterItems)
                        filtersMenu.Items.Remove(fi);
                    ForceReload();
                })) { Text = "Clear" });

            filtersMenu.Items.AddSeparator();

            AddFilter(filtersMenu, "With tags", x => x.Tags.Count > 0, true, false);

            var filtersButton = new Button { Text = "Filters" };
            filtersButton.Command = new Command((sender, e) => filtersMenu.Show(filtersButton));

            // Exporters

            var exporters = Plugin.ProvidedTypes[typeof(ICollectionExporter)];
            var exportMenu = new ContextMenu(exporters.Select(x => new ButtonMenuItem
                { 
                    Text = x.GetExtensionTitle(), 
                    Command = new Command((sender, e) =>
                        {
                            var instance = Plugin.CreateInstance<ICollectionExporter>(x);
                            instance.Configure();

                            var backupFile = FileManager.GetBackupFile(CurrentCollection.Title + ".pre-export.json");

                            using (var stream = backupFile.Create())
                                Collection.DumpModifications(CurrentCollection, stream);
                                
                            var task = new CollectionExportTask(instance, CurrentCollection);
                            task.ProgressChanged += (s, a) => Progress.Value = (int)(a.Progress * 100);
                            task.Finished += (s, a) => 
                                {
                                    Progress.Value = 0;
                                    backupFile.Delete();
                                };

                            TaskEX.Schedule(task);
                        })
                }));

            var saveItem = new ButtonMenuItem
                { 
                    Text = "Update", 
                    Command = new Command((sender, e) => 
                        {
                            var backupFile = FileManager.GetBackupFile(CurrentCollection.Title + ".pre-update.json");

                            using (var stream = backupFile.Create())
                                Collection.DumpModifications(CurrentCollection, stream);

                            var task = CollectionUpdater.Update(CurrentCollection);

                            task.ProgressChanged += (s, a) =>
                                {
                                    Progress.Value = (int)(a.Progress * 100);
                                };

                            task.Finished += (s, a) => 
                                {
                                    Progress.Value = 0;
                                    backupFile.Delete();
                                };
                        })
                };
            exportMenu.Items.Add(saveItem);
            CollectionList.SelectedIndexChanged += (sender, e) => saveItem.Enabled = CollectionList.SelectedIndex >= 0 && !String.IsNullOrEmpty(CurrentCollection.Descriptor.ImporterTypeName) && typeof(ICollectionUpdater).IsAssignableFrom(Plugin.GetType<ICollectionExporter>(CurrentCollection.Descriptor.ImporterTypeName));
                
            var exportButton = new Button { Text = "Export..." };
            exportButton.Command = new Command((sender, e) => exportMenu.Show(exportButton));

            // Actions

            var actions = Plugin.ProvidedTypes[typeof(IAction)];
            var actionMenu = new ContextMenu(actions.Select(x => new ButtonMenuItem
                { 
                    Text = x.GetExtensionTitle(),
                    Command = new Command((sender, e) =>
                        {
                            var instance = Plugin.CreateInstance<IAction>(x);
                            HandlePreconfiguration(instance, () =>
                                {
                                    bool configured = true;
                                    if (instance is IImageAction)
                                        configured = ((IImageAction)instance).Configure(Selection);
                                    if (instance is ICollectionAction)
                                        configured = ((ICollectionAction)instance).Configure(CurrentCollection);

                                    if (configured) 
                                    {
                                        var task = new ActionTask(instance);
                                        task.ProgressChanged += (s, a) => Progress.Value = (int)(a.Progress * 100);
                                        task.Finished += (s, a) => Progress.Value = 0;
                                        TaskEX.Schedule(task);
                                    }
                                    else
                                        Logger.Info($"Cancelled action {instance.Title}");
                                });
                        })
                }));

            /*actions = Plugin.ProvidedTypes[typeof(ICollectionAction)];
            foreach (var x in actions)
            {
                actionMenu.Items.Add(new ButtonMenuItem
                    {
                        Text = x.GetExtensionTitle(),
                        Command = new Command((sender, e) =>
                            {
                                var instance = Plugin.CreateInstance<ICollectionAction>(x);
                                HandlePreconfiguration(instance, () =>
                                {
                                    if (instance.Configure(CurrentCollection))
                                    {
                                        var task = new CollectionActionTask(instance);
                                        task.ProgressChanged += (s, a) => Progress.Value = (int)(a.Progress * 100);
                                        task.Finished += (s, a) => Progress.Value = 0;
                                        TaskEX.Schedule(task);
                                    }
                                    else
                                        Logger.Info($"Cancelled action {instance.Title}");
                                });
                            })
                    });
            }*/

            actionMenu.Items.Add(new ButtonMenuItem { Text = "Select all", Command = new Command((s, e) =>
                {
                    Selection.Clear();
                    foreach (var img in CurrentCollection.Images.Where(x => Filters.All(z => z(x))))
                        Selection.Add(img);
                    foreach (var box in ThumbContainer.Controls.Cast<ThumbBox>())
                        box.Selected.Value = true;
                })});

            var actionButton = new Button { Text = "Action..." };
            actionButton.Command = new Command((sender, e) => actionMenu.Show(actionButton));

            var newCollectionButton = new Button { Text = "New collection" };
            newCollectionButton.Command = new Command((sender, e) =>
                {
                    var dialog = new InputDialog("Name");
                    var name = dialog.Ask();
                    if (String.IsNullOrEmpty(name))
                        return;
                    Collections.Add(new Collection(name));
                });

            var bottomPanel = new TableLayout(
                new TableRow(
                    new TableCell(importButton),
                    new TableCell(exportButton),
                    new TableCell(actionButton),
                    new TableCell(newCollectionButton),
                    new TableCell(filtersButton),
                    new TableCell(Progress, true)
                )
            ) { Padding = new Padding(8, 0, 8, 8), Spacing = new Size(6, 6) };

            Content = new TableLayout(
                new TableRow(new TableCell(splitter, true)) { ScaleHeight = true },
                new TableRow(new TableCell(bottomPanel, true))
            );

            Title = "Collection Manager";
        }

        private void HandlePreconfiguration<T>(T instance, Action act)
        {
            var precon = instance as IPreconfigurable;
            if (precon == null)
                act();
            else
            {
                Task.Run(async () => await precon.Preconfigure())
                    .ContinueWith(x => Application.Instance.InvokeSafe(act));
            }
        }

        private void AddFilter(ContextMenu filtersMenu, string title, Predicate<EXImage> filter, bool isSystem = false, bool enabled = true)
        {
            if (enabled)
                Filters.Add(filter);

            var fi = new CheckMenuItem() { Text = title };
            fi.Checked = enabled;
            fi.CheckedChanged += (q, w) =>
                {
                    if (fi.Checked)
                        Filters.Add(filter);
                    else
                        Filters.RemoveAll(x => x.Equals(filter));

                    ForceReload();
                };

            if (!isSystem)
                FilterItems.Add(fi);
            
            filtersMenu.Items.Add(fi);
        }
	}
}

