﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Breezy.Assistant;
using Breezy.Assistant.Extensions;
using System.Net.Http;
using NLog;
using Breezy.PantEX.Images;

namespace Breezy.PantEX.GUI
{
    [Configurable("SourceBrowser")]
    public partial class SourceBrowser : Form
    {
        ImageSource Source;
        StateSavingEnumerable<EXImage> ImageStream;
        ThumbContainerController Controller;
        bool IsLoading;
        bool EndReached;
        TaskEX RunningTask;

        private Collection CurrentCollection
        {
            get { return (CollectionSelector.SelectedValue as ValueHoldingItem<Collection>).Value; }
        }

        public SourceBrowser(ImageSource src)
        {
            Source = src;
            ImageStream = src.StateSaving();

            InitializeLayout();

            Closed += (sender, e) => Controller.Dispose();
        }

        private void HandleLoadRequest(object sender, EventArgs args)
        {
            if (!IsLoading && !EndReached)
                Fetch();
        }

        private void ChangeCollection(Collection value)
        {
            Task.Run(() => Container.Controls.AsParallel().Select(x => new KeyValuePair<Control, bool>(x, value.Images.Contains((x as ThumbBox).InnerImage))).ToList())
                .ContinueWith(x => Application.Instance.InvokeSafe(() =>
                    {
                        foreach (var pair in x.Result)
                        {
                            var box = (ThumbBox)pair.Key;
                            if (box.Selected.Value != pair.Value)
                                box.Selected.Value = pair.Value;
                        }
                    }));
        }

        protected override void Dispose(bool disposing)
        {
            Controller?.Dispose();
            Controller = null;
        }

        protected override void OnClosed(EventArgs e)
        {
            Controller?.Dispose();
            Controller = null;
            RunningTask?.Cancel();
        }

        private SourceFetchTask CreateFetchTask(int n = 1)
        {
            var task = new SourceFetchTask(n, Source.Title, Source.Page, ImageStream);
            task.ProgressChanged += (s, a) =>
                {
                    foreach (var p in a.Value)
                    {
                        Controller.Add(p.Key, p.Value)
                                            .ContinueOnGUIWith(x =>
                                            {
                                                var xb = x.Result;
                                                xb.Selected.Value = CurrentCollection.Images.Contains(xb.InnerImage);
                                                xb.MouseUp += (z, e) =>
                                                    {
                                                        var col = CurrentCollection;
                                                        if (e.Buttons.HasFlag(MouseButtons.Primary))
                                                        {
                                                            xb.Selected.Value = !xb.Selected.Value;
                                                            if (!xb.Selected)
                                                                col.RemoveImage(xb.InnerImage);
                                                            else
                                                                col.AddImage(xb.InnerImage);
                                                        }
                                                        else
                                                            ImageViewer.SetImage(xb.InnerImage, Container.Controls.Cast<ThumbBox>().Select(y => y.InnerImage));
                                                    };
                                            });
                    }
                };

            return task;
        }

        private void Fetch(int n = 1)
        {
            IsLoading = true;
            FetchButton.Enabled = false;

            var task = CreateFetchTask(n);
            RunningTask = task;

            Controller.UpdatesEnabled = false;

            task.ResultProduced += (sender, args) =>
                {
                    IsLoading = false;
                    EndReached |= !args.Result.Any();
                    FetchButton.Enabled = true;
                    RunningTask = null;
                    Controller.UpdatesEnabled = true;
                };

            TaskEX.Schedule(task);
        }

        private void FetchWhile(Predicate<IEnumerable<EXImage>> func)
        {
            IsLoading = true;
            FetchButton.Enabled = false;

            var task = CreateFetchTask(1);
            RunningTask = task;

            Controller.UpdatesEnabled = false;

            task.ResultProduced += (sender, args) =>
                {
                    if (func(args.Result.Select(x => x.Key)))
                        FetchWhile(func);
                    else
                    {
                        IsLoading = false;
                        EndReached |= !args.Result.Any();
                        FetchButton.Enabled = true;
                        RunningTask = null;
                        Controller.UpdatesEnabled = true;
                    }
                };

            TaskEX.Schedule(task);
        }

        private void Fetch(object sender, EventArgs args)
        {
            Fetch();
        }

        // Static members

        private static int? DefaultWindowX;
        private static int? DefaultWindowY;
        private static int? DefaultWindowW = 800;
        private static int? DefaultWindowH = 600;
        private static int? DefaultThumbSize = 150;
        private static int? DefaultThumbCount = 4;

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            DefaultWindowX = dict.ContainsKey("WindowX") ? (int?)((long)dict["WindowX"]) : DefaultWindowX;
            DefaultWindowY = dict.ContainsKey("WindowY") ? (int?)((long)dict["WindowY"]) : DefaultWindowY;
            DefaultWindowW = dict.ContainsKey("WindowW") ? (int?)((long)dict["WindowW"]) : DefaultWindowW;
            DefaultWindowH = dict.ContainsKey("WindowH") ? (int?)((long)dict["WindowH"]) : DefaultWindowH;
            DefaultThumbSize = dict.ContainsKey("ThumbSize") ? (int?)((long)dict["ThumbSize"]) : DefaultThumbSize;
            DefaultThumbCount = dict.ContainsKey("ThumbCount") ? (int?)((long)dict["ThumbCount"]) : DefaultThumbCount;
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            if (DefaultWindowX.HasValue)
                dict["WindowX"] = DefaultWindowX.Value;
            if (DefaultWindowY.HasValue)
                dict["WindowY"] = DefaultWindowY.Value;
            if (DefaultWindowW.HasValue)
                dict["WindowW"] = DefaultWindowW.Value;
            if (DefaultWindowH.HasValue)
                dict["WindowH"] = DefaultWindowH.Value;
            if (DefaultThumbSize.HasValue)
                dict["ThumbSize"] = DefaultThumbSize.Value;
            if (DefaultThumbCount.HasValue)
                dict["ThumbCount"] = DefaultThumbCount.Value;
        }
    }

    [CanRunInBackground]
    public class SourceFetchTask : AbstractTask<IEnumerable<KeyValuePair<EXImage, Image>>>
    {
        ILogger Logger = LogManager.GetCurrentClassLogger();

        static readonly int PageSize = 20;

        int Pages;
        StateSavingEnumerable<EXImage> Source;

        int Counter;

        public SourceFetchTask(int pages, string title, int currentPage, StateSavingEnumerable<EXImage> source)
        {
            Pages = pages;
            Source = source;
            Title = $"Fetching {pages} pages from {title} [starting from: {currentPage}]";
        }

        async Task<KeyValuePair<EXImage, Image>> LoadThumb(EXImage img)
        {
            try
            {
                var thumb = await img.Thumb.GetImageAsync();
                return new KeyValuePair<EXImage, Image>(img, ImageFormatHelper.LoadImage(thumb));
            }
            catch (Exception e)
            {
                Logger.Error(e, "An error occured while loading thumbnail");
                return new KeyValuePair<EXImage, Image>(img, ImageFormatHelper.LoadImage(new ThumbImage(img)));
            }
        }

        async Task<KeyValuePair<EXImage, Image>> Map(EXImage img)
        {
            var res = await LoadThumb(img);

            if (IsTokenSet)
                Token.ThrowIfCancellationRequested();

            HandleProgress(res.ToEnumerable(), Counter++ / (double)(Pages * PageSize));
            return res;
        }

        protected override async Task Execute()
        {
            var items = await Source
                .TakeSlice(PageSize * Pages)
                .MapParAsync(Map, 16);

            if (IsTokenSet)
                Token.ThrowIfCancellationRequested();

            HandleFinish(items);
        }
    }
}

