﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Eto.Forms;
using System.Collections.Specialized;
using System.Threading;
using Breezy.Venus;
using Breezy.Assistant;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI
{
    internal class ValueHoldingItem<T> : IListItem
    {
        public T Value { get; private set; }

        public ValueHoldingItem(T value)
        {
            Value = value;
        }

        protected virtual string MapToKey()
        {
            return Value.ToString();
        }

        protected virtual string Format()
        {
            return Value.ToString();
        }

        public string Text {
            get { return Format(); }
            set { throw new NotImplementedException(); }
        }

        public string Key {
            get { return MapToKey(); }
        }

        public ValueHoldingItem<T> MutateValue(Func<T, T> mutator)
        {
            Value = mutator(Value);
            return this;
        }

        public ValueHoldingItem<T> MutateValue(Action<T> mutator)
        {
            mutator(Value);
            return this;
        }

        public static Func<T, ValueHoldingItem<T>> Map = x => new ValueHoldingItem<T>(x);
        public static Func<T, Func<T, string>, Func<T, string>, FormattingValueHoldingItem<T>> MapFormatting = (x, k, t) => new FormattingValueHoldingItem<T>(x, k, t);
    }

    internal class FormattingValueHoldingItem<T> : ValueHoldingItem<T>
    {
        private Func<T, string> KeyMapping;
        private Func<T, string> TextMapping;

        public FormattingValueHoldingItem(T value, Func<T, string> keyMapping, Func<T, string> textMapping)
            : base(value)
        {
            KeyMapping = keyMapping;
            TextMapping = textMapping;
        }

        protected override string MapToKey()
        {
            return KeyMapping(Value);
        }

        protected override string Format()
        {
            return TextMapping(Value);
        }
    }

    internal class CollectionListItem : ValueHoldingItem<Collection>
    {
        public CollectionListItem(Collection value) 
            : base(value)
        {
        }
    }

    internal class EnumerableMappingTask<T, R> : AbstractTask<IEnumerable<R>>
    {
        private readonly Func<T, Task<R>> Mapper;
        private readonly IEnumerable<T> Enumerable;

        public EnumerableMappingTask(IEnumerable<T> enumerable, Func<T, Task<R>> mapper)
        {
            this.Mapper = mapper;
            this.Enumerable = enumerable;
        }

        protected override async Task Execute()
        {
            double count = Enumerable.Count();
            int i = 0;
            var list = new List<R>();
            foreach (var item in Enumerable)
            {
                var res = await Mapper(item);
                list.Add(res);
                HandleProgress(res.ToEnumerable(), ++i / count);
            }
            HandleFinish(list);
        }
    }

    internal class EnumerableParallelMappingTask<T, R> : AbstractTask<IEnumerable<R>>
    {
        private readonly Func<T, Task<R>> Mapper;
        private readonly IEnumerable<T> Enumerable;
        private readonly int Degree;

        private double Count;
        private int Counter;

        public EnumerableParallelMappingTask(IEnumerable<T> enumerable, Func<T, Task<R>> mapper, int degree)
        {
            Mapper = mapper;
            Enumerable = enumerable;
            Degree = degree;
        }

        private async Task<R> HandleResult(T value)
        {
            var result = await Mapper(value);
            HandleProgress(result.ToEnumerable(), Counter++ / Count);
            return result;
        }

        protected override async Task Execute()
        {
            Count = Enumerable.Count();
            var result = await Enumerable.MapParAsync(HandleResult, Degree);
            HandleFinish(result);
        }
    }

    public static class Extensions
    {
        #region ListControl

        public static void HookTo<T>(this ListControl control, ObservableCollection<T> collection)
        {
            control.Items.AddRange(collection.Select(x => new ValueHoldingItem<T>(x)));

            NotifyCollectionChangedEventHandler hook = (sender, e) => 
                {
                    Application.Instance.AsyncInvoke(() =>
                        {
                            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                                foreach (var i in e.NewItems)
                                    control.Items.Add(new ValueHoldingItem<T>((T)i));
                            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                                foreach (var i in e.NewItems)
                                    control.Items.Remove(control.Items.Where(x => ((ValueHoldingItem<T>)x).Value.Equals(i)).First());
                        });
                };


            collection.CollectionChanged += hook;

            control.UnLoad += (sender, e) => collection.CollectionChanged -= hook;
        }

        public static T GetSelectedValue<T> (this ListControl control) where T : class
        {
            return control.SelectedIndex >= 0 ? (control.Items[control.SelectedIndex] as ValueHoldingItem<T>)?.Value : null;
        }

        #endregion

        #region IListItem

        public static T Unwrap<T>(this IListItem item)
        {
            return (item is ValueHoldingItem<T>) ? ((ValueHoldingItem<T>)item).Value : default(T);
        }

        #endregion

        #region Thread

        public static bool IsUIThread(this Thread thread)
        {
            return thread.ManagedThreadId == MainForm.UIThreadId;
        }

        #endregion

        #region Object

        public static bool IsOnUIThread(this object obj)
        {
            return Thread.CurrentThread.ManagedThreadId == MainForm.UIThreadId;
        }

        #endregion

        #region EXImage

        public static async Task<RawImage> GetImageExtAsync(this EXImage img)
        {
            return await Task.Run(() => img.GetImage());
        }

        #endregion

        #region Generic

        internal static ValueHoldingItem<T> WrapInListItem<T>(this T value)
        {
            return new ValueHoldingItem<T>(value);
        }

        internal static ValueHoldingItem<T> WrapInListItem<T>(this T value, Func<T, string> keyMapping, Func<T, string> textMapping)
        {
            return new FormattingValueHoldingItem<T>(value, keyMapping, textMapping);
        }

        #endregion

        #region ICollectionImporter

        public static ICollectionImporter ConfigureChain(this ICollectionImporter importer)
        {
            Application.Instance.InvokeSafe(() => importer.Configure());
            return importer;
        }

        #endregion

        #region ImageSourceFactory

        public static ImageSourceFactory ConfigureChain(this ImageSourceFactory factory)
        {
            Application.Instance.InvokeSafe(() => factory.Configure());
            return factory;
        }

        #endregion

        #region IEnumerable

        public static AbstractTask<IEnumerable<R>> MapAsTask<T, R>(this IEnumerable<T> enumerable, Func<T, Task<R>> mapper)
        {
            return new EnumerableParallelMappingTask<T, R>(enumerable, mapper, 8);
        }

        #endregion

        #region Application

        public static async Task<R> InvokeAsync<R>(this Application app, Func<R> func)
        {
            return await Task.Run(() => app.Invoke(func));
        }

        public static R InvokeSafe<R>(this Application app, Func<R> func)
        {
            if (app.Platform.IsWpf)
            {
                Synchronizer.Instance.Semaphore.WaitOne();
                var result = app.Invoke(func);
                Synchronizer.Instance.Semaphore.Release();
                return result;
            }
            else
                return app.Invoke(func);
        }

        public static void InvokeSafe(this Application app, Action func)
        {
            if (app.Platform.IsWpf)
            {
                Synchronizer.Instance.Semaphore.WaitOne();
                app.Invoke(func);
                Synchronizer.Instance.Semaphore.Release();
            }
            else
                app.Invoke(func);
        }

        #endregion

        #region Action

        public static void InvokeOnUI(this Action action)
        {
            if (action.IsOnUIThread())
                action.Invoke();
            else
                Application.Instance.InvokeSafe(action);
        }

        #endregion

        #region Func

        public static R InvokeOnUI<R>(this Func<R> action)
        {
            if (action.IsOnUIThread())
                return action.Invoke();
            else
                return Application.Instance.InvokeSafe(action);
        }

        #endregion

        #region IPreconfigurable

        public static void Configure(this IPreconfigurable preconfigurable, Action action)
        {
            if (preconfigurable == null)
                action();
            else
            {
                Task.Run(async () => await preconfigurable.Preconfigure())
                    .ContinueWith(x => Application.Instance.InvokeSafe(action));
            }
        }

        #endregion

        #region Task

        public static Task ContinueOnGUIWith<T>(this Task<T> task, Action<Task<T>> action)
        {
            return task.ContinueWith(x => Application.Instance.InvokeSafe(() => action(x)));
        }

        public static Task<R> ContinueOnGUIWith<T, R>(this Task<T> task, Func<Task<T>, R> func)
        {
            return task.ContinueWith(x => Application.Instance.InvokeSafe(() => func(x)));
        }

        #endregion
    }
}

