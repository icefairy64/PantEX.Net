﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;
using System.Linq;
namespace Breezy.PantEX.GUI.Actions
{
    [Named("Reload images from sources...")]
    public class ReloadImages : IImageAction
    {
        IEnumerable<EXImage> Images;

        public string Title => $"Reloading {Images.Count()} images";

        public bool Configure(IEnumerable<EXImage> images)
        {
            Images = images;
            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            var result = new List<EXImage>();

            var index = 0;
            foreach (var image in Images)
            {
                var newImage = await RemoteImageProvider.CreateFromSource(image);
                if (newImage == null)
                    newImage = image;
                result.Add(newImage);
                index++;
                progressHandler.Invoke(((double)index) / Images.Count());
            }

            index = 0;
            foreach (var image in Images)
            {
                var collection = image.Collection;
                var newImage = result[index];
                if (image != newImage)
                {
                    collection.RemoveImage(image);
                    collection.AddImage(newImage);
                }
                index++;
            }
        }
    }
}
