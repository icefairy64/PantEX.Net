﻿using System;
using System.Threading.Tasks;
using Eto.Forms;
using System.Text;
using System.Linq;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Collection statistics")]
    public class CollectionStats : ICollectionAction
    {
        private Collection Collection;

        public CollectionStats()
        {
        }

        public bool Configure(Collection collection)
        {
            Collection = collection;
            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Images: {Collection.Images.Count}");
            builder.AppendLine($"Tags: {Collection.Tags.Count}");
            builder.AppendLine($"Average tag count per image: {Collection.Images.Select(x => x.Tags.Count()).Average()}");
            MessageBox.Show(builder.ToString(), $"Statistics for {Collection.Title}", MessageBoxType.Information);
        }

        public string Title => "Show collection statistics";
    }
}

