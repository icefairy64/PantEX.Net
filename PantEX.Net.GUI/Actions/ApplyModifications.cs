﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.CodeDom.Compiler;
using System.IO;
using Eto.Forms;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Apply modifications...")]
    public class ApplyModifications : ICollectionAction
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private Collection TargetCollection;
        private FileInfo File;

        public string Title { get { return $"Applying modifications for {TargetCollection.Title}"; } }

        public bool Configure(Collection collection)
        {
            TargetCollection = collection;
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog(CollectionManager.Instance) == DialogResult.Ok)
                File = new FileInfo(dialog.FileName);
            else
                return false;
            return true;
        }

        public static async Task Apply(Collection collection, FileInfo file, ProgressHandler progressHandler)
        {
            var js = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            };

            using (var reader = file.OpenText())
            {
                var mods = JsonConvert.DeserializeObject<IEnumerable<CollectionModification>>(await reader.ReadToEndAsync(), js);
                var count = mods.Count();
                double i = 0;
                foreach (var mod in mods)
                {
                    if (0.IsOnUIThread())
                    {
                        switch (mod.Kind)
                        {
                            case CollectionModificationKind.ImageAddition:
                                Logger.Trace($"Adding {mod.Subject}");
                                collection.AddImage((EXImage)mod.Subject);
                                break;
                            case CollectionModificationKind.ImageRemoval:
                                Logger.Trace($"Removing {mod.Subject}");
                                collection.RemoveImage((EXImage)mod.Subject);
                                break;
                        }
                        progressHandler?.Invoke(i++ / count);
                    }
                    else
                    {

                        Application.Instance.Invoke(() =>
                        {
                            switch (mod.Kind)
                            {
                                case CollectionModificationKind.ImageAddition:
                                    Logger.Trace($"Adding {mod.Subject}");
                                    collection.AddImage((EXImage)mod.Subject);
                                    break;
                                case CollectionModificationKind.ImageRemoval:
                                    Logger.Trace($"Removing {mod.Subject}");
                                    collection.RemoveImage((EXImage)mod.Subject);
                                    break;
                            }
                            progressHandler?.Invoke(i++ / count);
                        });
                    }
                }
            }
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            await Apply(TargetCollection, File, progressHandler);
        }
    }
}

