﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Move...")]
    public class MoveImages : IImageAction
    {
        private Collection TargetCollection;
        private IEnumerable<EXImage> Images;

        public string Title {
            get {
                return $"Moving images to {TargetCollection?.Title}";
            }
        }

        public bool Configure(IEnumerable<EXImage> images)
        {
            var dialog = new SelectorDialog<Collection>(CollectionManager.Collections, "Select a collection");
            TargetCollection = dialog.Ask();
            Images = images;
            return TargetCollection != null;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            if (TargetCollection == null)
                return;

            await Task.Run(() => progressHandler?.Invoke(0.0));
            int i = 0;
            double n = Images.Count();
            foreach (var img in Images)
            {
                img.Collection.RemoveImage(img);
                TargetCollection.AddImage(img);
                progressHandler?.Invoke(i / n);
            }
            await Task.Run(() => progressHandler?.Invoke(1.0));
        }
    }
}

