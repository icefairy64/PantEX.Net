﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Eto.Forms;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Export selection...")]
    public class ExportSelection : IImageAction
    {
        private DirectoryInfo Directory;
        private bool Cancelled;
        private IEnumerable<EXImage> Images;

        public ExportSelection()
        {
        }

        public bool Configure(IEnumerable<EXImage> images)
        {
            var dialog = new SelectFolderDialog();
            if (dialog.ShowDialog(CollectionManager.Instance) != DialogResult.Ok)
            {
                Cancelled = true;
                return false;
            }
            if (!string.IsNullOrEmpty(dialog.Directory))
                Directory = new DirectoryInfo(dialog.Directory);

            Images = images;

            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            if (Cancelled || Directory == null || !Directory.Exists)
                return;

            int i = 0;
            double c = Images.Count();

            foreach (var img in Images)
            {
                var file = new FileInfo(Path.Combine(Directory.FullName, img.Filename));
                if (!file.Exists)
                {
                    using (var stream = file.OpenWrite())
                    {
                        using (var src = await img.GetDataStreamAsync())
                            await src.CopyToAsync(stream);
                    }
                }
                    
                if (this.IsOnUIThread())
                    progressHandler?.Invoke(i++ / c);
                else
                    Application.Instance.Invoke(() => progressHandler?.Invoke(i++ / c));
            }
        }

        public string Title
        {
            get
            {
                return $"Exporting selection to {Directory?.FullName}";
            }
        }
    }
}

