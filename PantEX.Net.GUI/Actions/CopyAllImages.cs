﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using System.Linq;
using System.IO;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Copy all images...")]
    public class CopyAllImages : ICollectionAction
    {
        private Collection SourceCollection;
        private Collection TargetCollection;

        public bool Configure(Collection collection)
        {
            SourceCollection = collection;
            var dialog = new SelectorDialog<Collection>(CollectionManager.Collections, "Select a collection");
            TargetCollection = dialog.Ask();
            return TargetCollection != null;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            if (TargetCollection == null)
                return;
            
            await Task.Run(() => progressHandler?.Invoke(0.0));
            int i = 0;
            var images = SourceCollection.Images
                .Where(x => !TargetCollection.Images.Any(z => Path.GetFileNameWithoutExtension(x.Filename).Equals(Path.GetFileNameWithoutExtension(z.Filename))));
            double n = images.Count();
            foreach (var img in images)
            {
                TargetCollection.AddImage(img);
                progressHandler?.Invoke(i / n);
            }
            await Task.Run(() => progressHandler?.Invoke(1.0));
        }

        public string Title
        {
            get
            {
                return $"Copying images from {SourceCollection.Title} to {TargetCollection?.Title}";
            }
        }
    }
}

