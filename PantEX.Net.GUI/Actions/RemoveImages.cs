﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Remove")]
    public class RemoveImages : IImageAction
    {
        private IEnumerable<EXImage> Images;

        public string Title {
            get {
                return "Removing images";
            }
        }

        public bool Configure(IEnumerable<EXImage> images)
        {
            Images = images;
            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            await Task.Run(() => progressHandler?.Invoke(0.0));
            int i = 0;
            double n = Images.Count();
            foreach (var img in Images)
            {
                img.Collection.RemoveImage(img);
                progressHandler?.Invoke(i / n);
            }
            await Task.Run(() => progressHandler?.Invoke(1.0));
        }
    }
}

