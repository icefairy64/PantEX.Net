﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Set collection meta value...")]
    public class SetMetaValue : ICollectionAction
    {
        private Collection TargetCollection;
        private string MetaName;
        private string MetaValue;

        public SetMetaValue()
        {
        }

        public bool Configure(Collection collection)
        {
            var d = new InputDialog("Data (\"name=value\")");
            var str = d.Ask();
            if (!str.Contains("="))
                return false;
            var parts = str.Split('=');
            MetaName = parts[0];
            MetaValue = parts[1];
            TargetCollection = collection;
            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            TargetCollection.Metadata[MetaName] = MetaValue;
        }

        public string Title
        {
            get
            {
                return "Setting meta value";
            }
        }
    }
}

