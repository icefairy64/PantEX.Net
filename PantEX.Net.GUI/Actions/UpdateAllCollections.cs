﻿using System;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;
using System.Linq;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Update all collections")]
    public class UpdateAllCollections : IAction
    {
        public UpdateAllCollections()
        {
        }

        public string Title { get; } = "Updating all collections...";

        public async Task Perform(ProgressHandler progressHandler)
        {
            foreach (var collection in CollectionManager.Collections)
            {
                if (collection.Modifications.Any())
                {
                    var task = CollectionUpdater.Update(collection);
                    TaskProgressHandler<Collection> handler = (sender, args) => progressHandler(args.Progress);
                    task.ProgressChanged += handler;
                    task.Finished += (sender, e) => task.ProgressChanged -= handler;
                }
            }
        }
    }
}
