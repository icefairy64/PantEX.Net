﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Eto.Forms;
using Eto.Drawing;
using System.Threading;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Manage tags")]
    public class ManageTags : IImageAction
    {
        private IEnumerable<EXImage> Images;
        private TagManagementForm Dialog;

        public bool Configure(IEnumerable<EXImage> images)
        {
            Images = images;

            var commonCollection = Images.GroupBy(x => x.Collection)
                .First().First().Collection;

            var commonTags = commonCollection.Tags.Where(x => Images.All(z => z.Tags.Contains(x))).ToList();

            Dialog = new TagManagementForm(commonTags, commonCollection.Tags);
            Dialog.ShowModal();

            return Dialog.Modifications.Count > 0;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            foreach (var mod in Dialog.Modifications)
            {
                foreach (var img in Images)
                {
                    switch (mod.Item1)
                    {
                        case TagModificationType.Add:
                            img.AddTag(mod.Item2);
                            break;
                        case TagModificationType.Remove:
                            img.RemoveTag(mod.Item2);
                            break;
                    }
                }
            }
        }

        public string Title
        {
            get
            {
                return "Managing tags";
            }
        }
    }

    internal enum TagModificationType
    {
        Add,
        Remove
    }

    internal class TagModificationResultEventArgs : EventArgs
    {
        public readonly bool Result;
        public readonly IEnumerable<Tuple<TagModificationType, Tag>> Modifications;

        public TagModificationResultEventArgs(bool result, IEnumerable<Tuple<TagModificationType, Tag>> modifications)
        {
            Result = result;
            Modifications = modifications;
        }
    }

    internal class TagManagementForm : Dialog
    {
        public List<Tuple<TagModificationType, Tag>> Modifications;

        private bool Apply = false;

        public TagManagementForm(IEnumerable<Tag> commonTags, IEnumerable<Tag> collectionTags)
            : base()
        {
            Modifications = new List<Tuple<TagModificationType, Tag>>();

            Title = "Manage tags";
            Size = new Size(500, 600);

            var layout = new TableLayout { Padding = 8, Spacing = new Size(6, 6) };

            var topPanel = new StackLayout { Spacing = 6, Orientation = Orientation.Horizontal, VerticalContentAlignment = VerticalAlignment.Stretch };

            var commonTagsList = new ListBox();
            commonTagsList.Items.AddRange(commonTags.Select(x => x.WrapInListItem()));

            var collectionTagsList = new ListBox();
            collectionTagsList.Items.AddRange(collectionTags.Where(x => !commonTags.Any(z => z == x)).Select(x => x.WrapInListItem()));

            commonTagsList.Activated += (sender, e) => 
                {
                    var tag = commonTagsList.GetSelectedValue<Tag>();
                    Modifications.RemoveAll(x => x.Item2 == tag);
                    Modifications.Add(new Tuple<TagModificationType, Tag>(TagModificationType.Remove, tag));

                    commonTagsList.Items.Remove(commonTagsList.Items.Where(x => tag == x.Unwrap<Tag>()).FirstOrDefault());

                    if (!collectionTagsList.Items.Any(x => tag == x.Unwrap<Tag>()))
                        collectionTagsList.Items.Add(tag.WrapInListItem());
                };

            collectionTagsList.Activated += (sender, e) => 
                {
                    var tag = collectionTagsList.GetSelectedValue<Tag>();
                    Modifications.RemoveAll(x => x.Item2 == tag);
                    Modifications.Add(new Tuple<TagModificationType, Tag>(TagModificationType.Add, tag));

                    collectionTagsList.Items.Remove(collectionTagsList.Items.Where(x => tag == x.Unwrap<Tag>()).FirstOrDefault());

                    if (!commonTagsList.Items.Any(x => tag == x.Unwrap<Tag>()))
                        commonTagsList.Items.Add(tag.WrapInListItem());
                };

            topPanel.Items.Add(new StackLayoutItem(commonTagsList, true));
            topPanel.Items.Add(new StackLayoutItem(collectionTagsList, true));

            layout.Rows.Add(new TableRow(new TableCell(topPanel)) { ScaleHeight = true });

            var bottomPanel = new StackLayout { Spacing = 6, Orientation = Orientation.Horizontal };

            var newTagButton = new Button { Text = "Create a tag" };
            newTagButton.Click += (sender, e) => 
                {
                    new InputDialog("Tag")
                        .AskAsync()
                        .ContinueWith(x => Application.Instance.InvokeSafe(() => collectionTagsList.Items.Add(PantEX.Tag.Create(x.Result).WrapInListItem())));
                };

            var okButton = new Button { Text = "OK" };
            okButton.Click += (sender, e) =>
                {
                    Apply = true;
                    Close();
                };

            var cancelButton = new Button { Text = "Cancel" };
            cancelButton.Click += (sender, e) => Close();

            Closing += (sender, e) => 
                {
                    if (!Apply)
                        Modifications.Clear();
                };

            bottomPanel.Items.Add(null);
            bottomPanel.Items.Add(new StackLayoutItem(newTagButton));
            bottomPanel.Items.Add(new StackLayoutItem(okButton));
            bottomPanel.Items.Add(new StackLayoutItem(cancelButton));

            layout.Rows.Add(new TableRow(new TableCell(bottomPanel)));

            Content = layout;
        }
    }
}

