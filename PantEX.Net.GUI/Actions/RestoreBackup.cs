﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Eto.Forms;
using NLog;
using Breezy.Assistant;
using System.IO;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Restore automatic backup")]
    public class RestoreBackup : IAction
    {
        public string Title => "Restore automatic backup";

        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public async Task Perform(ProgressHandler progressHandler)
        {
            var descriptorFiles = FileManager.BackupDirectory.EnumerateFiles()
                       .Where(x => x.Extension == AutoBackup.BackupDescriptorExtension)
                                             .Select(x => { Logger.Debug($"Found descriptor file {x.Name}"); return x.WrapInListItem(z => z.FullName, z => z.Name); })
                                             .OrderByDescending(x => x.Value.LastWriteTime);

            var dialog = new SelectorDialog<FileInfo>(descriptorFiles, "Select a backup to restore");
            var descriptorFile = await dialog.AskAsync();

            if (descriptorFile != null)
            {
                Logger.Debug($"Applying backup: {descriptorFile.Name}");
                using (var reader = descriptorFile.OpenText())
                {
                    while (!reader.EndOfStream)
                    {
                        var line = await reader.ReadLineAsync();
                        var collectionModsFile = FileManager.GetBackupFile(line);
                        Logger.Debug($"Applying unsaved changes from: {collectionModsFile.Name}");
                        var collection = CollectionManager.Collections
                                                          .FirstOrDefault(x => x.Title == collectionModsFile.Name.SubstringUntil('.'));
                        if (collection != null)
                        {
                            await ApplyModifications.Apply(collection, collectionModsFile, progressHandler);
                        }
                        else
                            Logger.Debug($"Matching collection not found");
                    }
                }
            }

            progressHandler?.Invoke(1);
        }
    }
}
