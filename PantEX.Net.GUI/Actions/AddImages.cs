﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using Eto.Forms;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Breezy.PantEX.Images;

namespace Breezy.PantEX.GUI.Actions
{
    [Named("Add images...")]
    public class AddImages : ICollectionAction
    {
        private Collection TargetCollection;
        private IEnumerable<string> Files;

        public string Title { get { return $"Adding images to {TargetCollection.Title}"; } }

        public bool Configure(Collection collection)
        {
            TargetCollection = collection;
            var dialog = new OpenFileDialog() { MultiSelect = true, Filters = { new FileDialogFilter("Images", ".jpg", ".png", ".jpeg", ".webp") } };
            if (dialog.ShowDialog(CollectionManager.Instance) == DialogResult.Ok)
                Files = dialog.Filenames;
            else
                return false;
            return true;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            var images = new List<EXImage>();
            double i = 0;
            foreach (var file in Files)
            {
                var img = await Task.Run(() => new FileImage(file));
                img.Thumb = new ThumbImage(img);
                images.Add(img);
                Application.Instance.Invoke(() => 
                    {
                        progressHandler?.Invoke(++i / Files.Count());
                        TargetCollection.AddImage(img);
                    });
            }
        }
    }
}

