﻿using System;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using NLog;

namespace Breezy.PantEX.GUI
{
    [CanRunInBackground]
    public class ImageLoadTask : AbstractTask<Image>
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private readonly EXImage Image;
        private readonly int Frame;

        public ImageLoadTask(EXImage img, int frame = 0)
        {
            Image = img;
            Frame = frame;
        }

        protected override async Task Execute()
        {
            Logger.Trace($"Loading full image for {Image}");
            var res = await ImageFormatHelper.LoadImageAsync(Image, Frame);
            Logger.Trace("Loading complete");
            HandleFinish(res);
        }
    }
}

