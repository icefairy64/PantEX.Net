﻿using System;
using Breezy.Assistant.Extensions;
using System.IO;
using Eto.Forms;
using System.Threading.Tasks;

namespace Breezy.PantEX.GUI
{
    [Named("EXPack")]
    public class EXPack : ICollectionExporter
    {
        private DirectoryInfo Directory;
        private DirectoryInfo ThumbsDirectory;

        public EXPack()
        {
        }

        public void Configure()
        {
            var d = new SelectFolderDialog();
            if (d.ShowDialog(CollectionManager.Instance) == DialogResult.Ok)
            {
                Directory = new DirectoryInfo(d.Directory); 
                ThumbsDirectory = new DirectoryInfo(Path.Combine(d.Directory, "thumbs"));
            }
        }

        public void Configure(CollectionDescriptor des)
        {
            if (String.IsNullOrEmpty(des.Descriptor))
                Configure();
            else
            {
                Directory = new DirectoryInfo(des.Descriptor); 
                ThumbsDirectory = new DirectoryInfo(Path.Combine(des.Descriptor, "thumbs"));
            }
        }

        public async Task Export(Collection collection, ProgressHandler progHandler)
        {
            if (!Directory.Exists)
                Directory.Create();
            if (!ThumbsDirectory.Exists)
                ThumbsDirectory.Create();

            using (var stream = new FileStream(Path.Combine(Directory.FullName, "collection.meta"), FileMode.Create))
            {
                PantEX.EXPack.Save(collection, (x, p) =>
                    {
                        var fn = Path.Combine(Directory.FullName, x.Filename);
                        if (!File.Exists(fn))
                        {
                            using (var ds = x.GetDataStreamAsync().Result)
                            using (var os = new FileStream(fn, FileMode.Create))
                                ds.CopyToAsync(os).Wait();
                        }
                        fn = Path.Combine(ThumbsDirectory.FullName, x.Thumb.Filename);
                        if (!File.Exists(fn))
                        {
                            using (var ds = x.Thumb.GetDataStreamAsync().Result)
                            using (var os = new FileStream(fn, FileMode.Create))
                                ds.CopyToAsync(os).Wait();
                        }
                        progHandler?.Invoke(p);
                    }, stream);
            }
        }

        public string Title
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}

