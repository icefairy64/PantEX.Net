﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    internal class Synchronizer
    {
        internal Semaphore Semaphore = new Semaphore(1, 1);
        
        public Bitmap CreateBitmap(int width, int height, PixelFormat format)
        {
            Semaphore.WaitOne();
            var result = new Bitmap(width, height, format);
            Semaphore.Release();
            return result;
        }

        // Static members

        private static Synchronizer FInstance;

        public static Synchronizer Instance
        {
            get
            {
                if (FInstance == null)
                    Initialize();
                return FInstance;
            }
        }

        public static void Initialize()
        {
            FInstance = new Synchronizer();
        }
    }
}
