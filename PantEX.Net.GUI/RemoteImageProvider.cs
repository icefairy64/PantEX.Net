﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using NLog;

namespace Breezy.PantEX.GUI
{
    [Configurable("RemoteImageProvider")]
    public static class RemoteImageProvider
    {
        static List<IRemoteImageProvider> Providers;
        static ILogger Logger = LogManager.GetCurrentClassLogger();

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            Providers = new List<IRemoteImageProvider>();
            MainForm.OnStart += (sender, e) => {
                var providers = Plugin.ProvidedTypes[typeof(IRemoteImageProvider)];
                Logger.Debug($"Known image providers: {providers.Count}");
                foreach (var provType in providers)
                {
                    Logger.Debug($"Found remote image provider: {provType}");
                    var instance = Plugin.CreateInstance<IRemoteImageProvider>(provType);
                    Providers.Add(instance);
                }
            };
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            
        }

        public static async Task<EXImage> CreateFromSource(EXImage proto)
        {
            foreach (var provider in Providers)
            {
                if (provider.Match(proto.Source))
                {
                    var result = await provider.CreateImageFromUri(proto.Source);
                    if (result != null)
                        return result;
                }
                    
            }
            return null;
        }
    }
}
