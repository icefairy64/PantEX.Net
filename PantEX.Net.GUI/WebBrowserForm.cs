﻿using System;
using Eto.Forms;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    public class UriEventArgs : EventArgs
    {
        public Uri Uri { get; private set; }

        public UriEventArgs(Uri uri)
        {
            Uri = uri;
        }
    }

    public class WebBrowserForm : Form
    {
        private WebView View;
        private Predicate<Uri> CloseTrigger;
        private Uri StartingUrl;

        public event EventHandler<UriEventArgs> NavigatedToMatchedUrl;

        public WebBrowserForm(Uri url, string title, Predicate<Uri> closeTrigger = null)
            : base()
        {
            View = new WebView();

            var layout = new StackLayout { Padding = new Padding(8), HorizontalContentAlignment = HorizontalAlignment.Stretch };
            layout.Items.Add(new StackLayoutItem(View, true));

            Title = title;
            CloseTrigger = closeTrigger;
            StartingUrl = url;

            View.Navigated += (sender, e) => HandleNavigation(e.Uri);

            Content = layout;
            Size = new Size(400, 600);

            Shown += (sender, e) => View.Url = StartingUrl;
        }

        private void HandleNavigation(Uri url)
        {
            if (CloseTrigger == null)
                return;

            if (CloseTrigger(url))
            {
                NavigatedToMatchedUrl?.Invoke(this, new UriEventArgs(url));
                Close();
            }
        }
    }
}

