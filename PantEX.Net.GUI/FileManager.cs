﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.IO;

namespace Breezy.PantEX.GUI
{
    [Configurable("FileManager")]
    public static class FileManager
    {
        public static string BackupPath = "backup";
        public static string ImagePlaceholderPath = Path.Combine(Environment.CurrentDirectory, "no-image.png");
        public static string CurrentDirectory = Environment.CurrentDirectory;

        private static readonly List<FileInfo> OpenTempFiles = new List<FileInfo>();

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            BackupPath = dict["BackupPath"] as string;
            ImagePlaceholderPath = dict["ImagePlaceholder"] as string;

            var dir = new DirectoryInfo(Path.Combine(CurrentDirectory, BackupPath));
            if (!dir.Exists)
                dir.Create();
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {
            dict["BackupPath"] = BackupPath;
            dict["ImagePlaceholder"] = ImagePlaceholderPath;
        }

        public static FileInfo ImagePlaceholder
        {
            get { return new FileInfo(ImagePlaceholderPath); }
        }

        public static FileInfo GetTempFile()
        {
            var file = new FileInfo(Path.GetTempFileName());
            OpenTempFiles.Add(file);
            return file;
        }

        public static void RemoveTempFiles()
        {
            foreach (var file in OpenTempFiles)
                file.Delete();
        }

        public static FileInfo GetBackupFile(string name)
        {
            return new FileInfo(Path.Combine(CurrentDirectory, BackupPath, name));
        }

        public static DirectoryInfo BackupDirectory => new DirectoryInfo(Path.Combine(CurrentDirectory, BackupPath));
    }
}

