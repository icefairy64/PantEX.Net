﻿using System;
using System.Linq;
using Eto.Forms;
using Eto.Drawing;
using Breezy.Assistant.Extensions;
using Breezy.Venus;

namespace Breezy.PantEX.GUI.Preferences
{
    public class AutoImporterPreferencesPageHandler : AbstractPreferencesPageHandler
    {
        public AutoImporterPreferencesPageHandler()
            : base()
        {
        }

        public override TabPage CreatePage()
        {
            var list = new ListBox();
            list.Items.AddRange(AutoImporter.Descriptors.Select(ValueHoldingItem<CollectionDescriptor>.Map));

            var listPanel = new StackLayout 
                { 
                    MinimumSize = new Size(200, 0), 
                    HorizontalContentAlignment = HorizontalAlignment.Stretch, 
                    VerticalContentAlignment = VerticalAlignment.Stretch 
                };
            
            listPanel.Items.Add(new StackLayoutItem(list, true));

            var titleBox = new TextBox { Enabled = false };
            var classBox = new DropDown { Enabled = false };
            var descriptorBox = new TextBox { Enabled = false };

            var importers = Plugin.ProvidedTypes[typeof(ICollectionImporter)].Select(x => x.WrapInListItem(z => z.FullName, z => z.GetExtensionTitle()));

            var addButton = new Button
                { 
                    Text = "Add", 
                    Command = new Command((s, a) => 
                            new SelectorDialog<Type>(importers, "Choose an importer")
                                .AskAsync()
                                .ContinueWith(x => list.Items.Add(Plugin.CreateInstance<ICollectionImporter>(x.Result).ConfigureChain().GenerateDescriptor().WrapInListItem())))
                };
            
            var removeButton = new Button 
                { 
                    Text = "Remove", 
                    Command = new Command((s, a) => list.Items.RemoveAt(list.SelectedIndex)), Enabled = false 
                };

            var managePanel = new StackLayout { Spacing = 6, Orientation = Orientation.Horizontal };
            managePanel.Items.Add(null);
            managePanel.Items.Add(new StackLayoutItem(addButton));
            managePanel.Items.Add(new StackLayoutItem(removeButton));

            var propPanel = new StackLayout { Spacing = 4, HorizontalContentAlignment = HorizontalAlignment.Stretch };
            propPanel.Items.Add(new StackLayoutItem(new Label { Text = "Title:" }));
            propPanel.Items.Add(new StackLayoutItem(titleBox));
            propPanel.Items.Add(new StackLayoutItem(new Label { Text = "Importer:" }));
            propPanel.Items.Add(new StackLayoutItem(classBox));
            propPanel.Items.Add(new StackLayoutItem(new Label { Text = "Descriptor:" }));
            propPanel.Items.Add(new StackLayoutItem(descriptorBox));
            propPanel.Items.Add(null);
            propPanel.Items.Add(new StackLayoutItem(managePanel));

            titleBox.TextBinding.Bind(list.SelectedValueBinding.Convert(x => (x as ValueHoldingItem<CollectionDescriptor>)?.Value.Title, x => (list.SelectedValue as ValueHoldingItem<CollectionDescriptor>)?.MutateValue(z => z.WithTitle(x))));

            classBox.Items.AddRange(importers);
            classBox.SelectedKeyBinding.Bind(list.SelectedValueBinding.Convert(x => (x as ValueHoldingItem<CollectionDescriptor>)?.Value.ImporterTypeName, x => (list.SelectedValue as ValueHoldingItem<CollectionDescriptor>)?.MutateValue(z => z.WithImporter(x))));

            descriptorBox.TextBinding.Bind(list.SelectedValueBinding.Convert(x => (x as ValueHoldingItem<CollectionDescriptor>)?.Value.Descriptor, x => (list.SelectedValue as ValueHoldingItem<CollectionDescriptor>)?.MutateValue(z => z.WithDescriptor(x))));

            list.SelectedIndexChanged += (sender, e) =>
                {
                    var value = list.SelectedIndex >= 0;
                    titleBox.Enabled = value;
                    classBox.Enabled = value;
                    descriptorBox.Enabled = value;
                    removeButton.Enabled = value;
                };

            var enableBox = new CheckBox { Text = "Enable", Checked = AutoImporter.IsEnabled };

            var mainLayout = new StackLayout { Spacing = 6, Orientation = Orientation.Horizontal, VerticalContentAlignment = VerticalAlignment.Stretch };
            mainLayout.Items.Add(new StackLayoutItem(listPanel));
            mainLayout.Items.Add(new StackLayoutItem(propPanel, true));

            var layout = new StackLayout { HorizontalContentAlignment = HorizontalAlignment.Stretch };
            layout.Items.Add(new StackLayoutItem(enableBox));
            layout.Items.Add(new StackLayoutItem(mainLayout, true));

            Applicators.Add(() =>
                {
                    AutoImporter.Descriptors.Clear();
                    AutoImporter.Descriptors.AddRange(list.Items.Select(x => x.Unwrap<CollectionDescriptor>()));
                    AutoImporter.IsEnabled = enableBox.Checked.Value;
                });

            return new TabPage { Content = layout, Text = "Auto Import", Padding = new Padding(8) };
        }
    }
}

