﻿using System;
using Eto.Forms;

namespace Breezy.PantEX.GUI.Preferences
{
    public class SlideshowPreferecesPageHandler : AbstractPreferencesPageHandler
    {
        public SlideshowPreferecesPageHandler()
        {
        }

        public override TabPage CreatePage()
        {
            var layout = new StackLayout { Padding = 6, Spacing = 6, HorizontalContentAlignment = HorizontalAlignment.Stretch };

            var intervalBox = new TextBox { Text = SlideshowController.Interval.ToString() };
            var intervalColumn = new StackLayout
                { 
                    Spacing = 6, 
                    Orientation = Orientation.Horizontal,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Items =
                        { 
                            new StackLayoutItem(new Label { Text = "Interval (in ms):" }),
                            new StackLayoutItem(intervalBox)
                        } 
                    };

            var randomBox = new CheckBox { Text = "Randomize", Checked = SlideshowController.Randomized };

            layout.Items.Add(new StackLayoutItem(intervalColumn));
            layout.Items.Add(new StackLayoutItem(randomBox));

            Applicators.Add(() => SlideshowController.Interval = Double.Parse(intervalBox.Text));
            Applicators.Add(() => SlideshowController.Randomized = randomBox.Checked.Value);

            return new TabPage { Content = layout, Text = "Slideshow" };
        }
    }
}

