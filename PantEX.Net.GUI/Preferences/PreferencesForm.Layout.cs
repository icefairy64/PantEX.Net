﻿using System;
using System.Linq;
using Eto.Forms;
using Eto.Drawing;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI.Preferences
{
    public partial class PreferencesForm : Form
    {
        private void InitLayout()
        {
            Title = "Preferences";
            ClientSize = new Size(640, 480);

            var tabs = new TabControl();

            var bottomPanel = new StackLayout { Orientation = Orientation.Horizontal };
            bottomPanel.Items.Add(null);
            bottomPanel.Items.Add(new StackLayoutItem(new Button { Text = "&Save", Command = new Command((s, e) => ApplyChanges()) }));

            var layout = new TableLayout() { Padding = new Padding(8), Spacing = new Size(0, 6) };
            layout.Rows.Add(new TableRow(new TableCell(tabs)) { ScaleHeight = true });
            layout.Rows.Add(new TableRow(new TableCell(bottomPanel)));

            Content = layout;

            PageHandlers.AddRange(Plugin.ProvidedTypes[typeof(AbstractPreferencesPageHandler)].Select(x => Plugin.CreateInstance<AbstractPreferencesPageHandler>(x)));
            PageHandlers.ForEach(x => tabs.Pages.Add(x.CreatePage()));
        }
    }
}

