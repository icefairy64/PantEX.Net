﻿using System;
using Breezy.Assistant.Extensions;
using Eto.Forms;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI.Preferences
{
    [Extendable]
    public abstract class AbstractPreferencesPageHandler
    {
        protected readonly List<Action> Applicators;

        public AbstractPreferencesPageHandler()
        {
            Applicators = new List<Action>();
        }

        public abstract TabPage CreatePage();

        public void ApplyChanges()
        {
            foreach (var a in Applicators)
                a();
        }
    }
}

