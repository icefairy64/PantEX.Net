﻿using System;
using Eto.Forms;

namespace Breezy.PantEX.GUI.Preferences
{
    public class GeneralPreferencesPageHandler : AbstractPreferencesPageHandler
    {
        public GeneralPreferencesPageHandler()
            : base()
        {
        }

        public override TabPage CreatePage()
        {
            var backScheduleBox = new CheckBox { Text = "Allow running some tasks on separate thread", Checked = TaskEX.AllowScheduleOnSeparateThread };
            var interactiveAdditionBox = new CheckBox { Text = "Interactive image addition", Checked = CollectionManager.IsAdditionInteractive };

            var layout = new StackLayout { Padding = 6, Spacing = 6, HorizontalContentAlignment = HorizontalAlignment.Stretch };
            layout.Items.Add(new StackLayoutItem(backScheduleBox));
            layout.Items.Add(new StackLayoutItem(interactiveAdditionBox));

            Applicators.Add(() =>
                {
                    TaskEX.AllowScheduleOnSeparateThread = backScheduleBox.Checked.Value;
                    CollectionManager.IsAdditionInteractive = interactiveAdditionBox.Checked.Value;
                });

            return new TabPage { Content = layout, Text = "General" };
        }
    }
}

