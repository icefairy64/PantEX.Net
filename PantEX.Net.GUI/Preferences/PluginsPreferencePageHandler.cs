﻿using System;
using System.Linq;
using Eto.Forms;
using Breezy.Assistant.Extensions;
using Eto.Drawing;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI.Preferences
{
    public class PluginsPreferencePageHandler : AbstractPreferencesPageHandler
    {
        public PluginsPreferencePageHandler()
            : base()
        {
        }

        public override TabPage CreatePage()
        {
            var layout = new StackLayout { Padding = 6, Spacing = 6, VerticalContentAlignment = VerticalAlignment.Stretch, Orientation = Orientation.Horizontal };
            var page = new TabPage { Content = layout, Text = "Plugins" };

            var loadPlugins = new List<string>(MainForm.Config.Plugins);

            var list = new ListBox();
            list.Items.AddRange(Plugin.PluginList.Where(x => !x.IsSystem).Select(x => x.WrapInListItem()));

            var listPanel = new StackLayout { MinimumSize = new Size(200, 0), HorizontalContentAlignment = HorizontalAlignment.Stretch };
            listPanel.Items.Add(new StackLayoutItem(list, true));

            var nameBox = new Label { Font = new Font(SystemFont.Bold) };
            var versionBox = new Label();
            var providesBox = new Label();
            var loadBox = new CheckBox { Text = "Load on startup", Enabled = false };

            var addButton = new Button
                {
                    Text = "Load",
                    Command = new Command((s, a) =>
                        {
                            var d = new OpenFileDialog();
                            d.Filters.Add(new FileDialogFilter("Assembly", ".exe", ".dll"));
                            var res = d.ShowDialog(page);
                            if (res == DialogResult.Ok)
                            {
                                var p = Plugin.Register(d.FileName);
                                list.Items.Add(p.WrapInListItem());
                            }
                        })
                };

            var toolBox = new StackLayout { Orientation = Orientation.Horizontal, VerticalContentAlignment = VerticalAlignment.Center };
            toolBox.Items.Add(new StackLayoutItem(loadBox));
            toolBox.Items.Add(null);
            toolBox.Items.Add(new StackLayoutItem(addButton));

            var detailsMain = new StackLayout { Spacing = 6, HorizontalContentAlignment = HorizontalAlignment.Stretch };
            detailsMain.Items.Add(new StackLayoutItem(nameBox));
            detailsMain.Items.Add(new StackLayoutItem(versionBox));
            detailsMain.Items.Add(new StackLayoutItem(providesBox, true));

            var detailsPanel = new StackLayout { Spacing = 6, HorizontalContentAlignment = HorizontalAlignment.Stretch };
            detailsPanel.Items.Add(new StackLayoutItem(new Scrollable { Content = detailsMain, Padding = 4, ExpandContentHeight = true }, true));
            detailsPanel.Items.Add(toolBox);

            nameBox.TextBinding.Bind(list.SelectedValueBinding.Convert(x => (x as ValueHoldingItem<Plugin>)?.Value.Name));
            versionBox.TextBinding.Bind(list.SelectedValueBinding.Convert(x => x == null ? "" : "Version: " + (x as ValueHoldingItem<Plugin>).Value.Assembly.GetName().Version.ToString()));
            providesBox.TextBinding.Bind(list.SelectedValueBinding.Convert(x => (x as ValueHoldingItem<Plugin>)?.Value.Provides.Where(z => z.Value.Count() > 0).Select(z => z.Value.Aggregate("\n\t" + z.Key.Name + ":", (a, t) => a + $"\n\t\t - {t.Name}")).Aggregate("Provides: ", (a, t) => a + t)));

            loadBox.CheckedBinding.Bind(list.SelectedValueBinding.Convert
                (
                    x => new bool?(loadPlugins.Any(z => x == null ? false : (x as ValueHoldingItem<Plugin>).Value.Filename.Contains(z))),
                    x => 
                    {
                        Predicate<string> pred = z => list.SelectedIndex < 0 ? false : (list.SelectedValue as ValueHoldingItem<Plugin>).Value.Filename.Contains(z);
                        var c = loadPlugins.Any(z => pred(z));
                        if (x.Value && !c)
                            loadPlugins.Add((list.SelectedValue as ValueHoldingItem<Plugin>)?.Value.Filename);
                        if (!x.Value && c)
                            loadPlugins.RemoveAll(pred);
                        return list.SelectedValue;
                    }
                ));
            
            layout.Items.Add(new StackLayoutItem(listPanel));
            layout.Items.Add(new StackLayoutItem(detailsPanel, true));

            Applicators.Add(() =>
                {
                    MainForm.Config.Plugins.Clear();
                    MainForm.Config.Plugins.AddRange(loadPlugins);
                });

            list.SelectedIndexChanged += (sender, e) =>
                {
                    var value = list.SelectedIndex >= 0;
                    loadBox.Enabled = value;
                };

            return page;
        }
    }
}

