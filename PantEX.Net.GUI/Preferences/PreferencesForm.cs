﻿using System;
using Eto.Forms;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI.Preferences
{
    public partial class PreferencesForm : Form
    {
        private List<AbstractPreferencesPageHandler> PageHandlers;

        public PreferencesForm()
        {
            PageHandlers = new List<AbstractPreferencesPageHandler>();
            InitLayout();
        }

        private void ApplyChanges()
        {
            PageHandlers.ForEach(x => x.ApplyChanges());
            MainForm.Config.Save();
        }
    }
}

