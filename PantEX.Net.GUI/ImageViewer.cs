﻿using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using Breezy.Venus;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using NLog;

namespace Breezy.PantEX.GUI
{
    public static class ImageViewer
    {
        private readonly static Logger Logger = LogManager.GetCurrentClassLogger();

        private static Timer Timer;
        private static EXImage CurrentImage;
        private static Image[] Raw;
        private static uint CurrentFrame;

        public static EXCursor Cursor { get; private set; }

        static ImageViewer()
        {
            Timer = new Timer(100);
            Timer.Elapsed += (sender, e) => HandleTimer();
            Cursor = new EXCursor();

            Cursor.Current.ValueChanged += (oldValue, newValue) => HandleImageChange(newValue);
        }

        private static void HandleImageChange(EXImage image)
        {
            image.Thumb.GetImageAsync()
                .ContinueWith(x => SetImage(x.Result, 0))
                .ContinueWith(async x => await image.GetImageAsync()).Unwrap()
                .ContinueWith(x => x.Result.FrameData.Select((z, i) => ImageFormatHelper.LoadImage(x.Result, i)))
                .ContinueWith(x => Start(x.Result.ToArray()))
                .ContinueWith(x =>
                    {
                        Logger.Error(x.Exception);
                        Application.Instance.InvokeSafe(() => MainForm.IsImageLoading = false);
                    },
                    TaskContinuationOptions.OnlyOnFaulted);
        }

        public static void SetImage(EXImage image, IEnumerable<EXImage> context)
        {
            Logger.Trace($"Setting image: {image}");

            if (image == CurrentImage)
                return;

            Timer.Stop();
            CurrentFrame = 0;
            CurrentImage = image;
            Application.Instance.InvokeSafe(() => MainForm.IsImageLoading = true);

            Cursor.Load(context);
            Cursor.MoveTo(image);
        }

        private static void Start(Image[] image)
        {
            Raw = image;
            if (image.Length > 1)
                Timer.Start();
            Application.Instance.InvokeSafe(() => MainForm.IsImageLoading = false);
            Logger.Trace("Before setting image");
            Application.Instance.Invoke(() => MainForm.CurrentImage.Value = Raw[0]);
            Logger.Trace("After setting image");
        }

        private static void HandleTimer()
        {
            CurrentFrame = (CurrentFrame + 1) % (uint)Raw.Length;
            Application.Instance.AsyncInvoke(() => MainForm.CurrentImage.Value = Raw[CurrentFrame]);
        }

        private static Task SetImage(RawImage raw, uint frame)
        {
            return Task.Run(() => ImageFormatHelper.LoadImage(raw, (int)frame))
                .ContinueWith(x => Application.Instance.AsyncInvoke(() => MainForm.CurrentImage.Value = x.Result));
        }
    }
}

