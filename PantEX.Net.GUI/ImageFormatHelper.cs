﻿using System;
using Eto.Drawing;
using Eto.Forms;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using Breezy.Assistant;
using Imazen.WebP;
using System.IO;
using Breezy.Venus;

namespace Breezy.PantEX.GUI
{
    public static class ImageFormatHelper
    {
        private static SimpleEncoder WebPEncoder = new SimpleEncoder();

        public static Bitmap LoadImage(EXImage img, int frame = 0)
        {
            var bmp = img.GetImage();
            return LoadImage(bmp, frame);
        }

        public static async Task<Bitmap> LoadImageAsync(EXImage img, int frame = 0)
        {
            var bmp = await img.GetImageAsync();
            return await Task.Run(() => LoadImage(bmp, frame));
        }

        public static Bitmap LoadImage(RawImage src, int frame = 0)
        {
            Eto.Drawing.PixelFormat format;
            switch (src.Format)
            {
                case RawPixelFormat.Rgb24:
                    format = Eto.Drawing.PixelFormat.Format24bppRgb;
                    break;
                case RawPixelFormat.Argb32:
                    format = Eto.Drawing.PixelFormat.Format32bppRgba;
                    break;
                default:
                    format = Eto.Drawing.PixelFormat.Format24bppRgb;
                    break;
            }

            var bmp = Application.Instance.InvokeSafe(() => new Bitmap((int)src.Width, (int)src.Height, format));
            var data = bmp.Lock();

            switch (src.Format)
            {
                case RawPixelFormat.Rgb24:
                    ToEtoRgb24(src.FrameData[frame], data, (int)src.Width, (int)src.Height);
                    break;
                case RawPixelFormat.Argb32:
                    ToEtoArgb32(src.FrameData[frame], data, (int)src.Width, (int)src.Height);
                    break;
                default:
                    throw new Exception($"Unsupported image format {src.Format}");
            }

            return bmp;
        }     

        private static unsafe void ToEtoRgb24(byte[] source, Eto.Drawing.BitmapData target, int width, int height)
        {
            fixed (void* src = &source[0])
            {
                var psrc = (byte*)src;
                var pdln = (byte*)target.Data.ToPointer();

                for (int y = 0; y < height; y++)
                {
                    var pdst = pdln;

                    for (int x = 0; x < width; x++)
                    {
                        var data = (int)(*((uint*)psrc) | 0xff000000);
                        *((int*)pdst) = target.TranslateArgbToData(data);
                        pdst += 3;
                        psrc += 3;
                    }

                    pdln += target.ScanWidth;
                }
            }
        }

        private static unsafe void ToEtoArgb32(byte[] source, Eto.Drawing.BitmapData target, int width, int height)
        {
            fixed (void* src = &source[0])
            {
                var psrc = (int*)src;
                var pdln = (byte*)target.Data.ToPointer();

                for (int y = 0; y < height; y++)
                {
                    var pdst = (uint*)pdln;

                    for (int x = 0; x < width; x++)
                    {
                        var data = *(psrc++);
                        *((int*)pdst++) = target.TranslateArgbToData(data);
                    }

                    pdln += target.ScanWidth;
                }
            }
        }
    }
}

