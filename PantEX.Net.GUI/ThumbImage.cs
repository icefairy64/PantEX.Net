﻿using System;
using System.Threading.Tasks;
using System.IO;
using Breezy.Assistant;
using System.Drawing;
using System.Drawing.Imaging;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Images
{
    public class ThumbImage : EXImage
    {
        private EXImage Prototype;
        private int Size;
        private byte[] Data;
        private string OriginalFilename;

        public ThumbImage(EXImage prototype, int size = 150)
        {
            Prototype = prototype;
            OriginalFilename = Prototype.Filename;
            Size = size;
            Data = null;
        }

        private async Task GenerateThumb()
        {
            var cbmp = await ImageFormatHelper.LoadImageAsync(Prototype);
            using (var ms = new MemoryStream())
            {
                cbmp.Save(ms, Eto.Drawing.ImageFormat.Jpeg);
                ms.Seek(0, SeekOrigin.Begin);
                var bmp = new Bitmap(ms);
                var thumb = bmp.GetThumbnailImage(Size, (int)((double)Size * bmp.Height / bmp.Width), () => false, IntPtr.Zero);
                using (var stream = new MemoryStream())
                {
                    thumb.Save(stream, ImageFormat.Png);
                    stream.Seek(0, SeekOrigin.Begin);
                    Data = stream.ReadAll();
                }
            }
        }

        public override async Task<Stream> GetDataStreamAsync()
        {
            if (Data == null)
                await GenerateThumb();
            return new MemoryStream(Data);
        }

        public override Stream DataStream
        {
            get
            {
                if (Data == null)
                    GenerateThumb().Wait();
                return new MemoryStream(Data);
            }
        }
    }
}

