﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using Eto.Forms;
using NLog;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI
{
    public static class CollectionUpdater
    {
        private readonly static ILogger Logger = LogManager.GetCurrentClassLogger();

        public static CollectionUpdateTask Update(Collection col)
        {
            var type = Plugin.GetType<ICollectionUpdater>(col.Descriptor.ImporterTypeName);
            var instance = Plugin.CreateInstance<ICollectionUpdater>(type);
            instance.Configure(col);

            var task = new CollectionUpdateTask(col, instance);

            TaskEX.Schedule(task);
            return task;
        }
    }

    public class CollectionUpdateTask : AbstractTask<Collection>
    {
        private readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private readonly Collection TargetCollection;
        private readonly ICollectionUpdater Updater;

        public CollectionUpdateTask(Collection targetCollection, ICollectionUpdater updater)
        {
            TargetCollection = targetCollection;
            Updater = updater;
            Title = $"Updating collection {targetCollection.Title}";
        }

        protected override async Task Execute()
        {
            double i = 0;
            var exceptions = new List<Exception>();
            foreach (var upd in TargetCollection.Modifications)
            {
                try
                {
                    Logger.Trace($"Performing modification: {upd}");
                    switch (upd.Kind)
                    {
                        case CollectionModificationKind.ImageAddition:
                            await Updater.AddImage((EXImage)upd.Subject);
                            break;
                        case CollectionModificationKind.ImageRemoval:
                            await Updater.RemoveImage((EXImage)upd.Subject);
                            break;
                    }

                    HandleProgress(TargetCollection, i++ / TargetCollection.Modifications.Count);
                }
                catch (Exception e)
                {
                    exceptions.Add(new Exception($"Error while performing modification: {upd}", e));
                }
            }
            if (exceptions.Count > 0)
            {
                Logger.Error(new AggregateException(exceptions));
                throw new Exception("Error during collection update");
            }
            else
            {
                await Updater.WriteUpdates();
                TargetCollection.ClearModifications();
                HandleFinish(TargetCollection);
            }
        }


    }
}

