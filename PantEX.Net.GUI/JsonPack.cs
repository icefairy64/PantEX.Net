﻿using System;
using System.IO;
using System.Threading.Tasks;
using Eto.Forms;
using Breezy.Assistant.Extensions;
using Breezy.PantEX.Images;
using System.Linq;
using Breezy.Venus;
using Breezy.Assistant;
using System.Collections.Generic;

namespace Breezy.PantEX.GUI
{
    [Named("JsonPack")]
    public class JsonPack : ICollectionImporter, ICollectionExporter, ICollectionUpdater
    {
        private static string[] FallbackExtensions = { ".png", ".jpg", ".webp" };

        private JsonPackConfiguration Config;

        public JsonPack()
        {
            if (MainForm.Venus != null)
                FallbackExtensions = MainForm.Venus.Importers.Select(x => x.Format.Extensions).Aggregate(new List<string>(), (a, x) => a.AddRangeChainable(x)).ToArray();
        }

        public string Title
        {
            get { return "JsonPack"; }
        }

        public void Configure()
        {
            var d = new SelectFolderDialog();
            if (d.ShowDialog(CollectionManager.Instance) == DialogResult.Ok)
                Config = new JsonPackConfiguration { Directory = new DirectoryInfo(d.Directory), ThumbsDirectory = new DirectoryInfo(Path.Combine(d.Directory, "thumbs")) };
        }

        public void Configure(CollectionDescriptor des)
        {
            if (String.IsNullOrEmpty(des.Descriptor))
                Configure();
            else
                Config = new JsonPackConfiguration { Directory = new DirectoryInfo(des.Descriptor), ThumbsDirectory = new DirectoryInfo(Path.Combine(des.Descriptor, "thumbs")) };
        }

        private EXImage Load(string dir, string filename, string thumbDir, string thumbName)
        {
            EXImage img;

            // Main image

            var mainFile = new FileInfo(Path.Combine(dir, filename));
            var i = 0;
            while (!mainFile.Exists)
            {
                if (i >= FallbackExtensions.Length)
                    break;
                mainFile = new FileInfo(Path.ChangeExtension(mainFile.FullName, FallbackExtensions[i++]));
            }

            if (!mainFile.Exists || mainFile.Length == 0)
            {
                img = new FileImage(FileManager.ImagePlaceholderPath);
                img.Filename = Path.ChangeExtension(filename, FileManager.ImagePlaceholder.Extension);
            }
            else
                img = new Images.FileImage(mainFile.FullName);

            // Thumbnail
            
            var thumbFile = new FileInfo(Path.Combine(thumbDir, thumbName));
            i = 0;
            while (!thumbFile.Exists)
            {
                if (i >= FallbackExtensions.Length)
                    break;
                thumbFile = new FileInfo(Path.ChangeExtension(thumbFile.FullName, FallbackExtensions[i++]));
            }

            if (!thumbFile.Exists || thumbFile.Length == 0)
                img.Thumb = new ThumbImage(img);
            else
                img.Thumb = new FileImage(thumbFile.FullName);
            
            return img;
        }

        public async Task<Collection> Import(ProgressHandler progHandler)
        {
            var col = new Collection(Config.Directory.Name);
            var thumbs = Path.Combine(Config.Directory.FullName, "thumbs");
            using (var stream = new FileStream(Path.Combine(Config.Directory.FullName, "collection.json"), FileMode.Open))
            {
                await PantEX.JsonPack.Load(col, async (imgFilename, thumbFilename, progress) => 
                    {
                        var data = Load(Config.Directory.FullName, imgFilename, thumbs, thumbFilename);
                        progHandler?.Invoke(progress);
                        return data;
                    }, stream);
                col.Descriptor = new CollectionDescriptor { Title = col.Title, Descriptor = Config.Directory.FullName, ImporterTypeName = GetType().FullName };
                return col;
            }
        }

        public async Task Export(Collection collection, ProgressHandler progHandler)
        {
            var forcedExporter = GetForcedExporter(collection);

            if (!Config.Directory.Exists)
                Config.Directory.Create();
            var thumbDir = new DirectoryInfo(Path.Combine(Config.Directory.FullName, "thumbs"));
            if (!thumbDir.Exists)
                thumbDir.Create();
            var data = await PantEX.JsonPack.Save(collection, async (img, progress) =>
                {
                    var imgFilename = Path.Combine(Config.Directory.FullName, img.Filename);

                    if (!File.Exists(imgFilename))
                    {
                        using (var stream = new FileStream(imgFilename, FileMode.Create))
                            await WriteImage(img, stream, forcedExporter);
                    }

                    imgFilename = Path.Combine(thumbDir.FullName, img.Thumb.Filename);

                    if (!File.Exists(imgFilename))
                    {
                        using (var stream = new FileStream(imgFilename, FileMode.Create))
                            await WriteImage(img.Thumb, stream, forcedExporter);
                    }
                    progHandler?.Invoke(progress);
                });
            using (var writer = new StreamWriter(new FileStream(Path.Combine(Config.Directory.FullName, "collection.json"), FileMode.Create)))
                await writer.WriteLineAsync(data);
        }

        public CollectionDescriptor GenerateDescriptor()
        {
            return new CollectionDescriptor { Title = Config.Directory.Name, ImporterTypeName = GetType().FullName, Descriptor = Config.Directory.FullName };
        }

        private IImageExporter GetForcedExporter(Collection collection)
        {
            var forcedFormat = collection.GetForcedFormat();
            if (!string.IsNullOrEmpty(forcedFormat))
                return MainForm.Venus.FindExporterByExtension(forcedFormat);
            return null;
        }

        #region ICollectionUpdater implementation

        private Collection Collection;
        private IImageExporter ForcedExporter;

        public void Configure(Collection collection)
        {
            Collection = collection;
            Configure(collection.Descriptor);
            ForcedExporter = GetForcedExporter(Collection);
        }

        public async Task AddImage(EXImage image)
        {
            PantEX.JsonPack.HandleNaming(image, ForcedExporter?.Format.Extensions.First());

            var fn = Path.Combine(Config.Directory.FullName, image.Filename);

            if (!File.Exists(fn))
            {
                using (var os = new FileStream(fn, FileMode.Create))
                    await WriteImage(image, os, ForcedExporter);
            }

            fn = Path.Combine(Config.ThumbsDirectory.FullName, image.Thumb.Filename);

            if (!File.Exists(fn))
            {
                using (var os = new FileStream(fn, FileMode.Create))
                    await WriteImage(image.Thumb, os, ForcedExporter);
            }
        }

        public async Task RemoveImage(EXImage image)
        {
            var file = new FileInfo(Path.Combine(Config.Directory.FullName, image.Filename));
            file.Delete();
            file = new FileInfo(Path.Combine(Config.ThumbsDirectory.FullName, image.Thumb.Filename));
            file.Delete();
            await Task.FromResult<object>(null);
        }

        public async Task WriteUpdates()
        {
            var json = PantEX.JsonPack.GenerateJson(Collection);
            using (var writer = new StreamWriter(new FileStream(Path.Combine(Config.Directory.FullName, "collection.json"), FileMode.Create)))
                await writer.WriteLineAsync(json);
        }

        #endregion

        private static async Task WriteImage(EXImage img, Stream os, IImageExporter exporter)
        {
            string ext = null;

            using (var s = await img.GetDataStreamAsync())
            {
                ext = MainForm.Venus.FindImporter(s).Format.Extensions.First();

                if (exporter != null && !exporter.Format.Extensions.Any(x => x.Equals(ext, StringComparison.InvariantCultureIgnoreCase)))
                    await Task.Run(() => exporter.Export(img.GetImage(), os));
                else
                {
                    await s.CopyToAsync(os);
                }
            }
        }
    }

    public class JsonPackConfiguration
    {
        public DirectoryInfo Directory;
        public DirectoryInfo ThumbsDirectory;
    }
}

