﻿using System;
using Eto.Drawing;
using Eto.Forms;

namespace Breezy.PantEX.GUI
{
    public class ImageView : Drawable
    {
        public Property<Image> Image { get; protected set; }
        public float ZoomRate { get; set; }

        public bool IsFaded
        {
            get { return Fade; }
            set
            {
                Fade = value;
                Invalidate();
            }
        }

        private float X, Y;
        private bool IsMouseDown;
        private PointF LastLocation;
        private float ZoomValue;
        private bool KeepZoom;
        private bool HorizontalZoom;
        private bool Fade = false;
        private Color FadeColor = new Color(0, 0, 0, 0.4f);

        public ImageView()
        {
            Image = new Property<Image>();
            Image.ValueChanged += (oldValue, newValue) => Reload(newValue);
            ZoomRate = 1.25f;
        }

        private void ValidateBounds()
        {
            if (Image.Value == null)
                return;

            if (Width > Image.Value.Width * ZoomValue)
            {
                if (X < -Width / 2)
                    X = -Width / 2;

                if (X + Image.Value.Width * ZoomValue > Width / 2)
                    X = Width / 2 - Image.Value.Width * ZoomValue;
            }
            else
            {
                if (X > -Width / 2)
                    X = -Width / 2;

                if (X + Image.Value.Width * ZoomValue < Width / 2)
                    X = Width / 2 - Image.Value.Width * ZoomValue;
            }

            if (Height > Image.Value.Height * ZoomValue)
            {
                if (Y < -Height / 2)
                    Y = -Height / 2;
            
                if (Y + Image.Value.Height * ZoomValue > Height / 2)
                    Y = Height / 2 - Image.Value.Height * ZoomValue;
            }
            else
            {
                if (Y > -Height / 2)
                    Y = -Height / 2;

                if (Y + Image.Value.Height * ZoomValue < Height / 2)
                    Y = Height / 2 - Image.Value.Height * ZoomValue;
            }
        }

        protected void Reload(Image img)
        {
            var xz = (float)Width / img.Width;
            var yz = (float)Height / img.Height;
            ZoomValue = Math.Min(xz, yz);
            HorizontalZoom = xz == ZoomValue;
            X = -img.Width / 2 * ZoomValue;
            Y = -img.Height / 2 * ZoomValue;
            KeepZoom = true;
            Invalidate();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Buttons.HasFlag(MouseButtons.Primary))
            {
                if (!IsMouseDown)
                    IsMouseDown = true;
                else
                {
                    X += e.Location.X - LastLocation.X;
                    Y += e.Location.Y - LastLocation.Y;
                    ValidateBounds();
                    Invalidate();
                }
                LastLocation = e.Location;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            IsMouseDown = false;
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            var r = e.Delta.Height > 0 ? ZoomRate : 1 / ZoomRate;
            ZoomValue *= r;
            X *= r;
            Y *= r;
            KeepZoom = false;
            ValidateBounds();
            Invalidate();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            if (KeepZoom)
            {
                HorizontalZoom = (float)Image.Value.Width / Image.Value.Height > (float)Width / Height;

                if (HorizontalZoom)
                {
                    ZoomValue = Width / (float)Image.Value.Width;
                    Y = -Image.Value.Height * ZoomValue / 2;
                }
                else
                {
                    ZoomValue = Height / (float)Image.Value.Height;
                    X = -Image.Value.Width * ZoomValue / 2;
                }
            }

            if (Image.Value != null)
                ValidateBounds();
            base.OnSizeChanged(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(Colors.Black, e.ClipRectangle);

            if (Image.Value == null)
                return;

            e.Graphics.DrawImage(Image.Value, 
                new RectangleF(-(X + Width / 2) / ZoomValue, -(Y + Height / 2) / ZoomValue, Width / ZoomValue, Height / ZoomValue),
                new RectangleF(0, 0, Width, Height));

            //e.Graphics.DrawImage(Image.Value, X + Width / 2, Y + Height / 2, Image.Value.Width * ZoomValue, Image.Value.Height * ZoomValue);

            if (Fade)
                e.Graphics.FillRectangle(FadeColor, e.ClipRectangle);
        }
    }
}

