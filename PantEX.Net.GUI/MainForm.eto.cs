﻿using System;
using Eto.Forms;
using Eto.Drawing;
using Breezy.PantEX.GUI.Preferences;
using NLog;

namespace Breezy.PantEX.GUI
{
    public partial class MainForm : Form
    {
        public static Property<Image> CurrentImage { get; private set; } = new Property<Image>();

        private static ImageView ImageView;
        private static Spinner Spinner;
        private static ButtonMenuItem SourcesMenu;

        private bool IsQuitting = false;

        private string FormatSize(long bytes)
        {
            double size = bytes;
            var suffixes = new string[] { "B", "KB", "MB", "GB", "TB" };
            int i = 0;
            while (size >= 1024)
            {
                size /= 1024;
                if (++i == suffixes.Length - 1)
                    break;
            }
            return $"{size:0.##} {suffixes[i]}";
        }

        void InitializeComponent()
        {
            Title = "PantEX";

            Size = new Size(DefaultWindowW.Value, DefaultWindowH.Value);

            if (DefaultWindowX.HasValue)
                Location = new Point(DefaultWindowX.Value, Location.Y);

            if (DefaultWindowY.HasValue)
                Location = new Point(Location.X, DefaultWindowY.Value);

            Closing += (sender, e) =>
                {
                    DefaultWindowX = Location.X;
                    DefaultWindowY = Location.Y;
                    DefaultWindowW = Width;
                    DefaultWindowH = Height;
                };

            ImageView = new ImageView();
            ImageView.Image.LinkTo(CurrentImage);

            Spinner = new Spinner { Visible = false, BackgroundColor = Colors.Transparent };

            var status = new Label { Text = Status.Value };
            Status.ValueChanged += (oldValue, newValue) => status.Text = newValue;

            var layout = new StackLayout { HorizontalContentAlignment = HorizontalAlignment.Stretch };
            layout.Items.Add(new StackLayoutItem(ImageView, true));
            layout.Items.Add(new StackLayoutItem(new StackLayout { Items = { new StackLayoutItem(status) }, Padding = new Padding(4) }));

            Content = layout;

            var quitCommand = new Command { MenuText = "Quit", Shortcut = Application.Instance.CommonModifier | Keys.Q };
            quitCommand.Executed += (sender, e) => Application.Instance.Quit();

            var crashCommand = new Command { MenuText = "Crash" };
            crashCommand.Executed += (sender, e) =>
                {
                    throw new Exception("User-initiated exception throw");
                };
            
            var gcCommand = new Command { MenuText = "Force GC" };
            gcCommand.Executed += (sender, e) =>
                {
                    var before = GC.GetTotalMemory(false);
                    GC.Collect();
                    var after = GC.GetTotalMemory(true);
                    Logger.Info($"Forced GC completed; memory allocated before: {FormatSize(before)}, after: {FormatSize(after)}");
                };

            var fileMenu = new ButtonMenuItem { Text = "&File" };
            fileMenu.Items.Add(new ButtonMenuItem { Text = "Collections...", Command = new Command((s, e) => new CollectionManager().Show()) });

            SourcesMenu = new ButtonMenuItem { Text = "&Sources" };

            Menu = new MenuBar
            {
                Items =
                {
                    fileMenu,
                    SourcesMenu
                },
                ApplicationItems =
                {
                    new ButtonMenuItem { Text = "&Preferences...", Command = new Command((s, e) => new PreferencesForm().Show()) }
                },
                QuitItem = quitCommand,
                    AboutItem = new ButtonMenuItem(new Command((s, e) => new AboutForm().Show()) { MenuText = "About" }) 
            };

            Closing += (sender, e) =>
                {
                    if (IsQuitting)
                        return;
                    var result = MessageBox.Show("Really exit?", MessageBoxButtons.YesNo, MessageBoxType.Question);
                    if (result == DialogResult.Yes)
                    {
                        IsQuitting = true;
                        Application.Instance.Quit();
                    }
                    else
                        e.Cancel = true;
                };

            Menu.HelpMenu.Items.Add(new ButtonMenuItem { Text = "Force GC", Command = gcCommand });
            Menu.HelpMenu.Items.Add(new ButtonMenuItem { Text = "Crash", Command = crashCommand });
        }
    }
}
