﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Breezy.PantEX.GUI
{
    public class EXCursor
    {
        private readonly Random RndGenerator;

        private LinkedListNode<EXImage> FCurrent;

        private LinkedListNode<EXImage> FValue
        {
            set
            {
                FCurrent = value;
                Current.Value = value.Value;
            }
        }

        public LinkedList<EXImage> Underlying { get; private set; }
        public Property<EXImage> Current { get; private set; }

        public EXCursor()
        {
            Current = new Property<EXImage>();
            RndGenerator = new System.Random();
        }

        public void Load(IEnumerable<EXImage> images)
        {
            Underlying = new LinkedList<EXImage>(images);
            FCurrent = Underlying.First;
        }

        public void MoveTo(EXImage img)
        {
            FValue = Underlying.Find(img);
        }

        public void Forward()
        {
            if (Underlying.Last.Value == FCurrent.Value)
                FValue = Underlying.First;
            else
                FValue = FCurrent.Next;
        }

        public void Back()
        {
            if (Underlying.First.Value == FCurrent.Value)
                FValue = Underlying.Last;
            else
                FValue = FCurrent.Previous;
        }

        public void Random()
        {
            FValue = Underlying.Find(Underlying.ElementAt(RndGenerator.Next(Underlying.Count)));
        }
    }
}

