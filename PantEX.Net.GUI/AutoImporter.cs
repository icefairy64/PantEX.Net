﻿using System;
using System.Collections.Generic;
using System.Linq;
using Breezy.Assistant.Extensions;
using Newtonsoft.Json.Linq;

namespace Breezy.PantEX.GUI
{
    [Configurable("AutoImporter")]
    public static class AutoImporter
    {
        public static List<CollectionDescriptor> Descriptors = new List<CollectionDescriptor>();
        public static bool IsEnabled = true;

        static IDictionary<string, object> Config;

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            MainForm.OnStart += (sender, e) => Init(dict);
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {
            dict["Collections"] = Descriptors;
            dict["Enabled"] = IsEnabled;
        }

        static void Init(IDictionary<string, object> dict)
        {
            var list = (JArray)dict["Collections"];
            Descriptors.AddRange(list.Select(x => (CollectionDescriptor)x));
            IsEnabled = (bool)dict["Enabled"];

            if (IsEnabled)
            {
                foreach (var des in Descriptors)
                {
                    var importer = Plugin.CreateInstance<ICollectionImporter>(des.ImporterTypeName);
                    importer.Configure(des);
                    var task = new CollectionImportTask(importer, des.Title);
                    task.ResultProduced += (sender, args) => CollectionManager.Collections.Add(args.Result);
                    TaskEX.Schedule(task);
                }
            }

            if (Descriptors.Count == 0 || !IsEnabled)
                CollectionManager.Collections.Add(new Collection("Unnamed"));
        }
    }
}

