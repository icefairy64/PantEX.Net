﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Eto.Forms;
using Eto.Drawing;
using Breezy.PantEX.GUI;
using Breezy.Assistant.Extensions;
using Breezy.Assistant;
using NLog;

namespace Breezy.PantEX.GUI
{  
    [Configurable("CollectionManager")]
    public partial class CollectionManager : Form
    {
        public static CollectionManager Instance { get; private set; }

        private int LoadAmount = 8;
        private bool EndReached = false;
        private bool IsLoading = false;
        private IEnumerable<EXImage> Images;
        private ObservableCollection<EXImage> Selection;
        private List<Predicate<EXImage>> Filters;

        private readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        private Collection CurrentCollection
        {
            get { return CollectionList.GetSelectedValue<Collection>(); }
        }

        private IEnumerable<EXImage> FilteredImages
        {
            get { return CurrentCollection.Images.Where(x => Filters.All(z => z(x))); }
        }

        public CollectionManager()
        {
            InitializeLayout();
            Selection = new ObservableCollection<EXImage>();
            Filters = new List<Predicate<EXImage>>();

            CollectionList.HookTo(Collections);

            Collections.CollectionChanged += (sender, e) => 
                {
                    if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                    {
                        foreach (var obj in e.NewItems)
                        {
                            var col = obj as Collection;
                            col.ImagesRemoved += HandleImageRemoval;
                            col.ImagesAdded += HandleImageAdditionAsync;
                        }
                    }
                };

            CollectionList.SelectedIndexChanged += (sender, e) => ChangeCollection(CurrentCollection);
            Controller.LoadRequested += HandleLoadRequest;

            foreach (var col in Collections)
            {
                col.ImagesRemoved += HandleImageRemoval;
                col.ImagesAdded += HandleImageAdditionAsync;
            }

            Closing += (sender, e) => 
                {
                    foreach (var col in Collections)
                    {
                        col.ImagesRemoved -= HandleImageRemoval;
                        col.ImagesAdded -= HandleImageAdditionAsync;
                    }

                    Instance = null;
                };

            Instance = this;
        }

        private void ForceReload()
        {
            ChangeCollection(CurrentCollection);
        }

        private void ChangeCollection(Collection value)
        {
            EndReached = false;
            Controller.RemoveAll();
            Images = value.Images.Reverse().StateSaving();
        }

        private void HandleThumbAddition(ThumbBox box)
        {
            Application.Instance.InvokeSafe(() => HandleThumbAdditionUnsafe(box));
        }

        private void HandleThumbAdditionUnsafe(ThumbBox box)
        {
            box.Selected.Value = Selection.Contains(box.InnerImage);

            box.Selected.ValueChanged += (oldValue, newValue) => 
                {
                    if (newValue && !oldValue)
                        Selection.Add(box.InnerImage);
                    if (!newValue && oldValue)
                        Selection.Remove(box.InnerImage);
                };

            box.MouseUp += (ss, e) => 
                {
                    if (e.Buttons.HasFlag(MouseButtons.Alternate))
                        ImageViewer.SetImage(box.InnerImage, FilteredImages);
                
                    if (e.Buttons.HasFlag(MouseButtons.Primary))
                    {
                        if (e.Modifiers.HasFlag(Keys.Control))
                            box.Selected.Value = !box.Selected;
                        else
                        {
                            foreach (var b in ThumbContainer.Controls.Cast<ThumbBox>())
                            {
                                if (b != box)
                                    b.Selected.Value = false;
                            }
                            box.Selected.Value = true;

                            Selection.Clear();
                            Selection.Add(box.InnerImage);
                        }
                    }
                };
        }

        private void HandleImageRemoval(object sender, CollectionModificationArgs args)
        {
            if (!IsAdditionInteractive)
                return;

            if (CurrentCollection == args.ModifiedCollection)
                Controller.Remove(args.Items);
        }

        private void HandleImageAddition(object sender, CollectionModificationArgs args)
        {
            if (!IsAdditionInteractive)
                return;

            if (CurrentCollection == args.ModifiedCollection)
            {
                var loaded = args.Items.Select(img => new Tuple<EXImage, Bitmap>(img, ImageFormatHelper.LoadImage(img.Thumb)));

                foreach (var img in loaded)
                {
                    Controller.Add(img.Item1, img.Item2, true)
                              .ContinueOnGUIWith(x => HandleThumbAdditionUnsafe(x.Result));
                }
            }
        }

        private void HandleImageAdditionAsync(object sender, CollectionModificationArgs args)
        {
            if (!IsAdditionInteractive)
                return;

            if (CurrentCollection == args.ModifiedCollection)
                args.Items.MapParAsync(LoadThumb, 16);
        }

        private async Task<EXImage> LoadThumb(EXImage img)
        {
            var bmp = await ImageFormatHelper.LoadImageAsync(img.Thumb);
            var box = await Controller.Add(img, bmp, true);
            HandleThumbAddition(box);
            return img;
        }

        private void HandleLoadRequest(object sender, EventArgs args)
        {
            if (CollectionList.SelectedIndex >= 0 && !EndReached && !IsLoading)
            {
                IsLoading = true;

                var task = new CustomTask<IEnumerable<KeyValuePair<EXImage, Image>>>(async (ph) =>
                    {
                        var imgs = Images.Where(x => Filters.All(p => p.Invoke(x))).Take(LoadAmount).ToList();
                        var prop = new Property<double>();
                        var count = imgs.Count;

                        var res = await imgs.MapParAsync(async (x) => 
                            {
                                var img = await ImageFormatHelper.LoadImageAsync(x.Thumb);
                                var result = new KeyValuePair<EXImage, Image>(x, img);

                                prop.Value += 1;
                                ph?.Invoke(prop.Value / count, result.ToEnumerable());

                                return result;
                            }, 4);
                        
                        return res;
                    }, "Loading thumbnails");

                task.ProgressChanged += (s, a) =>
                    {
                        foreach (var p in a.Value)
                        {
                            Controller.Add(p.Key, p.Value)
                                      .ContinueOnGUIWith(x => HandleThumbAdditionUnsafe(x.Result));
                        }
                    };
                
                task.ResultProduced += (s, a) => 
                    {
                        if (a.Result.Count() == 0)
                            EndReached = true;
                        IsLoading = false;
                    };
                
                IsLoading = true;
                Task.Run(() => task.Run());
            }
        }

        // Static members

        public static ObservableCollection<Collection> Collections { get; private set; } = new ObservableCollection<Collection>(); 

        public static bool IsAdditionInteractive;

        private static int? DefaultWindowX;
        private static int? DefaultWindowY;
        private static int? DefaultWindowW = 800;
        private static int? DefaultWindowH = 600;
        private static int? DefaultThumbSize = 150;
        private static int? DefaultThumbCount = 4;

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            IsAdditionInteractive = (bool)dict["InteractiveAddition"];

            DefaultWindowX = dict.ContainsKey("WindowX") ? (int?)((long)dict["WindowX"]) : DefaultWindowX;
            DefaultWindowY = dict.ContainsKey("WindowY") ? (int?)((long)dict["WindowY"]) : DefaultWindowY;
            DefaultWindowW = dict.ContainsKey("WindowW") ? (int?)((long)dict["WindowW"]) : DefaultWindowW;
            DefaultWindowH = dict.ContainsKey("WindowH") ? (int?)((long)dict["WindowH"]) : DefaultWindowH;
            DefaultThumbSize = dict.ContainsKey("ThumbSize") ? (int?)((long)dict["ThumbSize"]) : DefaultThumbSize;
            DefaultThumbCount = dict.ContainsKey("ThumbCount") ? (int?)((long)dict["ThumbCount"]) : DefaultThumbCount;
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            dict["InteractiveAddition"] = IsAdditionInteractive;

            if (DefaultWindowX.HasValue)
                dict["WindowX"] = DefaultWindowX.Value;
            if (DefaultWindowY.HasValue)
                dict["WindowY"] = DefaultWindowY.Value;
            if (DefaultWindowW.HasValue)
                dict["WindowW"] = DefaultWindowW.Value;
            if (DefaultWindowH.HasValue)
                dict["WindowH"] = DefaultWindowH.Value;
            if (DefaultThumbSize.HasValue)
                dict["ThumbSize"] = DefaultThumbSize.Value;
            if (DefaultThumbCount.HasValue)
                dict["ThumbCount"] = DefaultThumbCount.Value;
        }
    }

    [CanRunInBackground]
    public class CollectionImportTask : AbstractTask<Collection>
    {
        private ICollectionImporter Importer;
        private string CollectionTitle;

        public CollectionImportTask(ICollectionImporter importer)
        {
            Importer = importer;
            Title = "Importing collection";
        }

        public CollectionImportTask(ICollectionImporter importer, string collectionTitle)
        {
            Importer = importer;
            Title = $"Importing collection {collectionTitle}";
            CollectionTitle = collectionTitle;
        }

        protected override async Task Execute()
        {
            var counter = 0;
            var collection = await Importer.Import(x =>
                {
                    if (((int)(x * 1000)) - counter > 10)
                    {
                        HandleProgress(null, x);
                        counter = (int)(x * 1000);
                    }
                });
            
            Title = $"Importing collection {collection.Title}";
            if (CollectionTitle != null)
                collection.Title = CollectionTitle;
            HandleFinish(collection);
        }
    }

    [CanRunInBackground]
    public class CollectionExportTask : AbstractTask<Collection>
    {
        private ICollectionExporter Exporter;
        private Collection Collection;

        public CollectionExportTask(ICollectionExporter exporter, Collection collection)
        {
            Exporter = exporter;
            Collection = collection;
            Title = String.Format($"Exporting collection {collection.Title}");
        }

        protected override async Task Execute()
        {
            await Exporter.Export(Collection, x => HandleProgress(null, x));
            HandleFinish();
        }
    }

    public class ActionTask : AbstractTask<object>
    {
        private IAction Action;

        public ActionTask(IAction action)
        {
            Action = action;
            Title = $"Performing action: {action.Title}";
        }

        protected override async Task Execute()
        {
            await Action.Perform(x => HandleProgress(null, x));
            HandleFinish();
        }        
    }
}

