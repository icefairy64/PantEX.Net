﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;
using Eto.Forms;
using Eto.Drawing;
using Breezy.PantEX;
using Breezy.Assistant;
using Breezy.Assistant.Extensions;
using Newtonsoft.Json;
using System.Drawing.Imaging;
using NLog;
using System.Threading.Tasks;
using System.Threading;
using Breezy.Venus;
using Breezy.Venus.FormatHandlers;

namespace Breezy.PantEX.GUI
{
    [Configurable("MainWindow")]
    public partial class MainForm
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public static Settings Config { get; private set; }
        public static int UIThreadId { get; private set; }
        public static readonly Property<string> Status = new Property<string>("Welcome to PantEX~");

        public static VenusContext Venus { get; private set; }

        public static bool IsImageLoading
        {
            get { return ImageView.IsFaded; }
            set
            {
                ImageView.IsFaded = value;
                Spinner.Visible = value;
            }
        }

        public static event EventHandler OnStart;

        private FileInfo ConfigFile;

        private static void InitCodecs()
        {
            Venus = new VenusContext();
            Venus.Initialize();
            EXImage.Venus = Venus;
        }

        public MainForm()
        {
            Synchronizer.Initialize();

            UIThreadId = Thread.CurrentThread.ManagedThreadId;

            ConfigFile = new FileInfo("config.json");
            if (ConfigFile.Exists)
                Config = Settings.Load(ConfigFile.FullName);
            else
                Config = new Settings(ConfigFile.FullName);

            Config.InitializePlugins(Assembly.GetExecutingAssembly());

            InitializeComponent();

            InitCodecs();

            Closing += (sender, e) => Config?.Save();

            InitSourcesMenu();

            Plugin.PluginLoaded += (sender, e) => InitSourcesMenu();
            FavoriteSources.Descriptors.CollectionChanged += (sender, e) => InitSourcesMenu();

            KeyDown += (sender, e) => 
                {
                    if (e.Key == Keys.O)
                        ImageViewer.Cursor.Back();
                    if (e.Key == Keys.P)
                        ImageViewer.Cursor.Forward();
                    if (e.Key == Keys.S)
                        SlideshowController.Enabled = !SlideshowController.Enabled;
                };

            // Hello, cute little bug! It is so nice to meet you~
            // Creating bitmaps asyncronously may cause problems because of concurrent dictionary modifications (seriously, Eto.Forms?)
            new Bitmap(1, 1, Eto.Drawing.PixelFormat.Format24bppRgb).Dispose();

            OnStart(this, EventArgs.Empty);
        }

        private void InitSourcesMenu()
        {
            foreach (var item in SourcesMenu.Items.Where(x => x is ButtonMenuItem).ToList())
                SourcesMenu.Items.Remove(item);

            var favProv = new ButtonMenuItem() { Text = "Favorites" };
            SourcesMenu.Items.Add(favProv);
            foreach (var des in FavoriteSources.Descriptors)
            {
                var item = new ButtonMenuItem
                {
                    Text = des.Title,
                    Command = new Command((s, e) =>
                        {
                            var instance = Plugin.CreateInstance<ImageSource>(des.TypeName, des);
                            new SourceBrowser(instance).Show();
                        })
                };

                favProv.Items.Add(item);
            }

            var provs = new Dictionary<string, ButtonMenuItem>();
            foreach (var type in Plugin.ProvidedTypes[typeof(ImageSourceFactory)])
            {
                var prov = Plugin.CreateInstance<ImageSourceFactory>(type);
                var item = new ButtonMenuItem() { Text = prov.Title, Command = new Command((o, e) => 
                    { 
                        (prov as IPreconfigurable).Configure(() =>
                            {
                                prov.Configure();
                                new SourceBrowser(prov.Instance).Show();
                            });
                    }) };
                if (!provs.ContainsKey(prov.Provider))
                {
                    var provMenu = new ButtonMenuItem() { Text = prov.Provider };
                    SourcesMenu.Items.Add(provMenu);
                    provs.Add(prov.Provider, provMenu);
                }
                provs[prov.Provider].Items.Add(item);
            }
        }

        // Static members

        private static int? DefaultWindowX;
        private static int? DefaultWindowY;
        private static int? DefaultWindowW = 800;
        private static int? DefaultWindowH = 600;

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            DefaultWindowX = dict.ContainsKey("WindowX") ? (int?)((long)dict["WindowX"]) : DefaultWindowX;
            DefaultWindowY = dict.ContainsKey("WindowY") ? (int?)((long)dict["WindowY"]) : DefaultWindowY;
            DefaultWindowW = dict.ContainsKey("WindowW") ? (int?)((long)dict["WindowW"]) : DefaultWindowW;
            DefaultWindowH = dict.ContainsKey("WindowH") ? (int?)((long)dict["WindowH"]) : DefaultWindowH;
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            if (DefaultWindowX.HasValue)
                dict["WindowX"] = DefaultWindowX.Value;
            if (DefaultWindowY.HasValue)
                dict["WindowY"] = DefaultWindowY.Value;
            if (DefaultWindowW.HasValue)
                dict["WindowW"] = DefaultWindowW.Value;
            if (DefaultWindowH.HasValue)
                dict["WindowH"] = DefaultWindowH.Value;
        }
    }
}
