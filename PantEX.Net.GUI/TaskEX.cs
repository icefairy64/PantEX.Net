﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Eto.Forms;
using NLog;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX.GUI
{
    public delegate Task AsyncProcedure();
    public delegate Task<T> AsyncFunction<T>(Action<double, T> progressHandler);
    public delegate void TaskResultHandler<T>(object sender, TaskFinishArgs<T> args);
    public delegate void TaskProgressHandler<T>(object sender, TaskProgressArgs<T> args);
    public delegate void TaskFailHandler(object sender, TaskFailArgs args);

    [AttributeUsage(AttributeTargets.Class)]
    public class CanRunInBackgroundAttribute : Attribute
    {
    }

    public class TaskFailArgs : EventArgs
    {
        public readonly Exception Reason;

        public TaskFailArgs(Exception reason)
        {
            Reason = reason;
        }
    }

    public class TaskFinishArgs<T> : EventArgs
    {
        public readonly T Result;

        public TaskFinishArgs(T result)
        {
            Result = result;
        }
    }

    public class TaskProgressArgs<T> : EventArgs
    {
        public readonly T Value;
        public readonly double Progress;

        public TaskProgressArgs(T value, double progress)
        {
            Value = value;
            Progress = progress;
        }
    }

    #region TaskEX

    [Configurable("TaskEX")]
    public abstract class TaskEX
    {
        /// <summary>
        /// Occurs when task completes.
        /// Guaranteed to be called from UI thread
        /// </summary>
        public event EventHandler Finished;

        /// <summary>
        /// Occurs when task fails.
        /// Guaranteed to be called from UI thread
        /// </summary>
        public event TaskFailHandler Failed;

        public string Title { get; protected set; }

        protected CancellationToken Token;
        protected bool IsTokenSet;

        internal Task Handle;

        protected abstract Task Execute();

        private async Task Wrap()
        {
            try
            {
                Handle = Execute();
                await Handle;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                if (Failed != null)
                    Application.Instance.AsyncInvoke(() => Failed(this, new TaskFailArgs(e)));
            }
        }

        #pragma warning disable 4014
        public void Run()
        {
            Wrap();
        }

        #pragma warning disable 4014
        public void Run(CancellationToken token)
        {
            Token = token;
            IsTokenSet = true;
            Wrap();
        }

        protected void HandleFinish()
        {
            if (this.IsOnUIThread())
                Finished?.Invoke(this, null);
            else
                Application.Instance.Invoke(() => Finished?.Invoke(this, null));
        }

        public void Cancel()
        {
            Cancel(this);
        }

        // Static members

        public static ObservableCollection<TaskEX> Tasks { get; private set; } = new ObservableCollection<TaskEX>();

        public static bool AllowScheduleOnSeparateThread = true;

        private static bool IsRunning = false;
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private static Dictionary<TaskEX, CancellationTokenSource> Tokens = new Dictionary<TaskEX, CancellationTokenSource>();

        [Loader]
        public static void LoadConfig(IDictionary<string, object> dict)
        {
            if (dict.ContainsKey("ScheduleOnSeparateThread"))
                AllowScheduleOnSeparateThread = (bool)dict["ScheduleOnSeparateThread"];
        }

        [Saver]
        public static void SaveConfig(IDictionary<string, object> dict)
        {
            dict["ScheduleOnSeparateThread"] = AllowScheduleOnSeparateThread;
        }

        private static void RunTask(TaskEX task)
        {
            var src = new CancellationTokenSource();
            Tokens.Add(task, src);

            MainForm.Status.Value = $"Starting: '{task.Title}'";

            if (AllowScheduleOnSeparateThread && task.GetType().GetCustomAttribute<CanRunInBackgroundAttribute>() != null)
                new Thread(() => task.Run(src.Token)).Start();
            else
                task.Run(src.Token);
        }

        private static void HandleFinish(TaskEX current)
        {
            Tasks.Remove(current);
            Tokens.Remove(current);
            if (Tasks.Count > 0)
                RunTask(Tasks[0]);
            else
                IsRunning = false;
        }

        public static void RunOnSeparateThread(TaskEX task)
        {
            Logger.Info($"Starting task: {task.Title}");

            task.Failed += (sender, args) => 
                {
                    Logger.Error(String.Format("Task failed: {0}, reason: {1}", ((TaskEX)sender).Title, args.Reason));
                    HandleFinish((TaskEX)sender);
                };
            
            new Thread(() => task.Run()).Start();
        }

        public static void Schedule(TaskEX task)
        {
            Logger.Info($"Scheduling task: {task.Title}");

            task.Finished += (sender, e) =>
                {
                    MainForm.Status.Value = $"Finished: '{task.Title}'";
                    Logger.Info(String.Format("Task finished: {0}", task.Title));
                    HandleFinish(task);
                };
            task.Failed += (sender, args) => 
                {
                    MainForm.Status.Value = $"Failed: '{task.Title}'";
                    Logger.Error(String.Format("Task failed: {0}, reason: {1}", ((TaskEX)sender).Title, args.Reason));
                    HandleFinish((TaskEX)sender);
                };
            Tasks.Add(task);
            if (!IsRunning)
            {
                IsRunning = true;
                RunTask(task);
            }
        }

        private static void Cancel(TaskEX task)
        {
            if (Tokens.ContainsKey(task))
                Tokens[task].Cancel();
        }
    }

    #endregion

    #region Concrete task implementations

    public abstract class AbstractTask<T> : TaskEX
    {
        /// <summary>
        /// Occurs when task completes, producing a result.
        /// Guaranteed to be run on UI thread
        /// </summary>
        public event TaskResultHandler<T> ResultProduced;

        /// <summary>
        /// Occurs when task reports its progress, producing an intermediate result.
        /// Guaranteed to be run on UI thread
        /// </summary>
        public event TaskProgressHandler<T> ProgressChanged;

        protected void HandleFinish(T result)
        {
            if (this.IsOnUIThread())
                HandleFinishOnMain(result);
            else
                Application.Instance.InvokeSafe(() => HandleFinishOnMain(result));
        }

        private void HandleFinishOnMain(T result)
        {
            ResultProduced?.Invoke(this, new TaskFinishArgs<T>(result));
            base.HandleFinish();
        }

        protected void HandleProgress(T value, double progress)
        {
            if (this.IsOnUIThread())
                HandleProgressOnMain(value, progress);
            else
                Application.Instance.InvokeSafe(() => HandleProgressOnMain(value, progress));
        }

        private void HandleProgressOnMain(T value, double progress)
        {
            MainForm.Status.Value = $"{Title} ({(progress * 100):0.##}% done)";
            ProgressChanged?.Invoke(this, new TaskProgressArgs<T>(value, progress));
        }

        protected override abstract Task Execute();
    }

    public class CustomTask : TaskEX
    {
        private AsyncProcedure Routine;

        public CustomTask(AsyncProcedure task, string title = "")
        {
            Routine = task;
            Title = title;
        }

        protected override async Task Execute()
        {
            await Routine();
            HandleFinish();
        }
    }

    public class CustomTask<T> : AbstractTask<T>
    {
        private AsyncFunction<T> Routine;

        public CustomTask(AsyncFunction<T> task, string title = "")
        {
            Routine = task;
            Title = title;
        }

        protected override async Task Execute()
        {
            var res = await Routine((p, v) => HandleProgress(v, p));
            HandleFinish(res);
        }
    }

    #endregion
}

