﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;

namespace Breezy.PantEX.GUI
{
    public partial class MyPanel : Panel
    {
        public MyPanel()
        {
            Content = new StackLayout
            {
                Items =
                {
                    new Label { Text = "Some Content" }
                }
            };
        }
    }
}

