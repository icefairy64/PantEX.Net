﻿using System;

namespace Breezy.Venus
{
    public enum RawPixelFormat
    {
        Rgb24,
        Argb32
    }
}

