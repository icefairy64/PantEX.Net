﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Breezy.Venus
{
    public class RawImage
    {
        public byte[][] Data;

        public uint Width { get; internal set; }
        public uint Height { get; internal set; }
        public uint FrameCount { get; internal set; }

        public RawPixelFormat Format { get; set; }

        public byte[][] FrameData
        {
            get { return Data; }
        }

        public RawImage(uint width, uint height, uint frameCount)
        {
            Width = width;
            Height = height;
            FrameCount = frameCount;
            Data = new byte[frameCount][];
        }
    }
}

