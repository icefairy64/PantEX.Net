﻿using System;
using System.Collections.Generic;
using Breezy.Assistant.Extensions;
namespace Breezy.Venus
{
    [Extendable]
    public interface IImageIOProvider
    {
        bool Initialize();
        void Free();
        IReadOnlyList<IImageFormat> ProvidedFormats { get; }
        IReadOnlyDictionary<IImageFormat, Tuple<IImageImporter, IImageExporter>> IOHandlers { get; }
        bool CheckImportAvailability(IImageFormat format);
        bool CheckExportAvailability(IImageFormat format);
    }
}
