﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Breezy.Venus
{
    public interface IImageImporter
    {
        IImageFormat Format { get; }
        bool DoesMatch(Stream stream);
        RawImage Decode(Stream stream);
    }
}

