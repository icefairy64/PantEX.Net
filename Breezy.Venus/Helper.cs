﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using FreeImageAPI;

namespace Breezy.Venus
{
    public unsafe delegate void* PixelLineConverter(void* source, void* destination, uint pixels, ColorPalette pal);

    public static class Helper
    {
        public static RawPixelFormat GuessPixelFormat(PixelFormat sourceFormat)
        {
            switch (sourceFormat)
            {
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format16bppArgb1555:
                case PixelFormat.Alpha:
                case PixelFormat.Format32bppPArgb:
                case PixelFormat.Format64bppArgb:
                case PixelFormat.Format64bppPArgb:
                case PixelFormat.PAlpha:
                case PixelFormat.Format8bppIndexed:
                    return RawPixelFormat.Argb32;

                default:
                    return RawPixelFormat.Rgb24;
            }
        }

        public static PixelFormat ToDrawingPixelFormat(RawPixelFormat sourceFormat)
        {
            switch (sourceFormat)
            {
                case RawPixelFormat.Rgb24:
                    return PixelFormat.Format24bppRgb;
                case RawPixelFormat.Argb32:
                    return PixelFormat.Format32bppArgb;
                default:
                    return PixelFormat.Format32bppArgb;
            }
        }

        public static int GetBytesPerPixel(RawPixelFormat format)
        {
            switch (format)
            {
                case RawPixelFormat.Rgb24:
                    return 3;
                case RawPixelFormat.Argb32:
                    return 4;
                default:
                    return 0;
            }
        }

        public static uint GetImageSize(uint width, uint height, RawPixelFormat format)
        {
            switch (format)
            {
                case RawPixelFormat.Rgb24:
                    return width * height * 3 + 1;
                case RawPixelFormat.Argb32:
                    return width * height * 4;
                default:
                    return 0;
            }
        }

        public static unsafe PixelLineConverter GetConverter(PixelFormat sourceFormat, RawPixelFormat targetFormat)
        {
            if (targetFormat == RawPixelFormat.Argb32)
            {
                switch (sourceFormat)
                {
                    case PixelFormat.Format32bppArgb:
                        return Argb32ToArgb32;
                    case PixelFormat.Format8bppIndexed:
                        return Idx8ToArgb32;
                }
            }

            if (targetFormat == RawPixelFormat.Rgb24)
            {
                switch (sourceFormat)
                {
                    case PixelFormat.Format24bppRgb:
                        return Rgb24ToRgb24;
                    case PixelFormat.Format32bppRgb:
                        return Rgb32ToRgb24;
                }
            }

            return null;
        }

        private static unsafe void* Argb32ToArgb32(void* source, void* destination, uint pixels, ColorPalette pal)
        {
            var psrc = (int*)source;
            var pdst = (int*)destination;

            for (int i = 0; i < pixels; i++)
                *(pdst++) = *(psrc++);

            return pdst;
        }

        private static unsafe void* Rgb24ToRgb24(void* source, void* destination, uint pixels, ColorPalette pal)
        {
            var psrc = (byte*)source;
            var pdst = (byte*)destination;

            for (int i = 0; i < pixels; i++)
            {
                *((uint*)pdst) = *((uint*)psrc);
                pdst += 3;
                psrc += 3;
            }

            return pdst;
        }

        private static unsafe void* Rgb32ToRgb24(void* source, void* destination, uint pixels, ColorPalette pal)
        {
            var psrc = (uint*)source;
            var pdst = (byte*)destination;

            for (int i = 0; i < pixels; i++)
            {
                *((uint*)pdst) = *(psrc++);
                pdst += 3;
            }

            return pdst;
        }

        private static unsafe void* Idx8ToArgb32(void* source, void* destination, uint pixels, ColorPalette pal)
        {
            var psrc = (byte*)source;
            var pdst = (int*)destination;

            for (int i = 0; i < pixels; i++)
            {
                byte idx = *(psrc++);
                *(pdst++) = pal.Entries[idx].ToArgb();
            }

            return pdst;
        }

        public static RawImage DecodeWithSysDrawing(Stream stream)
        {
            var bmp = new Bitmap(stream);

            var image = FromBitmap(bmp);

            bmp.Dispose();
            return image;
        }

        public static RawImage FromBitmap(Bitmap bmp)
        {
            var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);

            var image = new RawImage((uint)bmp.Width, (uint)bmp.Height, 1);
            var format = GuessPixelFormat(bmp.PixelFormat);
            image.Format = format;

            var buf = new byte[GetImageSize(image.Width, image.Height, format)];

            unsafe
            {
                fixed (void* dst = &buf[0])
                {
                    var pdst = dst;
                    var psrc = (byte*)data.Scan0.ToPointer();
                    var conv = GetConverter(bmp.PixelFormat, format);

                    var width = image.Width;

                    for (int y = 0; y < image.Height; y++)
                    {
                        pdst = conv(psrc, pdst, width, bmp.Palette);
                        psrc += data.Stride;
                    }
                }
            }

            image.Data[0] = buf;

            bmp.UnlockBits(data);
            bmp.Dispose();

            return image;
        }
    }
}

