﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Breezy.Venus
{
    public class IImageFormat
    {
        private readonly string FMimeType;
        private readonly List<string> FExtensions;

        public string MimeType => FMimeType;
        public IReadOnlyList<string> Extensions => FExtensions;

        public IImageFormat(string mimeType, IEnumerable<string> extensions)
        {
            FMimeType = mimeType;
            FExtensions = extensions.ToList();
        }

        public override bool Equals(object obj)
        {
            var f = obj as IImageFormat;
            return f != null && f.FMimeType.ToLower() == FMimeType.ToLower();
        }

        public override int GetHashCode()
        {
            return FMimeType.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("[IImageFormat: FMimeType={0}, FExtensions={1}]", FMimeType, String.Join(", ", FExtensions));
        }
    }
}
