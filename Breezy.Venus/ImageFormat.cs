﻿using System;

namespace Breezy.Venus
{
    public enum ImageFormat
    {
        Bmp,
        Jpeg,
        Png,
        Gif,
        WebP
    }
}

