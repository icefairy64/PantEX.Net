﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using BitMiracle.LibJpeg;
using FreeImageAPI;

namespace Breezy.Venus.FormatHandlers
{
    public class JpegHandler// : IImageImporter
    {
        private static readonly byte[] Signature = { 0xff, 0xd8, 0xff };

        public bool DoesMatch(Stream stream)
        {
            var sign = new byte[Signature.Length];
            stream.Read(sign, 0, Signature.Length);

            for (int i = 0; i < Signature.Length; i++)
            {
                if (sign[i] != Signature[i])
                    return false;
            }

            return true;
        }

        public RawImage Decode(Stream stream)
        {
            var img = FreeImage.LoadFromStream(stream);
            var res = Helper.FromFreeImage(img);
            FreeImage.Unload(img);
            return res;
        }

        public RawImage DecodeM(Stream stream)
        {
            var jpeg = new JpegImage(stream);

            var result = new RawImage((uint)jpeg.Width, (uint)jpeg.Height, 1);
            if (jpeg.BitsPerComponent != 8)
                throw new Exception("JPEG with non-8 bits per component");

            bool isMono = jpeg.ComponentsPerSample == 1;

            result.Format = RawPixelFormat.Argb32;
            var buf = new byte[Helper.GetImageSize(result.Width, result.Height, result.Format)];

            unsafe
            {
                fixed (byte* dst = &(buf[0]))
                {
                    byte* pdst = dst;

                    for (int y = 0; y < jpeg.Height; y++)
                    {
                        var row = jpeg.GetRow(y).ToBytes();
                        int idx = 0;

                        for (int x = 0; x < jpeg.Width; x++)
                        {
                            if (isMono)
                            {
                                *(pdst++) = row[idx];
                                *(pdst++) = row[idx];
                                *(pdst++) = row[idx++];
                            }
                            else
                            {
                                for (int i = 0; i < 3; i++)
                                    *(pdst++) = row[2 - i + idx];
                                idx += 3;
                            }

                            *(pdst++) = 0xff;
                        }
                    }
                }
            }

            result.Data[0] = buf;
            jpeg.Dispose();

            return result;
        }

        public string FileExtension => ".jpg";
    }
}

