﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Zip;
using FreeImageAPI;
using ICSharpCode.SharpZipLib.Core;
using System.Collections.Generic;

namespace Breezy.Venus.FormatHandlers
{
    public class ZipHandler// : IImageImporter
    {
        private static readonly byte[] Signature = { 0x50, 0x4b };

        readonly VenusContext Venus;

        public bool DoesMatch(Stream stream)
        {
            var sign = new byte[Signature.Length];
            stream.Read(sign, 0, Signature.Length);

            for (int i = 0; i < Signature.Length; i++)
            {
                if (sign[i] != Signature[i])
                    return false;
            }

            return true;
        }

        public RawImage Decode(Stream stream)
        {
            var zip = new ZipFile(stream);
            RawImage res = new RawImage(0, 0, (uint)zip.Count);

            int i = 0;
            foreach (ZipEntry entry in zip)
            {
                using (var entryStream = zip.GetInputStream(entry))
                {
                    using (var memStream = new MemoryStream((int)entry.Size))
                    {
                        var frame = Venus.Load(memStream);

                        if (res.Width == 0)
                        {
                            res.Width = frame.Width;
                            res.Height = frame.Height;
                            res.Format = frame.Format;
                        }

                        res.Data[i++] = frame.Data[0];
                    }
                }
            }

            zip.Close();
            return res;
        }

        public string FileExtension => ".zip";
    }
}

