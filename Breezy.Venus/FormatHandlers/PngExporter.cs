﻿using System;
using System.IO;

namespace Breezy.Venus.FormatHandlers
{
    public class PngExporter// : IImageExporter
    {
        public PngExporter()
        {
        }

        public void Export(RawImage image, Stream outStream)
        {
            Helper.ExportWithFreeImage(image, outStream, FreeImageAPI.FREE_IMAGE_FORMAT.FIF_PNG);
        }

        public string FileExtension => ".png";
    }
}

