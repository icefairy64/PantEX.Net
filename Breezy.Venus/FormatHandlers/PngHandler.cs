﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using FreeImageAPI;

namespace Breezy.Venus.FormatHandlers
{
    public class PngHandler// : IImageImporter
    {
        private static readonly byte[] Signature = { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };

        public bool DoesMatch(Stream stream)
        {
            var sign = new byte[Signature.Length];
            stream.Read(sign, 0, Signature.Length);

            for (int i = 0; i < Signature.Length; i++)
            {
                if (sign[i] != Signature[i])
                    return false;
            }

            return true;
        }

        public RawImage Decode(Stream stream)
        {
            var img = FreeImage.LoadFromStream(stream);
            var res = Helper.FromFreeImage(img);
            FreeImage.Unload(img);
            return res;
        }

        public string FileExtension => ".png";
    }
}

