﻿using System;
using System.Threading.Tasks;
using System.IO;
using Imazen.WebP;
using Breezy.Assistant;
using System.Drawing;
using System.Drawing.Imaging;

namespace Breezy.Venus.FormatHandlers
{
    public class WebPHandler// : IImageImporter, IImageExporter
    {
        private static readonly byte[] Signature = { 0x57, 0x45, 0x42, 0x50, 0x56, 0x50, 0x38 };
        private static readonly SimpleDecoder WebPDecoder = new SimpleDecoder();
        private static readonly SimpleEncoder WebPEncoder = new SimpleEncoder();

        public float ExportQuality { get; private set; }

        public WebPHandler(float exportQuality)
        {
            ExportQuality = exportQuality;
        }

        public bool DoesMatch(Stream stream)
        {
            stream.Read(new byte[8], 0, 8);
            var sign = new byte[Signature.Length];
            stream.Read(sign, 0, Signature.Length);

            for (int i = 0; i < Signature.Length; i++)
            {
                if (sign[i] != Signature[i])
                    return false;
            }

            return true;
        }

        public RawImage Decode(Stream stream)
        {
            var buf = stream.ReadAll();
            var bmp = WebPDecoder.DecodeFromBytes(buf, buf.Length);
            var img = Helper.FromBitmap(bmp);
            bmp.Dispose();
            return img;
        }

        public void Export(RawImage image, Stream outStream)
        {
            var sdbmp = new Bitmap((int)image.Width, (int)image.Height, Helper.ToDrawingPixelFormat(image.Format));
            var data = sdbmp.LockBits(new Rectangle(0, 0, sdbmp.Width, sdbmp.Height), ImageLockMode.WriteOnly, sdbmp.PixelFormat);

            var components = Helper.GetBytesPerPixel(image.Format);

            unsafe
            {
                byte* dst = (byte*)data.Scan0.ToPointer();
                fixed (byte* src = &image.Data[0][0])
                {
                    byte* psrc = src;
                    for (int y = 0; y < image.Height; y++)
                    {
                        byte* pdst = dst;
                        for (int x = 0; x < image.Width; x++)
                        {
                            for (int i = 0; i < components; i++)
                                *(pdst++) = *(psrc++);
                        }
                        dst += data.Stride;
                    }
                }
            }

            sdbmp.UnlockBits(data);

            WebPEncoder.Encode(sdbmp, outStream, 95);

            sdbmp.Dispose();
        }

        public string FileExtension => ".webp";
    }
}

