﻿using System;
using System.IO;

namespace Breezy.Venus
{
    public interface IImageExporter
    {
        IImageFormat Format { get; }
        void Export(RawImage image, Stream outStream);
    }
}

