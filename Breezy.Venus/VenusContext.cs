﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Breezy.Venus
{
    public class VenusContext
    {
        List<IImageIOProvider> FProviders;

        List<IImageImporter> FImporters;
        List<IImageExporter> FExporters;

        public IReadOnlyList<IImageIOProvider> IOProviders => FProviders;
        public IReadOnlyList<IImageImporter> Importers => FImporters;
        public IReadOnlyList<IImageExporter> Exporters => FExporters;

        public void Initialize()
        {
            FProviders = new List<IImageIOProvider>();
            FImporters = new List<IImageImporter>();
            FExporters = new List<IImageExporter>();
            foreach (var providerType in Plugin.ProvidedTypes[typeof(IImageIOProvider)])
            {
                var provider = Plugin.CreateInstance<IImageIOProvider>(providerType);
                if (provider.Initialize())
                {
                    FProviders.Add(provider);
                    foreach (var pair in provider.IOHandlers.Select(x => x.Value))
                    {
                        if (pair.Item1 != null)
                            FImporters.Add(pair.Item1);
                        if (pair.Item2 != null)
                            FExporters.Add(pair.Item2);
                    }
                }
            }
        }

        public IImageImporter FindImporter(Stream stream)
        {
            bool isSeekable = stream.CanSeek;
            if (!isSeekable)
            {
                var tmp = new MemoryStream();
                stream.CopyTo(tmp);
                stream = tmp;
            }

            IImageImporter result = null;

            foreach (var imp in FImporters)
            {
                stream.Seek(0, SeekOrigin.Begin);
                var res = imp.DoesMatch(stream);
                if (res)
                {
                    result = imp;
                    break;
                }
            }

            if (!isSeekable)
                stream.Dispose();
            else
                stream.Seek(0, SeekOrigin.Begin);

            return result;
        }

        public RawImage Load(Stream stream)
        {
            var importer = FindImporter(stream);

            if (importer == null)
                throw new Exception("No suitable importer found");

            return importer.Decode(stream);
        }

        public IImageExporter FindExporter(IImageFormat format)
        {
            return FExporters
                .Where(x => x.Format.Equals(format))
                .FirstOrDefault();
        }

        public IImageExporter FindExporterByMime(string format)
        {
            return FExporters
                .Where(x => x.Format.MimeType.ToLower() == format.ToLower())
                .FirstOrDefault();
        }

        public IImageExporter FindExporterByExtension(string format)
        {
            return FExporters
                .Where(x => x.Format.Extensions.Any(e => e.Equals(format, StringComparison.InvariantCultureIgnoreCase)))
                .FirstOrDefault();
        }

        public void Export(RawImage image, Stream stream, IImageFormat format)
        {
            var exporter = FindExporter(format);

            if (exporter == null)
                throw new Exception("No suitable exporter found");

            exporter.Export(image, stream);
        }
    }
}
