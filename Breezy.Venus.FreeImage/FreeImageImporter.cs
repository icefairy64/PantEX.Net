﻿using System;
using System.IO;
using FreeImageAPI;
using NLog;
namespace Breezy.Venus.FreeImage
{
    public class FreeImageImporter : IImageImporter
    {
        readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        readonly FREE_IMAGE_FORMAT FFreeImageFormat;
        readonly IImageFormat FFormat;

        public FreeImageImporter(FREE_IMAGE_FORMAT format)
        {
            FFreeImageFormat = format;
            FFormat = new IImageFormat(
                FreeImageAPI.FreeImage.GetFIFMimeType(format),
                FreeImageAPI.FreeImage.GetFIFExtensionList(format).Split(',')
            );
        }

        public FREE_IMAGE_FORMAT FreeImageFormat => FFreeImageFormat;
        public IImageFormat Format => FFormat;

        public RawImage Decode(Stream stream)
        {
            var img = FreeImageAPI.FreeImage.LoadFromStream(stream);
            var res = Helper.FromFreeImage(img);
            FreeImageAPI.FreeImage.Unload(img);
            return res;
        }

        public bool DoesMatch(Stream stream)
        {
            var realFormat = FreeImageAPI.FreeImage.GetFileTypeFromStream(stream);
            var result = realFormat == FFreeImageFormat;
            return result;
        }

        public override string ToString()
        {
            return string.Format("[FreeImageImporter: FFreeImageFormat={0}, FFormat={1}]", FFreeImageFormat, FFormat);
        }
    }
}
