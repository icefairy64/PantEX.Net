﻿using NUnit.Framework;
using System;
using System.Resources;
using System.Reflection;
using Breezy.Assistant.Extensions;

namespace Breezy.Venus.FreeImage.Test
{
    [TestFixture()]
    public class ImportTest
    {
        [Test()]
        public void ImportPng()
        {
            Plugin.Register(Assembly.GetExecutingAssembly());

            var venus = new VenusContext();
            venus.Initialize();

            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("tux.png");
            Assert.IsNotNull(stream, "Sample PNG image not found");

            var importer = venus.FindImporter(stream);
            Assert.IsNotNull(importer, "Importer not found");
            Assert.AreEqual(importer.Format.MimeType.ToLower(), "image/png");

            var image = importer.Decode(stream);
            Assert.AreEqual(image.Width, 624, 0.1, "Widths are not equal");
            Assert.AreEqual(image.Height, 640, 0.1, "Widths are not equal");
        }
    }
}
