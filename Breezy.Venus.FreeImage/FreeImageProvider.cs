﻿using System;
using System.Collections.Generic;
using NLog;
using FreeImageAPI;
using System.Linq;

namespace Breezy.Venus.FreeImage
{
    public class FreeImageProvider : IImageIOProvider
    {
        readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        List<IImageImporter> Importers;
        List<IImageExporter> Exporters;

        Dictionary<IImageFormat, Tuple<IImageImporter, IImageExporter>> FIOHandlers;

        public IReadOnlyList<IImageFormat> ProvidedFormats => throw new NotImplementedException();

        public IReadOnlyDictionary<IImageFormat, Tuple<IImageImporter, IImageExporter>> IOHandlers => FIOHandlers;

        public bool CheckExportAvailability(IImageFormat format)
        {
            return Importers.Any(x => x.Format.Equals(format));
        }

        public bool CheckImportAvailability(IImageFormat format)
        {
            return Exporters.Any(x => x.Format.Equals(format));
        }

        public void Free()
        {
            
        }

        public bool Initialize()
        {
            Importers = new List<IImageImporter>();
            Exporters = new List<IImageExporter>();

            Logger.Info("Checking FreeImage library availability...");

            try
            {
                var available = FreeImageAPI.FreeImage.IsAvailable();
                Logger.Info($"Is FreeImage available: {available}");

                var pairList = new List<Tuple<IImageImporter, IImageExporter>>();

                foreach (FREE_IMAGE_FORMAT format in Enum.GetValues(typeof(FREE_IMAGE_FORMAT)))
                {
                    var formatAvailable = FreeImageAPI.FreeImage.IsPluginEnabled(format) == 1;
                    Logger.Info($"Is FreeImageFormat {format} available: {formatAvailable}");
                    if (formatAvailable) 
                    {
                        var pair = new Tuple<IImageImporter, IImageExporter>(null, null);
                        if (FreeImageAPI.FreeImage.FIFSupportsReading(format))
                        {
                            var importer = new FreeImageImporter(format);
                            Importers.Add(importer);
                            pair = new Tuple<IImageImporter, IImageExporter>(importer, null);
                            Logger.Info($"FreeImageFormat {format} supports reading; MIME: {importer.Format.MimeType}, extensions: {String.Join(", ", importer.Format.Extensions)}");
                        }
                        if (FreeImageAPI.FreeImage.FIFSupportsWriting(format))
                        {
                            var exporter = new FreeImageExporter(format);
                            Exporters.Add(exporter);
                            pair = new Tuple<IImageImporter, IImageExporter>(pair.Item1, exporter);
                            Logger.Info($"FreeImageFormat {format} supports writing; MIME: {exporter.Format.MimeType}, extensions: {String.Join(", ", exporter.Format.Extensions)}");
                        }
                        if (pair.Item1 != null || pair.Item2 != null)
                            pairList.Add(pair);
                    }
                }

                FIOHandlers = new Dictionary<IImageFormat, Tuple<IImageImporter, IImageExporter>>();

                foreach (var pair in pairList)
                    if (!FIOHandlers.ContainsKey(pair.Item1?.Format ?? pair.Item2.Format))
                    {
                        Logger.Debug($"Adding format {pair}");
                        FIOHandlers.Add(pair.Item1?.Format ?? pair.Item2.Format, pair);
                    }

                return available;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"An error occured while checking FreeImage availability: {e.Message}");
                Logger.Error(e.StackTrace);
                return false;
            }
        }
    }
}
