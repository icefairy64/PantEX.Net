﻿using System;
using FreeImageAPI;
using System.IO;

namespace Breezy.Venus.FreeImage
{
    internal static class Helper
    {
        internal static RawImage FromFreeImage(FIBITMAP img)
        {
            var bpp = FreeImageAPI.FreeImage.GetBPP(img);
            FreeImageAPI.FreeImage.FlipVertical(img);

            RGBQUAD[] palette = null;
            if (bpp < 15)
                palette = FreeImageAPI.FreeImage.GetPaletteEx(img).AsArray;

            uint w = FreeImageAPI.FreeImage.GetWidth(img);
            uint h = FreeImageAPI.FreeImage.GetHeight(img);

            var res = new RawImage(w, h, 1);
            res.Format = RawPixelFormat.Argb32;
            var buf = new byte[Breezy.Venus.Helper.GetImageSize(w, h, res.Format)];

            unsafe
            {
                fixed (byte* dst = &buf[0])
                {
                    uint* pdst = (uint*)dst;

                    for (uint y = 0; y < h; y++)
                    {
                        for (uint x = 0; x < w; x++)
                        {
                            if (bpp < 15)
                            {
                                byte idx;
                                FreeImageAPI.FreeImage.GetPixelIndex(img, x, y, out idx);
                                *pdst = palette[idx];
                            }
                            else
                            {
                                RGBQUAD pixel;
                                FreeImageAPI.FreeImage.GetPixelColor(img, x, y, out pixel);
                                *pdst = pixel.uintValue;
                            }

                            if (bpp < 32)
                                *pdst |= 0xff000000;

                            pdst++;
                        }
                    }
                }
            }

            res.Data[0] = buf;

            return res;
        }

        internal static void ExportWithFreeImage(RawImage image, Stream outStream, FREE_IMAGE_FORMAT format)
        {
            var components = image.Format == RawPixelFormat.Rgb24 ? 3 : 4;
            var raw = FreeImageAPI.FreeImage.Allocate((int)image.Width, (int)image.Height, components * 8);

            unsafe
            {
                fixed (byte* src = &image.Data[0][0])
                {
                    byte* psrc = src;
                    for (int y = 0; y < image.Height; y++)
                    {
                        byte* pdst = (byte*)FreeImageAPI.FreeImage.GetScanLine(raw, y);

                        for (int x = 0; x < image.Width; x++)
                        {
                            for (int i = 0; i < components; i++)
                                *(pdst++) = *(psrc++);
                        }
                    }
                }
            }

            FreeImageAPI.FreeImage.FlipVertical(raw);

            FreeImageAPI.FreeImage.SaveToStream(raw, outStream, format);
            FreeImageAPI.FreeImage.Unload(raw);
        }
    }
}
