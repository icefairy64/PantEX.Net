﻿using System;
using System.IO;
using FreeImageAPI;

namespace Breezy.Venus.FreeImage
{
    public class FreeImageExporter : IImageExporter
    {
        readonly FREE_IMAGE_FORMAT FFreeImageFormat;
        readonly IImageFormat FFormat;

        public FreeImageExporter(FREE_IMAGE_FORMAT format)
        {
            FFreeImageFormat = format;
            FFormat = new IImageFormat(
                FreeImageAPI.FreeImage.GetFIFMimeType(format),
                FreeImageAPI.FreeImage.GetFIFExtensionList(format).Split(',')
            );
        }

        public FREE_IMAGE_FORMAT FreeImageFormat => FFreeImageFormat;
        public IImageFormat Format => FFormat;

        public void Export(RawImage image, Stream outStream)
        {
            Helper.ExportWithFreeImage(image, outStream, FFreeImageFormat);
        }

        public override string ToString()
        {
            return string.Format("[FreeImageExporter: FFreeImageFormat={0}, FFormat={1}]", FFreeImageFormat, FFormat);
        }
    }
}
