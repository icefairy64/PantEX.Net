﻿namespace Breezy.PantEX.Gaengdo.Sources

open System
open System.Text.RegularExpressions
open Breezy.PantEX
open Breezy.PantEX.GUI
open Breezy.PantEX.FSharp.Http
open Breezy.PantEX.FSharp.Bits
open Breezy.PantEX.Images
open Breezy.Assistant.FSharp.Regex
open FSharpx.Strings
open FSharpx.Choice

type photoNetSource (url : string) =
    inherit ImageSource ()

    let thumbUrl = "http://thumbs.photo.net/photo/{0}-sm.jpg"
    let fullUrl = "http://gallery.photo.net/photo/{0}-lg.jpg"
    let sourceUrl = "http://photo.net/photodb/photo?photo_id={0}"

    let imagePattern = Regex ("class=\"trp_photo\".+?href=\"/photodb/photo\?photo_id=(?<id>.+?)\"", RegexOptions.Singleline)

    let url = url

    let replaceId id str =
        str |> replace "{0}" id

    let createImage id =
        let img = HttpImage (fullUrl |> replaceId id, thumbUrl |> replaceId id)
        img.Source <- Uri (sourceUrl |> replace "{0}" id)
        img.Thumb.Filename <- img.Filename
        img.Thumb.Title <- img.Title

        img :> EXImage

    let parseHtml html =
        html
        |> findMatchesNamed imagePattern
        |> Seq.map (fun x -> Map.find "id" x)
        |> Seq.map createImage

    override this.Load (page : int) =
        let res =
            choose {
                let! data = url |> replace "{0}" ((page * 24).ToString()) |> tryFetchString
                return parseHtml data
            }

        res |> choiceOr Seq.empty


    override this.LoadAsync (page : int) =
        let ld = 
            async {
                let! a = url |> replace "{0}" ((page * 24).ToString()) |> tryFetchStringAsync
                let res = 
                    choose {
                        let! data = a
                        return parseHtml data
                    }
                return res |> choiceOr Seq.empty
            }

        Async.StartAsTask ld

    override this.CreateDescriptor () =
        let des = ImageSourceDescriptor ()
        des.TypeName <- typeof<photoNetSource>.FullName
        des.Descriptor <- url
        des

    override this.Title = "Photo.net"

module PhotoNet =
    let searchStr = "http://photo.net/gallery/tag-search/search?query_string={1}&search=&sort_order={2}&start_index={0}"
    let topStr = "http://photo.net/gallery/photocritique/filter?period={1}&rank_by={2}&category={3}&start_index={0}&store_prefs_p=1&shown_tab=0&page=Next"

    let categories = 
        [
            "All";
            "NoNudes";
            "Abstract";
            "Architecture";
            "Astrophotography";
            "Birds";
            "Cars and Vehicles";
            "Children";
            "Concerts";
            "Digital Alterations";
            "Documentary";
            "Events";
            "Fashion";
            "Fine Art";
            "Flowers";
            "Humor";
            "Insects";
            "Landscape";
            "Macro";
            "Nature";
            "News/Journalism";
            "Nudes";
            "Pets";
            "Portraits";
            "Sports";
            "Still Life/Studio";
            "Street";
            "Travel";
            "Underwater";
            "Wedding and Social"
        ]

    let periods = 
        [
            ("1",     "24 hours");
            ("3",     "3 days");
            ("7",     "Last week");
            ("30",    "Last month");
            ("90",    "Last 3 months");
            ("365",   "Last year");
            ("365-1", "Year ago");
            ("365-2", "2 years ago");
            ("365-3", "3 years ago");
            ("365-4", "4 years ago");
            ("365-5", "5 years ago");
            ("5000",  "All time");
        ]

    let create url =
        photoNetSource url

    type photoNetSearch () =
        inherit ImageSourceFactory ()

        let mutable query = ""

        override this.Provider = "Photo.net"
        override this.Title = "Search"
        override this.Instance = searchStr |> replace "{1}" query |> replace "{2}" "1" |> create :> ImageSource

        override this.Configure () =
            let d = new InputDialog ("Query")
            query <- d.Ask ()

    type photoNetTop () =
        inherit ImageSourceFactory ()

        let mutable category = ""
        let mutable period = ""

        override this.Provider = "Photo.net"
        override this.Title = "Top in category"
        override this.Instance = topStr |> replace "{3}" category |> replace "{1}" period |> replace "{2}" "sum" |> create :> ImageSource

        override this.Configure () =
            let d = new SelectorDialog<string> (categories, "Select category")
            category <- d.Ask ()
            let d2 = new SelectorDialog<string> (periods |> Seq.map (fun x -> snd x), "Select time period")
            let cperiod = d2.Ask ()
            period <- periods |> Seq.find (fun x -> snd x = cperiod) |> fst
