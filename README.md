# PantEX.Net

**PantEX.Net** is a simple image collection management tool currently being in early stage of development.

## Features

* Basic collection operations
* Tagging images
* Exporting and importing collections with metadata stored in JSON
* Browsing and adding images from various sources _(provided via plug-ins)_
* Bookmarking favorite sources
* External image I/O _(FreeImage plugin is included)_

## Supported sources

* Danbooru
* Reddit
* Imgur _(currently only for Reddit posts)_
* Photo.Net
* Sankaku Channel / Idol
* DeviantArt _(requires an account)_
* Behoimi.org (3dbooru)
* Luscious.net

## Dependencies

* Breezy.Mars _(currently can be found as a part of Breezy.Iltal)_
* Breezy.Iltal _(for DeviantArt plugin)_

## Usage demonstration

* (v0.4) [YouTube](https://www.youtube.com/watch?v=KMyduiE3Zm0)