﻿using System;
using Breezy.Mars.Rest;
using System.Collections.Generic;
using Breezy.Derpibooru.Model;
using System.Threading.Tasks;

namespace Breezy.Derpibooru
{
    [RestRoot("https://derpibooru.org/")]
    public interface IDerpibooruAPI
    {
        [RestAction("images.json", RestActionMethod.Get, true)]
        Task<IDictionary<string, IList<DerpibooruImage>>> GetImages
        (
            [RestParameter("page")] int page,
            [RestParameter("constraint")] string constraint = null,
            [RestParameter("gt")] string greaterThan = null,
            [RestParameter("gte")] string greaterOrEqualsTo = null,
            [RestParameter("lt")] string lessThan = null,
            [RestParameter("lte")] string lessOrEqualsTo = null,
            [RestParameter("order")] char? order = null,
            [RestParameter("random_image")] bool? random = null
        );

        [RestAction("search.json", RestActionMethod.Get, true)]
        Task<DerpibooruSearchResponse> Search
        (
            [RestParameter("page")] int page,
            [RestParameter("q")] string query
        );

        [RestAction("lists.json", RestActionMethod.Get, true)]
        Task<IDictionary<string, IList<DerpibooruImage>>> GetTopScoring
        (
            [RestParameter("page")] int page,
            [RestParameter("last")] string period = null
        );
    }
}
