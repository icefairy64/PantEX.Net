﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace Breezy.Derpibooru.Model
{
    [Serializable]
    public class DerpibooruSearchResponse
    {
        [JsonProperty("search")]
        public IList<DerpibooruImage> Images;
    }
}
