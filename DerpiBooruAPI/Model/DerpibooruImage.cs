﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Breezy.Derpibooru.Model
{
    [Serializable]
    public class DerpibooruImage
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("uploader_id")]
        public int? UploaderId { get; set; }

        [JsonProperty("score")]
        public int? Score { get; set; }

        [JsonProperty("width")]
        public int? Width { get; set; }

        [JsonProperty("height")]
        public int? Height { get; set; }

        [JsonProperty("file_name")]
        public string Filename { get; set; }

        [JsonProperty("image")]
        public string ImagePath { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("representations")]
        public IDictionary<string, string> RepresentationPaths;
    }
}
