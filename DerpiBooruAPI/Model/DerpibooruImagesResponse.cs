﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace Breezy.Derpibooru.Model
{
    [Serializable]
    public class DerpibooruImagesResponse
    {
        [JsonProperty("images")]
        public IList<DerpibooruImage> Images;
    }
}
