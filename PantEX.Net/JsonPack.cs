﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Breezy.Assistant;
using System.Collections.Concurrent;

namespace Breezy.PantEX
{
    public delegate void SaveHandler(EXImage img, double progress);
    public delegate EXImage LoadHandler(string filename, string thumbFilename, double progress);
    public delegate Task SaveHandlerAsync(EXImage img, double progress);
    public delegate Task<EXImage> LoadHandlerAsync(string filename, string thumbFilename, double progress);

    public static class JsonPack
    {
        private static JsonSerializer Serializer = JsonSerializer.CreateDefault();

        public static readonly string ForceWebPOption = "force-webp";
        public static readonly string ForceFormatOption = "force-format";

        public static void Load(Collection collection, LoadHandler loader, Stream stream)
        {
            using (var reader = new JsonTextReader(new StreamReader(stream)))
            {
                var model = Serializer.Deserialize<Model>(reader);
                collection.Metadata[ForceFormatOption] = model.ForceWebP ? "webp" : model.ForceFormat;
                int n = 0;
                int c = model.Images.Count();
                var exceptions = new List<Exception>();
                foreach (var img in model.Images)
                {
                    try
                    {
                        var thumbFilename = img.Filename;
                        if (img.ThumbExtension != null)
                            thumbFilename = Path.ChangeExtension(thumbFilename, img.ThumbExtension);
                        var image = loader(img.Filename, thumbFilename, (double)(n++) / c);
                        image.AddTags(img.Tags.Select(i => Tag.Create(model.Tags[i])));
                        collection.AddImage(image, false);
                    }
                    catch (Exception e)
                    {
                        exceptions.Add(e);
                    }
                }
                if (exceptions.Count > 0)
                    throw new AggregateException(exceptions);
            }
        }

        public static async Task Load(Collection collection, LoadHandlerAsync loader, Stream stream)
        {
            string data = null;

            using (var reader = new StreamReader(stream))
            {
                data = await reader.ReadToEndAsync();
            }

            using (var reader = new JsonTextReader(new StringReader(data)))
            {
                var model = await Task.Run(() => Serializer.Deserialize<Model>(reader));
                collection.Metadata[ForceFormatOption] = model.ForceWebP ? "webp" : model.ForceFormat;
                int n = 0;
                int c = model.Images.Count();

                var exceptions = new ConcurrentQueue<Exception>();

                var tagsList = model.Tags.Select(x => Tag.Create(x)).ToList();

                var imgArray = new EXImage[c];

                Parallel.ForEach(model.Images, async (img, ls, index) => 
                    {
                        try
                        {
                            var thumbFilename = img.Filename;
                            if (img.ThumbExtension != null)
                                thumbFilename = Path.ChangeExtension(thumbFilename, img.ThumbExtension);
                            var image = await loader(img.Filename, thumbFilename, (double)(n++) / c);
                            image.AddTags(img.Tags.Select(i => tagsList[i]), false);
                            image.Source = img.Source;
                            imgArray[index] = image;
                        }
                        catch (Exception e)
                        {
                            exceptions.Enqueue(e);
                        }
                    });

                if (exceptions.Count > 0)
                    throw new AggregateException(exceptions);

                collection.AddImages(imgArray, false);
            }
        }

        public static void Save(Collection collection, SaveHandler saver, Stream stream)
        {
            using (var writer = new JsonTextWriter(new StreamWriter(stream)))
            {
                var forcedFormat = collection.GetForcedFormat();

                var tags = collection.Tags.ToList();
                int n = 0;
                int c = collection.Images.Count;

                var exceptions = new List<Exception>();

                foreach (var img in collection.Images)
                {
                    HandleNaming(img, forcedFormat);

                    try
                    {
                        saver(img, (double)(n++) / c);
                    }
                    catch (Exception e)
                    {
                        exceptions.Add(new Exception($"Error while exporting image [{img}]", e));
                    }
                }
                if (exceptions.Count > 0)
                    throw new AggregateException(exceptions);

                var images = collection.Images.Select(x => ImageModel.Create(x.Filename, x.Tags.Select(y => tags.IndexOf(y)), Path.GetExtension(x.Thumb.Filename), x.Source));

                var model = Model.Create(images, tags.Select(x => x.Title));
                model.ForceFormat = forcedFormat;

                Serializer.Serialize(writer, model);
            }
        }

        public static async Task<string> Save(Collection collection, SaveHandlerAsync saver)
        {
            var forcedFormat = collection.GetForcedFormat();

            var tags = collection.Tags.ToList();
            int n = 0;
            int c = collection.Images.Count;
            var exceptions = new List<Exception>();

            foreach (var img in collection.Images)
            {
                HandleNaming(img, forcedFormat);

                try
                {
                    await saver(img, (double)(n++) / c);
                }
                catch (Exception e)
                {
                    exceptions.Add(new Exception($"Error while exporting image [{img.Filename}]", e));
                }
            }
            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);

            var images = collection.Images.Select(x => ImageModel.Create(x.Filename, x.Tags.Select(y => tags.IndexOf(y)), Path.GetExtension(x.Thumb.Filename), x.Source));

            var model = Model.Create(images, tags.Select(x => x.Title));
            model.ForceFormat = forcedFormat;

            var writer = new StringWriter();

            using (var jw = new JsonTextWriter(writer))
                Serializer.Serialize(jw, model);
            
            return writer.ToString();
        }

        public static string GenerateJson(Collection collection)
        {
            var forcedFormat = collection.GetForcedFormat();

            foreach (var img in collection.Images)
                HandleNaming(img, forcedFormat);

            var tags = collection.Tags.ToList();
            var images = collection.Images.Select(x => ImageModel.Create(x.Filename, x.Tags.Select(y => tags.IndexOf(y)), Path.GetExtension(x.Thumb.Filename), x.Source));

            var model = Model.Create(images, tags.Select(x => x.Title));
            model.ForceFormat = forcedFormat;

            var writer = new StringWriter();
            using (var jw = new JsonTextWriter(writer))
                Serializer.Serialize(jw, model);
            return writer.ToString();
        }

        public static void HandleNaming(EXImage img, string forcedFormat)
        {
            if (!string.IsNullOrEmpty(forcedFormat))
            {
                img.Filename = Path.ChangeExtension(img.Filename, "." + forcedFormat);
                img.Thumb.Filename = Path.ChangeExtension(img.Thumb.Filename, "." + forcedFormat);
            }

            if (img.Filename.Length > 255)
                img.Filename = img.Filename.Right(255);

            if (img.Thumb.Filename.Length > 255)
                img.Thumb.Filename = img.Thumb.Filename.Right(255);
        }

        [Serializable]
        private class Model
        {
            [JsonProperty("images")]
            public IEnumerable<ImageModel> Images { get; set; }
            [JsonProperty("tags")]
            public IList<string> Tags { get; set; }
            [JsonProperty("force-webp")]
            public bool ForceWebP { get; set; }
            [JsonProperty("force-format")]
            public string ForceFormat { get; set; }

            internal static Model Create(IEnumerable<ImageModel> images, IEnumerable<string> tags)
            {
                return new Model
                {
                    Images = new List<ImageModel>(images),
                    Tags = new List<string>(tags)
                };
            }
        }

        [Serializable]
        private class ImageModel
        {
            [JsonProperty("title")]
            public string Filename { get; set; }
            [JsonProperty("thumb")]
            public string ThumbExtension { get; set; }
            [JsonProperty("tags")]
            public IList<int> Tags { get; set; }
            [JsonProperty("source")]
            public Uri Source { get; set; }

            internal static ImageModel Create(string filename, IEnumerable<int> tags, string ext, Uri source)
            {
                return new ImageModel
                { 
                    Filename = filename, 
                    ThumbExtension = ext,
                    Tags = new List<int>(tags),
                    Source = source
                };
            }
        }
    }
}

