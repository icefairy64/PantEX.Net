﻿using System;
using System.Threading.Tasks;

namespace Breezy.PantEX
{
    public interface IPreconfigurable
    {
        Task Preconfigure();
    }
}

