﻿using System;
using Newtonsoft.Json.Linq;

namespace Breezy.PantEX
{
    public class ImageSourceDescriptor
    {
        public string Title;
        public string TypeName;
        public string Descriptor;

        public static explicit operator ImageSourceDescriptor(JObject token)
        {
            return new ImageSourceDescriptor
            {
                Title = token["Title"].Value<string>(),
                TypeName = token["TypeName"].Value<string>(),
                Descriptor = token["Descriptor"].Value<string>()
            };
        }
    }
}

