﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Linq;

// <SIGN><IMGCOUNT DW><TAGCOUNT DW>GS
// [TAG1]RS..[TAGN]GS
// [IMG1]RS..[IMGN]

// Image structure:
// [FILENAME]US[TITLE]US[THUMBEXT]US[TAGS]

// GS = group separator  (0x1D)
// RS = record separator (0x1E)
// US = unit separator   (0x1F)

namespace Breezy.PantEX
{
    internal struct ImageData
    {
        public string Filename;
        public string Title;
        public string ThumbExt;
        public BitArray Tags;
    }

    public static class EXPack
    {
        private static readonly byte[] Signature = { 0x50, 0x41, 0x4e, 0x54, 0x53, 0x55, 0x45, 0x58 };
        private static readonly int BufferSize = 1024;

        private static uint ByteArrayToDword(byte[] arr, int offset = 0)
        {
            return (uint)((arr[3 + offset] << 24) | (arr[2 + offset] << 16) | (arr[1 + offset] << 8) | arr[0 + offset]);
        }

        private static ushort ByteArrayToWord(byte[] arr, int offset = 0)
        {
            return (ushort)((arr[1 + offset] << 8) | arr[0 + offset]);
        }

        private static void WriteDword(uint data, byte[] arr, int offset = 0)
        {
            var tmp = data;
            arr[offset + 3] = (byte)(tmp & 0xff);
            tmp >>= 8;
            arr[offset + 2] = (byte)(tmp & 0xff);
            tmp >>= 8;
            arr[offset + 1] = (byte)(tmp & 0xff);
            tmp >>= 8;
            arr[offset] = (byte)tmp;
        }

        private static void WriteWord(ushort data, byte[] arr, int offset = 0)
        {
            var tmp = data;
            arr[offset + 1] = (byte)(tmp & 0xff);
            tmp >>= 8;
            arr[offset] = (byte)tmp;
        }

        public static void Save(Collection collection, SaveHandler saver, Stream stream)
        {
            // Header

            stream.Write(Signature, 0, Signature.Length);
            var buffer = new byte[8];
            WriteDword((uint)collection.Images.Count, buffer);
            WriteDword((uint)collection.Tags.Count, buffer, 4);
            stream.Write(buffer, 0, 8);

            // Tags

            buffer = new byte[BufferSize];
            int idx = 0;

            foreach (var tag in collection.Tags)
            {
                int len = 0;
                foreach (var c in tag.Title)
                {
                    WriteWord((ushort)c, buffer, len);
                    len += 2;
                }
                if (++idx < collection.Tags.Count)
                    buffer[len++] = 0x1e;
                else
                    buffer[len++] = 0x1d;
                stream.Write(buffer, 0, len);
            }

            // Images

            idx = 0;

            foreach (var img in collection.Images)
            {
                int len = 0;

                // Filename
                foreach (var c in img.Filename)
                {
                    WriteWord((ushort)c, buffer, len);
                    len += 2;
                }
                buffer[len++] = 0x1f;

                // Title
                foreach (var c in img.Title)
                {
                    WriteWord((ushort)c, buffer, len);
                    len += 2;
                }
                buffer[len++] = 0x1f;

                // Thumb extension
                foreach (var c in Path.GetExtension(img.Thumb.Filename))
                {
                    WriteWord((ushort)c, buffer, len);
                    len += 2;
                }
                buffer[len++] = 0x1f;

                // Tags
                for (int tagPos = 0; tagPos < collection.Tags.Count; tagPos++)
                {
                    if ((tagPos & 0x7) == 0)
                    {
                        if (tagPos > 0)
                            len++;
                        buffer[len] = 0;
                    }

                    buffer[len] |= (byte)((img.Tags.Contains(collection.Tags[tagPos]) ? 1 : 0) << (tagPos & 0x7));
                }

                if (++idx < collection.Images.Count)
                    buffer[len++] = 0x1e;
                else
                    buffer[len++] = 0x1d;
                stream.Write(buffer, 0, len);

                saver?.Invoke(img, (double)idx / collection.Images.Count);
            }
        }

        public static void Load(Collection collection, LoadHandler loader, Stream stream)
        {
            // Header

            var buffer = new byte[BufferSize];
            stream.Read(buffer, 0, Signature.Length);

            // TODO Check signature

            stream.Read(buffer, 0, 4);
            var imgCount = ByteArrayToDword(buffer);
            stream.Read(buffer, 0, 4);
            var tagCount = ByteArrayToDword(buffer);

            // Tags

            int bytesRead = 0;
            int idx = 0;
            var tags = new string[tagCount];
            var sb = new StringBuilder();

            while (idx < tagCount && (bytesRead = stream.Read(buffer, 0, BufferSize)) > 0)
            {
                for (int i = 0; i < bytesRead;)
                {
                    if (buffer[i] == 0x1e)
                    {
                        // Record separator
                        tags[idx++] = sb.ToString();
                        sb.Clear();
                        i++;
                        continue;
                    }

                    if (buffer[i] == 0x1d)
                    {
                        // Group separator
                        tags[idx++] = sb.ToString();
                        sb.Clear();
                        i++;
                        break;
                    }

                    var c = (char)ByteArrayToWord(buffer, i);
                    sb.Append(c);
                    i += 4;
                }
            }

            var tagsReal = tags.Select(x => Tag.Create(x)).ToList();

            // Images

            var images = new ImageData[imgCount];
            byte state = 0;
            var bits = new BitArray((int)tagCount);
            int bitPos = 0;

            while (idx < imgCount && (bytesRead = stream.Read(buffer, 0, BufferSize)) > 0)
            {
                for (int i = 0; i < bytesRead;)
                {
                    if (buffer[i] == 0x1f)
                    {
                        // Unit separator
                        switch (state)
                        {
                            case 0:
                                images[idx].Filename = sb.ToString();
                                break;
                            case 1:
                                images[idx].Title = sb.ToString();
                                break;
                            case 2:
                                images[idx].ThumbExt = sb.ToString();
                                break;
                        }
                        sb.Clear();
                        state++;
                        i++;
                        continue;
                    }

                    if (buffer[i] == 0x1e || buffer[i] == 0x1d)
                    {
                        //images[idx].Tags = new BitArray(bits);

                        var img = loader(images[idx].Filename, Path.ChangeExtension(images[idx].Filename, images[idx].ThumbExt), (double)idx / imgCount);
                        for (int tagPos = 0; tagPos < tagCount; tagPos++)
                        {
                            if (bits.Get(tagPos))
                                img.AddTag(tags[tagPos]);
                        }
                        collection.AddImage(img, false);

                        bitPos = 0;
                        idx++;

                        if (buffer[i++] == 0x1e)
                            continue;
                        else
                            break;
                    }

                    if (state < 3)
                    {
                        var c = (char)ByteArrayToWord(buffer, i);
                        sb.Append(c);
                        i += 4;
                    }
                    else
                    {
                        for (int z = 0; z < 8; z++)
                            bits.Set(bitPos * 8 + z, (buffer[i] & (1 << z)) > 0);

                        bitPos++;
                    }
                }
            }
        }
    }
}

