﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Breezy.PantEX
{
    public class Tag
    {
        public long ID { get; protected set; }
        public string Title { get; protected set; }

        protected Tag(string title)
        {
            Title = title;
            ID = Count++;
        }

        [JsonConstructor]
        public Tag(long id, string title)
        {
            ID = id;
            Title = title;
        }

        public override bool Equals(object obj)
        {
            var x = obj as Tag;
            return x != null && x.ID == ID;
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override string ToString()
        {
            return Title;
        }

        // Static members

        private static long Count;
        private static Dictionary<String, Tag> Instances;

        static Tag()
        {
            Instances = new Dictionary<String, Tag>();
        }

        public static Tag Create(string title)
        {
            Tag f;

            if (!Instances.TryGetValue(title, out f))
            {
                f = new Tag(title);
                Instances[title] = f;
            }

            return f;
        }
    }
}

