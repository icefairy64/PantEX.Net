﻿using System;
using Breezy.Assistant.Extensions;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using Breezy.Mars;
using NLog;
using System.Threading.Tasks;

namespace Breezy.PantEX
{
    [Configurable("Http")]
    public static class HttpSingleton
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public static HttpClient Client { get; private set; }
        public static HttpCooldownWrapper HttpCooldown { get; private set; }

        [Loader]
        public static void Load(IDictionary<string, object> dict)
        {
            var mh = new HttpClientHandler()
            {
                ClientCertificateOptions = ClientCertificateOption.Automatic,
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            };

            if (dict.TryGetValue("Proxy", out object rawProxy))
            {
                if (rawProxy is string proxy)
                {
                    Logger.Info($"Using proxy server {proxy}");
                    mh.Proxy = new WebProxy(proxy);
                }
            }

            Client = new HttpClient(mh);
            Client.DefaultRequestHeaders.UserAgent.Clear();
            Client.DefaultRequestHeaders.UserAgent.ParseAdd("PantEX/0.4");
            ServicePointManager.DefaultConnectionLimit = 1000;

            HttpCooldown = new HttpCooldownWrapper(
                TimeSpan.Zero,
                Client,
                x => x.TotalSeconds < 1 ? TimeSpan.FromSeconds(1) : TimeSpan.FromMilliseconds(x.Milliseconds * 2),
                x => x.TotalSeconds < 2 ? TimeSpan.Zero : TimeSpan.FromMilliseconds(x.Milliseconds / 2),
                Logger.Debug
            );
        }

        [Saver]
        public static void Save(IDictionary<string, object> dict)
        {

        }

        public static async Task<string> GetStringAsync(Uri requestUri)
        {
            var response = await Client.GetAsync(requestUri);
            var content = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
                return content;

            // Error handling

            Logger.Error("An error occured during HTTP request");
            Logger.Error("================");
            Logger.Error($"Response: {response.StatusCode}");
            Logger.Error(content);
            Logger.Error("Headers:");
            foreach (var header in response.Headers)
                Logger.Error($"{header.Key}: {header.Value}");
            Logger.Error("================");

            throw new HttpRequestException($"Failed to load {requestUri}");
        }

        public static async Task<string> GetStringAsync(string requestUri) => await GetStringAsync(new Uri(requestUri));
    }
}
