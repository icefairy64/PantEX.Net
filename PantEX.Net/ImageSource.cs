﻿using System;
using System.Collections.Generic;
using Breezy.Assistant.Extensions;
using Breezy.Assistant;

namespace Breezy.PantEX
{
    [Extendable]
    public abstract class ImageSource : PageIterator<EXImage>
    {
        public abstract string Title { get; }

        public ImageSource()
        {
        }

        public override abstract IEnumerable<EXImage> Load(int page);
        public abstract ImageSourceDescriptor CreateDescriptor();
    }
}

