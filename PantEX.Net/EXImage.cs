﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Breezy.Venus;

namespace Breezy.PantEX
{
    [Serializable]
    public abstract class EXImage
	{
        public static VenusContext Venus;

        [JsonIgnore]
        private int? FHash;

        [JsonIgnore]
        private int Hash
        {
            get
            {
                if (!FHash.HasValue)
                    FHash = Path.GetFileNameWithoutExtension(Filename).GetHashCode();
                return FHash.Value;
            }
        }

        [JsonProperty]
        protected List<Tag> FTags;
        [JsonIgnore]
        protected EXImage FThumb;
        [JsonIgnore]
        protected WeakReference<RawImage> ImageData;

        [JsonIgnore]
        public long ID { get; internal set; }
        [JsonProperty]
        public string Filename { get; set; }
        [JsonProperty]
        public string Title { get; set; }
        [JsonProperty]
        public Uri Source { get; set; }
        [JsonIgnore]
        public Collection Collection { get; internal set; }

        [JsonProperty]
        public EXImage Thumb 
        {
            get { return FThumb; }
            set
            {
                FThumb = value;
                FThumb.Filename = Path.GetFileNameWithoutExtension(Filename) + Path.GetExtension(FThumb.Filename);
            }
        }

        protected bool Loaded;

        [JsonIgnore]
        public IReadOnlyList<Tag> Tags
        {
            get { if (!Loaded) Load(); return FTags; }
        }

        protected EXImage()
		{
            FTags = new List<Tag>();
		}

        public void AddTag(string tag)
        {
            FTags.Add(Tag.Create(tag));
            Collection?.UpdateImage(this);
        }

        public void AddTag(Tag tag)
        {
            FTags.Add(tag);
            Collection?.UpdateImage(this);
        }

        public void RemoveTag(Tag tag)
        {
            FTags.Remove(tag);
        }

        public void AddTags(IEnumerable<string> tags, bool update = true)
        {
            FTags.AddRange(tags.Select(x => Tag.Create(x)));
            if (update)
                Collection?.UpdateImage(this);
        }

        public void AddTags(IEnumerable<Tag> tags, bool update = true)
        {
            FTags.AddRange(tags);
            if (update)
                Collection?.UpdateImage(this);
        }

        public override bool Equals(object obj)
        {
            return obj is EXImage && Hash == (obj as EXImage).Hash;
        }

        public override int GetHashCode()
        {
            return Hash;
        }

        public RawImage GetImage()
        {
            if (!Loaded)
                Load();
            if (ImageData == null || !ImageData.TryGetTarget(out RawImage data))
            {
                using (var stream = DataStream)
                {
                    try
                    {
                        data = Venus.Load(stream);
                        ImageData = new WeakReference<RawImage>(data);
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"An error occured while decoding image {this}", e);
                    }
                }
            }

            return data;
        }

        public async Task<RawImage> GetImageAsync()
        {
            if (!Loaded)
                Load();
            if (ImageData == null || !ImageData.TryGetTarget(out RawImage data))
            {
                using (var stream = await GetDataStreamAsync())
                {
                    try
                    {
                        data = Venus.Load(stream);
                        ImageData = new WeakReference<RawImage>(data);
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"An error occured while decoding image {this}", e);
                    }
                }
            }

            return data;
        }

        public void Load() {
            DoLoad();
            Loaded = true;
        }

        protected virtual void DoLoad() {
            
        }

        [JsonIgnore]
        public abstract Stream DataStream { get; }

        public abstract Task<Stream> GetDataStreamAsync();
	}
}

