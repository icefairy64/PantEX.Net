﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;
namespace Breezy.PantEX
{
    [Extendable]
    public interface IAction
    {
        string Title { get; }

        Task Perform(ProgressHandler progressHandler);
    }
}
