﻿using System;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX
{
    [Extendable]
    public interface ICollectionImporter
    {
        string Title { get; }

        void Configure();
        void Configure(CollectionDescriptor des);
        Task<Collection> Import(ProgressHandler progHandler);
        CollectionDescriptor GenerateDescriptor();
    }
}

