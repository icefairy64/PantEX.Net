﻿using System;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX
{
    public delegate void ProgressHandler(double progress);

    [Extendable]
    public interface ICollectionExporter
    {
        string Title { get; }

        void Configure();
        void Configure(CollectionDescriptor des);
        Task Export(Collection collection, ProgressHandler progHandler);
    }
}

