﻿using System;
using System.Net;
using System.Net.Http;
using Breezy.Mars;
using NLog;

namespace Breezy.PantEX
{
    public static class Common
    {
        public static HttpClient Http => HttpSingleton.Client;
        public static HttpCooldownWrapper HttpCooldown => HttpSingleton.HttpCooldown;
    }
}

