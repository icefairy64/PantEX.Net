﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;

namespace Breezy.PantEX
{
    [Extendable]
    public interface ICollectionAction : IAction
    {
        bool Configure(Collection collection);
    }
}

