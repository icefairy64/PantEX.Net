﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX
{
    [Extendable]
    public interface IImageAction : IAction
    {
        bool Configure(IEnumerable<EXImage> images);
    }
}

