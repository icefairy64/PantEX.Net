﻿using System;
using Breezy.Assistant.Extensions;

namespace Breezy.PantEX
{
    [Extendable]
    public abstract class ImageSourceFactory
    {
        public abstract string Provider { get; }
        public abstract string Title { get; }
        public abstract ImageSource Instance { get; }

        public abstract void Configure();

        protected ImageSourceFactory()
        {
        }
    }
}

