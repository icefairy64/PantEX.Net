﻿using System;
using System.Threading.Tasks;
using Breezy.Assistant.Extensions;
namespace Breezy.PantEX
{
    [Extendable]
    public interface IRemoteImageProvider
    {
        bool Match(Uri uri);
        Task<EXImage> CreateImageFromUri(Uri uri);
    }
}
