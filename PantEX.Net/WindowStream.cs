﻿using System;
using System.IO;

namespace Breezy.PantEX
{
    public class WindowStream : FileStream
    {
        private readonly long Offset;
        private readonly int FLength;
        private int Left;

        public WindowStream(string filename, long offset, int length)
            : base(filename, FileMode.Open)
        {
            Offset = offset;
            FLength = length;
            Left = length;
            Seek(offset, SeekOrigin.Begin);
        }

        public override long Length
        {
            get { return FLength; }
        }

        public override int Read(byte[] array, int offset, int count)
        {
            if (Left <= 0)
                return -1;

            if (Left >= count)
            {
                Left -= count;
                return base.Read(array, offset, count);
            }
            else
            {
                Left = 0;
                return base.Read(array, offset, Left);
            }
        }

        public override int ReadByte()
        {
            return Left-- <= 0 ? -1 : base.ReadByte();
        }

        public override long Position
        {
            get
            {
                return base.Position - Offset;
            }
            set
            {
                base.Position = value + Offset;
            }
        }
    }
}

