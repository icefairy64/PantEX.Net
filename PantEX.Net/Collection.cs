﻿using System;
using System.Collections.Generic;
using System.Linq;
using Breezy.Assistant;
using Newtonsoft.Json;
using System.IO;

namespace Breezy.PantEX
{
    public delegate void CollectionModificationHandler(object sender, CollectionModificationArgs args);

    public class CollectionModificationArgs
    {
        public Collection ModifiedCollection { get; private set; }
        public IEnumerable<EXImage> Items { get; private set; }

        public CollectionModificationArgs(Collection modifiedCollection, IEnumerable<EXImage> items)
        {
            ModifiedCollection = modifiedCollection;
            Items = items;
        }
    }

    public enum CollectionModificationKind
    {
        ImageAddition,
        ImageRemoval
    }

    public struct CollectionModification
    {
        public CollectionModificationKind Kind;
        public object Subject;

        public override string ToString()
        {
            return string.Format("[CollectionModification: Kind={0}, Subject={1}]", Kind, Subject);
        }
    }

    public class Collection
    {
        protected List<EXImage> FImages;
        protected HashSet<Tag> FTags;
        protected List<CollectionModification> FModifications;

        public string Title { get; set; }
        public CollectionDescriptor Descriptor { get; set; }

        public event CollectionModificationHandler ImagesAdded;
        public event CollectionModificationHandler ImagesRemoved;

        public IReadOnlyList<EXImage> Images
        {
            get { return FImages; }
        }

        public IReadOnlyList<Tag> Tags
        {
            get { return FTags.ToList(); }
        }

        public IReadOnlyList<CollectionModification> Modifications
        {
            get { return FModifications; }
        }

        public IDictionary<string, string> Metadata { get; private set; }

        public Collection(string title)
        {
            FImages = new List<EXImage>();
            FTags = new HashSet<Tag>();
            FModifications = new List<CollectionModification>();
            Metadata = new Dictionary<string, string>();
            Title = title;
        }

        public override string ToString()
        {
            return Title;
        }

        private void EnqueueImageAddition(EXImage img)
        {
            if (!FModifications.Exists(x => x.Kind == CollectionModificationKind.ImageRemoval && x.Subject.Equals(img)))
                FModifications.Add(new CollectionModification { Kind = CollectionModificationKind.ImageAddition, Subject = img });
            else
                FModifications.RemoveAll(x => x.Kind == CollectionModificationKind.ImageRemoval && x.Subject.Equals(img));
        }

        private void EnqueueImageRemoval(EXImage img)
        {
            if (!FModifications.Exists(x => x.Kind == CollectionModificationKind.ImageAddition && x.Subject.Equals(img)))
                FModifications.Add(new CollectionModification { Kind = CollectionModificationKind.ImageRemoval, Subject = img });
            else
                FModifications.RemoveAll(x => x.Kind == CollectionModificationKind.ImageAddition && x.Subject.Equals(img));
        }

        public void AddImage(EXImage img, bool isModification = true)
        {
            foreach (var tag in img.Tags)
                FTags.Add(tag);

            FImages.Add(img);
            img.Collection = this;
            ImagesAdded?.Invoke(this, new CollectionModificationArgs(this, img.ToEnumerable()));
            if (isModification)
                EnqueueImageAddition(img);
        }

        public void AddImages(IEnumerable<EXImage> images, bool isModification = true)
        {
            foreach (var img in images)
            {
                foreach (var tag in img.Tags)
                    FTags.Add(tag);

                img.Collection = this;
                if (isModification)
                    EnqueueImageAddition(img);
            }
            FImages.AddRange(images);
            //if (isModification)
            //    FModifications.AddRange(images.Select(img => new CollectionModification { Kind = CollectionModificationKind.ImageAddition, Subject = img }));
            ImagesAdded?.Invoke(this, new CollectionModificationArgs(this, images));
        }

        public void RemoveImage(EXImage img, bool isModification = true)
        {
            FImages.Remove(img);
            if (isModification)
                EnqueueImageRemoval(img);
            ImagesRemoved?.Invoke(this, new CollectionModificationArgs(this, img.ToEnumerable()));
        }

        public void RemoveImages(IEnumerable<EXImage> images, bool isModification = true)
        {
            foreach (var img in images)
            {
                FImages.Remove(img);
                if (isModification)
                    EnqueueImageRemoval(img);
            }
            ImagesRemoved?.Invoke(this, new CollectionModificationArgs(this, images));
        }

        internal void UpdateImage(EXImage img)
        {
            foreach (var tag in img.Tags)
                FTags.Add(tag);
        }

        public void ClearModifications()
        {
            FModifications.Clear();
        }

        // Static members

        private static readonly JsonSerializer Json;

        static Collection()
        {
            Json = new JsonSerializer
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                TypeNameHandling = TypeNameHandling.Auto,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public static void DumpModifications(Collection collection, Stream stream)
        {
            using (var writer = new JsonTextWriter(new StreamWriter(stream)))
                Json.Serialize(writer, collection.FModifications);
        }
    }
}

