﻿using System;
using Newtonsoft.Json.Linq;

namespace Breezy.PantEX
{
    public struct CollectionDescriptor
    {
        public string Title;
        public string ImporterTypeName;
        public string Descriptor;

        public CollectionDescriptor WithTitle(string value)
        {
            return new CollectionDescriptor { Title = value, ImporterTypeName = this.ImporterTypeName, Descriptor = this.Descriptor };
        }

        public CollectionDescriptor WithImporter(string value)
        {
            return new CollectionDescriptor { Title = this.Title, ImporterTypeName = value, Descriptor = this.Descriptor };
        }

        public CollectionDescriptor WithDescriptor(string value)
        {
            return new CollectionDescriptor { Title = this.Title, ImporterTypeName = this.ImporterTypeName, Descriptor = value };
        }

        public override string ToString()
        {
            return Title;
        }

        public static explicit operator CollectionDescriptor(JObject token)
        {
            return new CollectionDescriptor
            {
                Title = token["Title"].Value<string>(),
                ImporterTypeName = token["ImporterTypeName"].Value<string>(),
                Descriptor = token["Descriptor"].Value<string>()
            };
        }
    }
}

