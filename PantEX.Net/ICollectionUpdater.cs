﻿using System;
using Breezy.Assistant.Extensions;
using System.Threading.Tasks;

namespace Breezy.PantEX
{
    [Extendable]
    public interface ICollectionUpdater
    {
        void Configure(Collection collection);
        Task AddImage(EXImage image);
        Task RemoveImage(EXImage image);
        Task WriteUpdates();
    }
}

