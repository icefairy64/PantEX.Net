﻿using System;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Threading.Tasks;
using NLog;
using Newtonsoft.Json;

namespace Breezy.PantEX.Images
{
    [Serializable]
    public class HttpImage : EXImage
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        [JsonProperty]
        public string Url;

        [JsonIgnore]
        public Func<HttpRequestMessage> RequestFactory;

        private FileImage Cache;
        private FileInfo FilePath;

        public HttpImage()
        {
        }

        public HttpImage(string url)
        {
            Url = url;
            var u = new Uri(url);
            Filename = u.Segments[u.Segments.Length - 1];
            Title = Filename;
        }

        public HttpImage(string url, string thumbUrl)
            : this(url)
        {
            Thumb = new HttpImage(thumbUrl);
        }

        public HttpImage(Func<HttpRequestMessage> requestFactory, string thumbUrl)
            : this(requestFactory.Invoke().RequestUri.ToString())
        {
            RequestFactory = requestFactory;
            Thumb = new HttpImage(thumbUrl);
        }

        ~HttpImage()
        {
            if (FilePath != null && FilePath.Exists)
                FilePath.Delete();
        }

        [JsonIgnore]
        public override Stream DataStream
        {
            get
            {
                try
                    {
                    if (Cache == null)
                    {
                        var result = RequestFactory == null ? Common.HttpCooldown.GetAsync(new Uri(Url)).Result : Common.Http.SendAsync(RequestFactory.Invoke()).Result;
                        using (var stream = result.Content.ReadAsStreamAsync().Result)
                        {
                            FilePath = new FileInfo(Path.GetTempFileName());
                            using (var os = File.Create(FilePath.FullName))
                                stream.CopyTo(os);
                            Cache = new FileImage(FilePath.FullName);
                        }
                    }
                    return Cache.DataStream;
                    }
                catch (Exception e)
                {
                    throw new Exception($"Error while loading remote image {this}", e);
                }
            }
        }

        public override async Task<Stream> GetDataStreamAsync()
        {
            try
            {
                if (Cache == null)
                {
                    var result = RequestFactory == null ? await Common.HttpCooldown.GetAsync(new Uri(Url)) : await Common.Http.SendAsync(RequestFactory.Invoke());
                    using (var stream = await result.Content.ReadAsStreamAsync())
                    {
                        FilePath = new FileInfo(Path.GetTempFileName());
                        using (var os = File.Create(FilePath.FullName))
                            await stream.CopyToAsync(os);
                        Cache = new FileImage(FilePath.FullName);
                    }
                }
                return await Cache.GetDataStreamAsync();
            }
            catch (Exception e)
            {
                throw new Exception($"Error while loading remote image {this}", e);
            }
        }

        public override string ToString()
        {
            return $"[HttpImage: Url: {Url}, Thumb={Thumb}]";
        }
        
    }
}

