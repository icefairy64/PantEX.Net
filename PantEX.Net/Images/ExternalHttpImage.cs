﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Breezy.PantEX.Images
{
    [Serializable]
    public class ExternalHttpImage : EXImage
    {
        private readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        [JsonProperty]
        public Uri PageUri { get; }

        [JsonProperty]
        public Func<string, Uri> ImageResolver { get; }

        [JsonIgnore]
        private HttpImage UnderlyingImage;

        public ExternalHttpImage(Uri pageUri, Func<string, Uri> imageResolver)
        {
            Filename = pageUri.Segments[pageUri.Segments.Length - 1];
            ImageResolver = imageResolver;
            PageUri = pageUri;
        }

        public override Stream DataStream
        {
            get
            {
                if (UnderlyingImage == null)
                {
                    Logger.Debug($"Loading underlying image from {PageUri}");
                    var html = Common.Http.GetStringAsync(PageUri).Result;
                    var imageUri = ImageResolver.Invoke(html).AbsoluteUri;
                    Logger.Debug($"Got {imageUri}");
                    UnderlyingImage = new HttpImage(imageUri);
                }
                return UnderlyingImage.DataStream;
            }
        }

        public override async Task<Stream> GetDataStreamAsync()
        {
            if (UnderlyingImage == null)
            {
                Logger.Debug($"Loading underlying image from {PageUri}");
                var html = await Common.Http.GetStringAsync(PageUri);
                var imageUri = ImageResolver.Invoke(html).AbsoluteUri;
                Logger.Debug($"Got {imageUri}");
                UnderlyingImage = new HttpImage(imageUri);
            }
            return await UnderlyingImage.GetDataStreamAsync();
        }

        public override int GetHashCode()
        {
            return PageUri.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var img = obj as ExternalHttpImage;
            return img != null && img.PageUri.Equals(PageUri);
        }
    }
}
