﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Breezy.PantEX.Images
{
    [Serializable]
    public class FileImage : EXImage
    {
        [JsonProperty]
        public FileInfo File;

        public FileImage(string filename)
            : base()
        {
            File = new FileInfo(filename);
            Filename = File.Name;
            Title = Filename;
        }

        [JsonIgnore]
        public override Stream DataStream
        {
            get { return File.Open(FileMode.Open, FileAccess.Read, FileShare.Read); }
        }

        public override async Task<Stream> GetDataStreamAsync()
        {
            return await Task.FromResult(File.Open(FileMode.Open, FileAccess.Read, FileShare.Read));
        }

        public override string ToString()
        {
            return string.Format("[FileImage: {0}]", File);
        }
    }
}

