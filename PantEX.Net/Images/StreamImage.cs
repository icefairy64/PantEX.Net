﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Breezy.PantEX.Images
{
    public class StreamImage : EXImage
    {
        private readonly Stream Stream;

        public StreamImage(Stream stream, string filename)
            : base()
        {
            Stream = stream;
            Filename = filename;
            Title = filename;
        }

        public override Stream DataStream
        {
            get { return Stream; }
        }

        public override async Task<Stream> GetDataStreamAsync()
        {
            return await Task.FromResult(Stream);
        }
    }
}

