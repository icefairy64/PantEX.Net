﻿using System;

namespace Breezy.PantEX
{
    public static class Extension
    {
        #region Collection

        public static bool IsWebPForced(this Collection col)
        {
            return col.Metadata.ContainsKey(PantEX.JsonPack.ForceWebPOption) 
                ? bool.Parse(col.Metadata[PantEX.JsonPack.ForceWebPOption]) 
                : false;
        }

        public static string GetForcedFormat(this Collection col)
        {
            return col.Metadata.ContainsKey(PantEX.JsonPack.ForceFormatOption) 
                ? col.Metadata[PantEX.JsonPack.ForceFormatOption] 
                : null;
        }

        #endregion
    }
}

