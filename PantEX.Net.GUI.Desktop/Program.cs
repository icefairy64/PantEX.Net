﻿using System;
using Eto;
using Eto.Forms;
using Breezy.PantEX.GUI;
using NLog;
using System.IO;
using Newtonsoft.Json;
using Breezy.PantEX;

namespace PantEX.Net.GUI.Desktop
{
    public class Program
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        [STAThread]
        public static void Main(string[] args)
        {
            Logger.Info($"PantEX.Net is running on {Environment.OSVersion} (Runtime: {Environment.Version}); 64-bit: {Environment.Is64BitProcess}");

            GLib.ExceptionManager.UnhandledException += (e) => 
                {
                    Logger.Error(e.ExceptionObject);
                    Logger.Info("Dumping unsaved collection modifications...");
                    foreach (var col in CollectionManager.Collections)
                    {
                        if (col.Modifications.Count > 0)
                        {
                            using (var stream = FileManager.GetBackupFile($"{col.Title}.mod.json").Create())
                                Collection.DumpModifications(col, stream);
                        }
                    }
                    Logger.Info("Removing temporary files...");
                    FileManager.RemoveTempFiles();
                    e.ExitApplication = true;
                };

            var platform = Eto.Platform.Get(Eto.Platforms.Gtk3);

            if (platform == null) {
                Logger.Info("Failed to load GTK3 platform; falling back to GTK2");
                platform = Eto.Platform.Get(Eto.Platforms.Gtk2);
            }

            Application app;

            if (platform == null)
            {
                Logger.Info("Failed to load GTK2 platform; using auto-detected");
                app = new Application();
                Logger.Info($"Platform: {app.Platform.ToString()}");
            } 
            else {
                Logger.Info($"Platform: {platform.ToString()}");
                app = new Application(platform);
            }

            app.Run(new MainForm());

            FileManager.RemoveTempFiles();
        }
    }
}
