﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Breezy.Derpibooru.Model;

namespace Breezy.PantEX.Derpibooru
{
    public class DerpibooruTopScoring : ImageSourceFactory
    {
        public override string Provider => "Derpibooru";

        public override string Title => "Top scoring";

        public override ImageSource Instance => new DerpibooruTopScoringSource();

        public override void Configure()
        {
            
        }
    }

    public class DerpibooruTopScoringSource : AbstractDerpibooruSource
    {
        public DerpibooruTopScoringSource()
        {
            
        }

        public DerpibooruTopScoringSource(ImageSourceDescriptor descriptor)
        {
            
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = "",
                TypeName = typeof(DerpibooruTopScoringSource).FullName
            };
        }

        protected override async Task<IEnumerable<DerpibooruImage>> RetrieveImages(int page)
        {
            return (await DerpibooruAPI.GetTopScoring(page)).Values.First();
        }
    }
}
