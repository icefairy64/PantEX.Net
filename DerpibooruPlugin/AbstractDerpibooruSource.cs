﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.Derpibooru;
using Breezy.Mars.Rest;
using Breezy.Derpibooru.Model;
using Breezy.PantEX.Images;
using System.Linq;

namespace Breezy.PantEX.Derpibooru
{
    public abstract class AbstractDerpibooruSource : ImageSource
    {
        protected static readonly IDerpibooruAPI DerpibooruAPI;

        static AbstractDerpibooruSource()
        {
            var dapiType = RestImplementator.CreateImplementation(typeof(IDerpibooruAPI));
            DerpibooruAPI = RestImplementator.CreateInstance<IDerpibooruAPI>(dapiType, Common.Http);
        }

        public override string Title => "Derpibooru";

        protected abstract Task<IEnumerable<DerpibooruImage>> RetrieveImages(int page);

        private EXImage ToEXImage(DerpibooruImage image) {
            var exImg = new HttpImage("https:" + image.ImagePath);
            var exThumb = new HttpImage("https:" + image.RepresentationPaths["thumb"]);
            exImg.Thumb = exThumb;
            exImg.AddTags(image.Tags.Replace(", ", ",").Split(','), false);
            return exImg;
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            return RetrieveImages(page + 1).Result.Select(ToEXImage);
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            return (await RetrieveImages(page + 1)).Select(ToEXImage);
        }
    }
}
