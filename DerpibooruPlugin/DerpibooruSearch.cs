﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.Derpibooru.Model;
using System.Linq;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Derpibooru
{
    public class DerpibooruSearch : ImageSourceFactory
    {
        public override string Provider => "Derpibooru";

        public override string Title => "Search";

        public override ImageSource Instance => new DerpibooruSearchSource(Query);

        private string Query;

        public override void Configure()
        {
            Query = new InputDialog("Query").Ask();
            Query = Query.Replace(' ', '+');
        }
    }

    public class DerpibooruSearchSource : AbstractDerpibooruSource
    {
        readonly string Query;

        public DerpibooruSearchSource(string query)
        {
            Query = query;
        }

        public DerpibooruSearchSource(ImageSourceDescriptor descriptor)
        {
            Query = descriptor.Descriptor;
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = Query,
                TypeName = typeof(DerpibooruSearchSource).FullName
            };
        }

        protected override async Task<IEnumerable<DerpibooruImage>> RetrieveImages(int page)
        {
            return (await DerpibooruAPI.Search(page, Query)).Images;
        }
    }
}
