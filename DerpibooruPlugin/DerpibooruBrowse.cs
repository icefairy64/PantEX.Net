﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.Derpibooru.Model;
using System.Linq;

namespace Breezy.PantEX.Derpibooru
{
    public class DerpibooruBrowse : ImageSourceFactory
    {
        public override string Provider => "Derpibooru";

        public override string Title => "Browse";

        public override ImageSource Instance => new DerpibooruBrowseSource();

        public override void Configure()
        {
            
        }
    }

    public class DerpibooruBrowseSource : AbstractDerpibooruSource
    {
        public DerpibooruBrowseSource()
        {
        }

        public DerpibooruBrowseSource(ImageSourceDescriptor descriptor) 
        {
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = "",
                TypeName = typeof(DerpibooruBrowseSource).FullName
            };
        }

        protected override async Task<IEnumerable<DerpibooruImage>> RetrieveImages(int page)
        {
            return (await DerpibooruAPI.GetImages(page)).Values.First();
        }
    }
}
