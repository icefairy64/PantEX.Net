﻿namespace Breezy.PantEX.FSharp

module Bits =

    let choiceOr d c =
        match c with
        | Choice1Of2 x -> x
        | Choice2Of2 x -> d

    let choiceOrThrow c =
        match c with
        | Choice1Of2 x -> x
        | Choice2Of2 x -> raise x