﻿namespace Breezy.PantEX.FSharp

open System
open System.Net.Http
open Breezy.PantEX
open FSharpx.Choice
    
module Http = 

    let tryFetchString (url : string) =
        try
            Choice1Of2 (HttpSingleton.GetStringAsync (url)).Result
        with
            | err -> Choice2Of2 err

    let tryFetchStringAsync (url : string) =
        let a = 
            async {
                return! Async.AwaitTask (HttpSingleton.GetStringAsync (url))
            }
        Async.Catch a