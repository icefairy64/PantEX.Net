﻿namespace Breezy.PantEX.F3.Sources

open Breezy.PantEX
open Breezy.PantEX.Images
open Breezy.Assistant.FSharp.Regex
open Breezy.PantEX.FSharp.Http
open Breezy.PantEX.FSharp.Bits

open Newtonsoft.Json

open System
open System.Text.RegularExpressions
open System.Net.Http

open FSharpx.Option
open FSharpx.Strings
open FSharpx.Lazy
open FSharpx.Choice

type hentaiFoundryImage () =
    inherit EXImage ()

    let mutable url : string = null
    let mutable fullImage : EXImage = null

    let imagePattern = Regex ("<span class=\"imageTitle\">.+?<div class=\"boxbody\".+?src=\"(?<imageUrl>.+?)\"", RegexOptions.Singleline)
    let tagsPattern  = Regex ("search_in=keywords\">(?<tag>.+?)<", RegexOptions.Singleline)

    [<JsonProperty ("OriginalUrl")>]
    member this.purl
        with get () = url 
        and set (v) = url <- v 

    override this.DoLoad () =
        let task = Common.Http.GetStringAsync (url)
        let html = task.Result
        let imageUrl = findMatchesNamed imagePattern html |> Seq.item 0 |> Map.find "imageUrl"
        fullImage <- Images.HttpImage ("http:" + imageUrl)

        findMatchesNamed tagsPattern html |> Seq.map (Map.find "tag") |> Seq.iter (fun x -> fullImage.AddTag (x))

    override this.DataStream =
        fullImage.DataStream

    override this.GetDataStreamAsync () =
        let a = async {
            return! Async.AwaitTask (fullImage.GetDataStreamAsync ())
        }
        Async.StartAsTask a

    override this.ToString () = String.Format ("[HentaiFoundryImage: {0}, Thumb: {1}]", url, this.Thumb)

    static member createImage aurl =
        let img = hentaiFoundryImage ()
        img.purl <- aurl
        img

type hentaiFoundrySource (url : string) =
    inherit ImageSource ()

    let url = url

    let root = "http://www.hentai-foundry.com"
    let postfix = "page/%d?enterAgree=1&size=0"

    let thumbPattern = Regex ("<a class=\"thumbLink\" href=\"(?<full>.+?)\"><span title=\"(?<title>.+?)\" class=\"thumb\" style=\"background-image: url\(\/\/(?<thumbUrl>.+?)\)\"><\/span>", RegexOptions.Singleline)

    let last a =
        Array.get a ((Array.length a) - 1)

    let loadImage tm =
        let full = root + Map.find "full" tm
        let img = hentaiFoundryImage.createImage full

        img.Source <- Uri (full)
        img.Thumb <- HttpImage ("http://" + Map.find "thumbUrl" tm)
        img.Filename <- last img.Source.Segments
        img.Title <- Map.find "title" tm

        img :> EXImage

    let parseHtml html = 
           findMatchesNamed thumbPattern html
           |> Seq.map loadImage

    let loadAsync page = async {
        let! a = tryFetchStringAsync (replace "%d" ((page + 1).ToString ()) (url + postfix))
        let res =
            choose {
                let! html = a
                return parseHtml html
            }
        return choiceOr Seq.empty res
    }

    new (des : ImageSourceDescriptor) =
        hentaiFoundrySource des.Descriptor

    override this.Load (page : int) =
        let res =
            choose {
                let! html = tryFetchString (replace "%d" ((page + 1).ToString ()) (url + postfix))
                return parseHtml html
            }
        choiceOr Seq.empty res

    override this.LoadAsync (page : int) =
        Async.StartAsTask (loadAsync page)

    override this.CreateDescriptor () =
        let mutable des = ImageSourceDescriptor ()
        des.TypeName <- typeof<hentaiFoundrySource>.FullName
        des.Descriptor <- url
        des

    override this.Title = "Hentai Foundry"

module HentaiFoundrySource =

    type recent () =
        inherit ImageSourceFactory ()
        override this.Configure () = ()
        override this.Instance = hentaiFoundrySource "http://www.hentai-foundry.com/pictures/recent/all/" :> ImageSource
        override this.Provider = "Hentai Foundry"
        override this.Title = "Recent"

    type popular () =
        inherit ImageSourceFactory ()
        override this.Configure () = ()
        override this.Instance = hentaiFoundrySource "http://www.hentai-foundry.com/pictures/popular/" :> ImageSource
        override this.Provider = "Hentai Foundry"
        override this.Title = "Popular"