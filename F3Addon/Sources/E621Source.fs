﻿namespace Breezy.PantEX.F3.Sources

open System
open System.Collections.Generic
open System.Threading.Tasks
open Breezy.PantEX
open Breezy.PantEX.GUI
open Breezy.Assistant.FSharp
open FSharpx.Option
open FSharpx.Strings

type e621Source(url : string) = 
    inherit ImageSource()

    let url = url

    let isObject json = 
        match json with
        | Json.Object x -> Some json
        | _ -> None

    let tryGetString = function
        | Json.Text x -> Some x
        | _ -> None

    let tryFindString k m =
        Map.tryFind k m >>= tryGetString

    let boolOption f v =
        match f v with
        | true  -> Some v
        | false -> None

    let checkUri s =
        Uri.IsWellFormedUriString (s, UriKind.Absolute)

    let extractImage json =
        match json with
        | Json.Object obj -> maybe {
                let! fileUrl = tryFindString "file_url" obj
                let! thumbUrl = tryFindString "preview_url" obj
                let! tags = tryFindString "tags" obj >>= (fun x -> split ' ' x |> Seq.ofArray |> Some)

                let img = Breezy.PantEX.Images.HttpImage (fileUrl, thumbUrl)
                img.AddTags (tags, false)

                tryFindString "source" obj 
                >>= boolOption checkUri
                >>= (fun x -> img.Source <- Uri (x); None) |> ignore

                return img :> EXImage
            }
        | _ -> None

    let extractImages html =
        let data = Json.parse html

        let l =
            match data with
            | Json.Array z -> z
            | _ -> failwith "Haven't received a JSON array"
               
        List.choose isObject l
        |> Seq.ofList
        |> Seq.map extractImage
        |> Seq.choose (fun x -> x)

    let loadAsync (page : int) = async {
        let! html = Async.AwaitTask (HttpSingleton.GetStringAsync (url.Replace ("%d", (page + 1).ToString ())))
        return extractImages html
    }

    new (des : ImageSourceDescriptor) =
        e621Source des.Descriptor

    override x.Load (page : int) =
        let task = HttpSingleton.GetStringAsync (url.Replace ("%d", (page + 1).ToString ()))
        extractImages task.Result

    override x.LoadAsync (page : int) =
        Async.StartAsTask (loadAsync page)

    override x.CreateDescriptor () =
        let mutable des = ImageSourceDescriptor ()
        des.TypeName <- typeof<e621Source>.FullName
        des.Descriptor <- url
        des

    override x.Title = "e621"

module E621Source =
    let browseUrl = "https://e621.net/post/index.json?page=%d"
    let searchByTagUrl = "https://e621.net/post/index.json?tags=%s&page=%d"

    let browse () = e621Source browseUrl
    let searchByTag tags = e621Source (searchByTagUrl.Replace ("%s", tags))