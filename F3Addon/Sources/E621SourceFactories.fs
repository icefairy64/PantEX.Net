﻿namespace Breezy.PantEX.F3.Sources

open System
open Breezy.PantEX
open Breezy.PantEX.GUI

type E621Browse() = 
    inherit ImageSourceFactory()

    override x.Configure () = ()

    override this.Provider = "e621"

    override this.Title = "Browse"

    override this.Instance
        with get () = E621Source.browse () :> ImageSource

type E621SearchByTags() = 
    inherit ImageSourceFactory()

    let mutable tags = ""

    override x.Configure () =
        let dialog = new InputDialog ("Tags", "")
        tags <- dialog.Ask ()
        dialog.Dispose ()

    override this.Provider = "e621"

    override this.Title = "Search by tags"

    override this.Instance
        with get () = E621Source.searchByTag tags :> ImageSource