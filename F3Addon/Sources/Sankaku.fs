﻿namespace Breezy.PantEX.F3.Sources

open System
open System.Collections.Generic
open System.Text.RegularExpressions
open System.IO
open Breezy.PantEX
open Breezy.Assistant.FSharp.Regex
open Breezy.PantEX.FSharp.Http
open Breezy.PantEX.FSharp.Bits
open Breezy.PantEX.GUI
open FSharpx.Option
open FSharpx.Strings
open FSharpx.Lazy
open FSharpx.Choice
open Eto.Forms
open Newtonsoft.Json

[<Serializable>]
type sankakuImage () =
    inherit EXImage ()

    let imagePattern = Regex ("a href=\"(?<imageUrl>.+?)\" id=highres itemprop=contentUrl")

    let mutable url : string = null

    let convertUrl url =
        replace "&amp;" "&" url

    let back = lazy (
            let task = Common.Http.GetStringAsync (url)
            let html = task.Result
            let imageUrl = findMatchesNamed imagePattern html |> Seq.item 0 |> Map.find "imageUrl" |> convertUrl
            Images.HttpImage ("https:" + imageUrl)
        )

    [<JsonProperty ("OriginalUrl")>]
    member this.purl
        with get () = url 
        and set (v) = url <- v |> convertUrl

    static member createImage aurl =
        let img = sankakuImage ()
        img.purl <- aurl
        img

    override this.DataStream =
        let real = 
            match url with
            | null -> this.Source.AbsoluteUri
            | _    -> url
        
        url <- real
        
        let img = force back
        img.DataStream

    override this.GetDataStreamAsync () =
        let real = 
            match url with
            | null -> this.Source.AbsoluteUri
            | _    -> url
        
        url <- real

        let a = async {
            let img = force back
            return! Async.AwaitTask (img.GetDataStreamAsync ())
        }
        Async.StartAsTask a

    override this.ToString () = String.Format ("[SankakuImage: {0}, Thumb: {1}]", url, this.Thumb)

type sankakuSource (url : string) =
    inherit ImageSource ()

    let url = url

    // full in thumb : "/post/show/*" -> append root
    // images : "//some.content.srv/*" -> append scheme

    let root = url.Substring (0, url.IndexOf ('/', url.IndexOf ('.')))

    let thumbPattern = Regex ("span class=\"thumb.+?a href=\"(?<full>.+?)\".+?img class=.*?preview.*?src=\"(?<thumbFile>.+?)\".+?title=\"(?<tags>.+?)\"", RegexOptions.Singleline)
    let tagPattern   = Regex ("a href=\"\/?tags=(?<tag>.+?)\"")

    let parseTags str =
        split ' ' str
        |> Seq.ofArray
        |> Seq.filter (fun x -> contains "Score:" x = false && contains "Size:" x = false && contains "User:" x = false)
        |> Seq.map (fun x -> match x with
                             | z when contains "Rating:" z -> z.ToLower ()
                             | z -> z)

    let last a =
        Array.get a ((Array.length a) - 1)

    let loadImage tm =
        let src = root + (Map.find "full" tm)
        let img = sankakuImage.createImage src
        let thumbUrl = "https:" + Map.find "thumbFile" tm

        img.Source <- Uri (src)
        img.Thumb <- Images.HttpImage (thumbUrl) :> EXImage

        let uri = Uri (thumbUrl)
        img.Filename <- last uri.Segments
        img.Title <- last uri.Segments

        img.AddTags (Map.find "tags" tm |> parseTags, false)

        img :> EXImage

    let parseHtml html = 
        findMatchesNamed thumbPattern html
        |> Seq.map loadImage
    
    let loadAsync page = async {
        let! a = tryFetchStringAsync (replace "%d" ((page + 1).ToString ()) url)
        let res =
            choose {
                let! html = a
                return parseHtml html
            }
        return choiceOr Seq.empty res
    }

    new (des : ImageSourceDescriptor) =
        sankakuSource des.Descriptor

    override this.Load (page : int) =
        let res =
            choose {
                let! html = tryFetchString (replace "%d" ((page + 1).ToString ()) url)
                return parseHtml html
            }
        choiceOr Seq.empty res

    override this.LoadAsync (page : int) =
        Async.StartAsTask (loadAsync page)

    override this.CreateDescriptor () =
        let mutable des = ImageSourceDescriptor ()
        des.TypeName <- typeof<sankakuSource>.FullName
        des.Descriptor <- url
        des
    
    override this.Title = "Sankaku"

module SankakuSource =
    let browse sub = sankakuSource (replace "%s" sub "https://%s.sankakucomplex.com/?page=%d")
    let search sub query = sankakuSource (replace "%s" sub "https://%s.sankakucomplex.com/?tags=%t&commit=Search&page=%d" |> replace "%t" query)

    type browseChan () =
        inherit ImageSourceFactory ()
        override this.Configure () = ()
        override this.Instance = browse "chan" :> ImageSource
        override this.Provider = "Sankaku Channel"
        override this.Title = "Browse"

    type browseIdol () =
        inherit ImageSourceFactory ()
        override this.Configure () = ()
        override this.Instance = browse "idol" :> ImageSource
        override this.Provider = "Sankaku Idol"
        override this.Title = "Browse"

    type searchChan () =
        inherit ImageSourceFactory ()

        let mutable query = ""

        override this.Configure () = 
            let d = new InputDialog ("Query")
            query <- d.Ask ()

        override this.Instance = search "chan" query :> ImageSource
        override this.Provider = "Sankaku Channel"
        override this.Title = "Search"

    type searchIdol () =
        inherit ImageSourceFactory ()

        let mutable query = ""

        override this.Configure () = 
            let d = new InputDialog ("Query")
            query <- d.Ask ()

        override this.Instance = search "idol" query :> ImageSource
        override this.Provider = "Sankaku Idol"
        override this.Title = "Search"
