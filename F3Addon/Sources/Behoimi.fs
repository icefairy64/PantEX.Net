﻿namespace Breezy.PantEX.F3.Sources

open System
open Breezy.PantEX
open Breezy.PantEX.Sources

type behoimiSource (url : string) =
    inherit AbstractDanbooruSource (url, "http://behoimi.org")

    override this.CreateDescriptor () =
        let img = base.CreateDescriptor ()
        img.TypeName <- typeof<behoimiSource>.FullName
        img

    override this.Title = "Behoimi"

module Behoimi =
    let browseUrl = "http://behoimi.org/post/index.json?page={0}"

    type browse () = 
        inherit ImageSourceFactory ()
        override this.Configure () = ()
        override this.Instance = behoimiSource browseUrl :> ImageSource
        override this.Provider = "Behoimi"
        override this.Title = "Browse"