﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Breezy.PantEX.Sources;
using Newtonsoft.Json;
using System.Collections.Generic;
using NLog;

namespace Breezy.PantEX.ImageProviders
{
    public class GelbooruImageProvider : IRemoteImageProvider
    {
        readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public async Task<EXImage> CreateImageFromUri(Uri uri)
        {
            var postId = uri.Query.Split('&').Where(x => x.Substring(0, 3) == "id=").First().Substring(3);
            var content = await HttpSingleton.GetStringAsync(new Uri("https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&id={postId}".Replace("{postId}", postId)));
            var list = JsonConvert.DeserializeObject<List<GelbooruImageModel>>(content);
            var model = list?[0];
            if (model != null)
                return GelbooruSource.CreateEXImage(model);
            else
                return null;
        }

        public bool Match(Uri uri)
        {
            Logger.Trace($"Probing URI: {uri}, host: {uri?.Host}");
            return uri?.Host == "gelbooru.com";
        }
    }
}
