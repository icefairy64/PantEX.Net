﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Linq;
using System.IO;
using Breezy.Assistant;
using NLog;
using System.Net.Http;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Breezy.PantEX.Images;

namespace Breezy.PantEX
{
    internal static class ImgurHelper
    {
        private readonly static ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly static string AppID = "ed9584280dc8e4b";

        private readonly static Regex[] Patterns = 
            {
                new Regex(@"data-src=""(.+?)"""),
                new Regex(@"twitter:image.+?content=""(.+?)"""),
                new Regex(@"class=""(?:[a-z0-9\-\ ]*?\\ )??image(?:\ ?[a-z0-9\-\ ]*?)??"".+?(?:img|a).+?(?:src|href)=""(.+?)""", RegexOptions.Singleline),
                new Regex(@"class=""[^""]+?post-image.+?img.+?src=""(.+?)""", RegexOptions.Singleline),
                new Regex(@"property=""og:image"".+?content=""(.+?)""")
            };

        private readonly static Regex AlbumIDPattern = new Regex(@"imgur\.com/a/([^/]+)/?");

        public static async Task<IEnumerable<EXImage>> FetchAlbum(string url)
        {
            var content = await Common.Http.GetStringAsync(url);
            var hashes = new List<string>();

            foreach (var pat in Patterns)
            {
                foreach (var match in Processor.Process(pat, content))
                {
                    var data = match.ElementAt(0);
                    var filename = data.Substring(data.LastIndexOf('/') + 1);
                    if ((!data.Contains("i.imgur") && !data.Contains("imgur2_")) || filename.Length < 7 || !filename.Contains('.'))
                        continue;

                    var hash = filename.Substring(0, 7) + Path.GetExtension(filename);
                    if (hash.IndexOf('?') > 0)
                        hash = hash.Substring(0, hash.IndexOf('?'));
                    Logger.Trace($"Adding hash {hash}");
                    hashes.Add(hash);
                }
            }

            return hashes.Distinct(StringComparer.CurrentCulture)
                .Where(x => Path.GetFileNameWithoutExtension(x).Length > 0)
                .Select(x => Path.ChangeExtension(x, ".webp"))
                .Select(x => new Images.HttpImage("http://i.imgur.com/" + x) { Thumb = new Images.HttpImage("http://i.imgur.com/" + x.Replace(".", "m.")) { Filename = x } });
        }

        public static async Task<IEnumerable<EXImage>> FetchAlbumNew(string url)
        {
            var m = AlbumIDPattern.Match(url);
            if (m.Success)
            {
                var id = m.Groups[1].Value;

                var req = new HttpRequestMessage(HttpMethod.Get, $"https://api.imgur.com/3/album/{id}/images");
                req.Headers.Add("Authorization", $"Client-ID {AppID}");
                var res = await Common.Http.SendAsync(req);
                var data = await res.Content.ReadAsStringAsync();
                var album = JsonConvert.DeserializeObject<ContainerModel<IList<ImageModel>>>(data);

                return album.Data
                    .Select(x => x.Link)
                    .Select(x => new HttpImage(x, x.Substring(0, x.LastIndexOf('.')) + "m" + x.Substring(x.LastIndexOf('.'))));
            }
            else
                return await FetchAlbum(url);
        }

        [Serializable]
        private class ContainerModel<T>
        {
            [JsonProperty("data")]
            public T Data;
        }

        [Serializable]
        private class AlbumModel
        {
            [JsonProperty("title")]
            public string Title;
            [JsonProperty("images")]
            public IList<ImageModel> Images;
        }

        [Serializable]
        private class ImageModel
        {
            [JsonProperty("link")]
            public string Link;
        }
    }
}

