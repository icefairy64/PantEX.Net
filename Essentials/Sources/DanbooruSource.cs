﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq;
using Breezy.PantEX.Images;
using Breezy.Assistant;

namespace Breezy.PantEX.Sources
{
    public class DanbooruSource : ImageSource
    {
        private string Url;

        private DanbooruSource(string url)
        {
            Url = url;
        }

        public DanbooruSource(ImageSourceDescriptor des)
        {
            Url = des.Descriptor;
        }

        public override string Title {
            get { return "Danbooru"; }
        }

        private EXImage CreateImage(IDictionary<string, string> dict)
        {
            var img = new HttpImage(dict["img"]);
            img.Thumb = new HttpImage(dict["thumb"]);
            img.AddTags(dict["tags"].Split(' '), false);
            var id = dict["id"];
            img.Source = new Uri($"http://danbooru.donmai.us/posts/{id}");
            return img;
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            var data = HttpSingleton.GetStringAsync(Url.Replace("{0}", (page + 1).ToString())).Result;
            var list = data.FindNamed(ImagePattern);
            return list.Select(CreateImage).ToList();
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            var data = await HttpSingleton.GetStringAsync(Url.Replace("{0}", (page + 1).ToString()));
            var list = data.FindNamed(ImagePattern);
            return list.Select(CreateImage).ToList();
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                TypeName = typeof(DanbooruSource).FullName,
                Descriptor = Url
            };
        }

        // Static members

        private static readonly string Root = "http://danbooru.donmai.us";
        private static readonly string SimpleUrl = "http://danbooru.donmai.us/posts?page={0}";
        private static readonly string SearchUrl = "http://danbooru.donmai.us/posts?tags={1}&page={0}";

        private static readonly Regex ImagePattern = new Regex(
                                                         "data-id=\"(?<id>.+?)\".+?" +
                                                         "data-tags=\"(?<tags>.*?)\".+?" +
                                                         "data-file-url=\"(?<img>.*?)\".+?" +
                                                         "data-preview-file-url=\"(?<thumb>.*?)\"", RegexOptions.Singleline);
    
        public static DanbooruSource Browse()
        {
            return new DanbooruSource(SimpleUrl);
        }

        public static DanbooruSource Search(string query)
        {
            return new DanbooruSource(SearchUrl.Replace("{1}", query));
        }
    }
}

