﻿using System;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Sources
{
    public class DanbooruSearch : ImageSourceFactory
    {
        private string Query;
        private int Page;

        public DanbooruSearch()
        {
        }

        public override void Configure()
        {
            Query = new InputDialog("Query").Ask();
            Page = int.Parse(new InputDialog("Page", "1").Ask());
        }

        public override string Provider
        {
            get
            {
                return "Danbooru";
            }
        }

        public override string Title
        {
            get
            {
                return "Search";
            }
        }

        public override ImageSource Instance
        {
            get
            {
                var src = DanbooruSource.Search(Query);
                src.Page = Page - 1;
                return src;
            }
        }
    }
}

