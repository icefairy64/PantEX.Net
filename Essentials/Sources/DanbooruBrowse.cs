﻿using System;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Sources
{
    public class DanbooruBrowse : ImageSourceFactory
    {
        private int Page;

        public DanbooruBrowse()
        {
        }

        public override void Configure()
        {
            Page = int.Parse(new InputDialog("Page", "1").Ask());
        }

        public override ImageSource Instance
        {
            get
            {
                var src = DanbooruSource.Browse();
                src.Page = Page - 1;
                return src;
            }
        }

        public override string Provider
        {
            get
            {
                return "Danbooru";
            }
        }

        public override string Title
        {
            get
            {
                return "Browse";
            }
        }
    }
}

