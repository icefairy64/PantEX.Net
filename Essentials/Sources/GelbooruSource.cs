﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Breezy.PantEX.Images;
using System.IO;
using System.Linq;
using Breezy.PantEX.GUI;
using NLog;

namespace Breezy.PantEX.Sources
{
    public class GelbooruSource : ImageSource
    {
        ILogger Logger = LogManager.GetCurrentClassLogger();

        public override string Title => "Gelbooru";

        string UrlTemplate;

        public GelbooruSource(string urlTemplate)
        {
            UrlTemplate = urlTemplate;
        }

        public GelbooruSource(ImageSourceDescriptor descriptor)
        {
            UrlTemplate = descriptor.Descriptor;
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = UrlTemplate,
                TypeName = typeof(GelbooruSource).FullName
            };
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            return LoadAsync(page).Result;
        }

        internal static EXImage CreateEXImage(GelbooruImageModel model)
        {
            var image = new HttpImage(model.FullUrl, Path.ChangeExtension(model.FullUrl.Replace($"images/{model.Directory}/", $"thumbnails/{model.Directory}/thumbnail_"), ".jpg"))
            {
                Source = new Uri($"https://gelbooru.com/index.php?page=post&s=view&id={model.Id}")
            };
            image.AddTags(model.Tags.Split(' '), false);
            return image;
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            var content = await HttpSingleton.GetStringAsync(new Uri(UrlTemplate.Replace("{page}", page.ToString())));
            var models = JsonConvert.DeserializeObject<List<GelbooruImageModel>>(content);
            return models.Select(CreateEXImage);
        }

        public static GelbooruSource Browse()
        {
            return new GelbooruSource("https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&pid={page}");
        }

        public static GelbooruSource Search(string tags)
        {
            return new GelbooruSource($"https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&pid={{page}}&tags={tags}");
        }
    }

    [Serializable]
    internal class GelbooruImageModel
    {
        [JsonProperty("source")]
        public string Source;

        [JsonProperty("image")]
        public string Filename;

        [JsonProperty("file_url")]
        public string FullUrl;

        [JsonProperty("directory")]
        public string Directory;

        [JsonProperty("tags")]
        public string Tags;

        [JsonProperty("id")]
        public int Id;
    }

    public class GelbooruBrowse : ImageSourceFactory
    {
        public override string Provider => "Gelbooru";

        public override string Title => "Browse";

        public override ImageSource Instance => GelbooruSource.Browse();

        public override void Configure()
        {
        }
    }

    public class GelbooruSearch : ImageSourceFactory
    {
        public override string Provider => "Gelbooru";

        public override string Title => "Search by tags";

        public override ImageSource Instance => GelbooruSource.Search(TagString);

        string TagString;

        public override void Configure()
        {
            var dialog = new InputDialog("Tags");
            TagString = dialog.Ask();
            if (TagString == null)
            {
                throw new Exception("No query provided");
            }
        }
    }
}
