﻿using System;
using Breezy.PantEX.GUI;
using System.Linq;
using System.Collections.Generic;
using NLog;
namespace Breezy.PantEX.Sources
{
    public class LusciousBrowse : ImageSourceFactory
    {
        private ILogger Logger = LogManager.GetCurrentClassLogger();

        private string Category;

        public LusciousBrowse()
        {
        }

        public override string Provider => "Luscious";

        public override string Title => "Browse";

        public override ImageSource Instance => LusciousSource.Browse(Category);

        public override void Configure()
        {
            Logger.Info("Configuring...");
            var dialog = new SelectorDialog<Tuple<string, string>>(LusciousSource.Categories.Select(x => new Tuple<string, string>(x.Key, x.Value)), "Choose a category");
            var result = dialog.Ask();
            Category = result.Item1;
            Logger.Info("Done");
        }
    }
}
