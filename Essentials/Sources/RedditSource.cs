﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using Breezy.Reddit;
using NLog;

namespace Breezy.PantEX.Sources
{
    public class RedditSource : ImageSource
    {
        private readonly static ILogger Logger = LogManager.GetCurrentClassLogger();

        public readonly static string[] ImageExtensions = { ".jpg", ".jpeg", ".png" };

        public override string Title
        {
            get { return "Reddit"; }
        }

        private readonly Feed Feed;

        public RedditSource(Feed feed)
        {
            Feed = feed;
        }

        public RedditSource(ImageSourceDescriptor des)
        {
            Feed = new Feed(des.Descriptor, true);
        }

        private async Task<IEnumerable<EXImage>> Grab(string url, string thumb)
        {
            var list = new List<EXImage>();
            if (thumb.Equals("default"))
                thumb = url;
            if (Path.HasExtension(url) && ImageExtensions.Any(x => x.Equals(Path.GetExtension(url))))
            {
                Logger.Trace($"Adding image [{url}] [{thumb}]");
                var img = new Images.HttpImage(url);
                try
                {
                    var thumbImg = new Images.HttpImage(thumb);
                    img.Thumb = thumbImg;
                }
                catch (Exception e)
                {
                    Logger.Debug("Could not load thumbnail [{thumb}]");
                    img.Thumb = new Images.HttpImage(url);
                }
                list.Add(img);
            }
            else
            {
                if (url.Contains("imgur.com"))
                {
                    Logger.Trace($"Querying Imgur for {url}");
                    try
                    {
                        var data = await ImgurHelper.FetchAlbumNew(url);
                        list.AddRange(data);
                        foreach (var d in data)
                            Logger.Trace($"Adding image [{d}]");
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e, $"An error occured while fetching data from Imgur ({url})");
                    }
                }
                else
                    Logger.Trace($"Skipping unknown URL {url}");
            }
            return list;
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            return Feed.Load(page)
                .Select(x => Grab(x.Url.ToString(), x.Thumbnail.ToString()).Result)
                .Aggregate(new List<EXImage>(), (IEnumerable<EXImage> a, IEnumerable<EXImage> x) => a.Concat(x));
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            var res = await Task.WhenAll((await Feed.LoadAsync(page))
                .Select(async x => await Grab(x.Url.ToString(), x.Thumbnail.ToString())));
            
            return res.Aggregate(new List<EXImage>(), (IEnumerable<EXImage> a, IEnumerable<EXImage> x) => a.Concat(x));
        }
            
        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                TypeName = typeof(RedditSource).FullName,
                Descriptor = Feed.Path
            };
        }
    }
}

