﻿using System;
using Eto.Forms;
using Breezy.Reddit;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Sources
{
    public class RedditNew : ImageSourceFactory
    {
        private Feed Feed;

        public RedditNew()
        {
        }

        public override void Configure()
        {
            var d = new InputDialog();
            d.ShowModal();
            Feed = new Feed($"/r/{d.Data}");
        }

        public override string Provider => "Reddit";
        public override string Title => "New";
        public override ImageSource Instance => new RedditSource(Feed);
    }
}

