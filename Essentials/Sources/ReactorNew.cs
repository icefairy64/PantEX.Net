﻿using System;
using Breezy.PantEX.GUI;

namespace Breezy.PantEX.Sources
{
    public class ReactorNew : ImageSourceFactory
    {
        private string Subdomain;

        public ReactorNew()
        {
        }

        public override void Configure()
        {
            Subdomain = new InputDialog("Subdomain").Ask();
        }

        public override string Provider
        {
            get
            {
                return "Reactor.cc";
            }
        }

        public override string Title
        {
            get
            {
                return "New";
            }
        }

        public override ImageSource Instance
        {
            get
            {
                return new ReactorSource(Subdomain);
            }
        }
    }
}

