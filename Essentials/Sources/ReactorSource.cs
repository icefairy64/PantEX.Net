﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Linq;
using Breezy.Assistant;
using Breezy.PantEX.Images;

namespace Breezy.PantEX.Sources
{
    public class ReactorSource : ImageSource
    {
        private readonly string RootUrl;
        private int InternalPage = -1;

        public ReactorSource(string subdomain)
        {
            RootUrl = $"http://{subdomain}.reactor.cc/";
        }

        public ReactorSource(ImageSourceDescriptor des)
        {
            RootUrl = des.Descriptor;
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            string url = InternalPage > 0 ? $"{RootUrl}/{InternalPage}" : RootUrl;
            string html = Common.Http.GetStringAsync(url).Result;
            var data = html.FindNamed(ImagePattern);

            var res = data.Select(x => new HttpImage(x["url"]) { Thumb = new HttpImage(x["preview"]) }).ToList();
            InternalPage = int.Parse(html.FindNamed(NextPagePattern).First()["page"]);

            return res;
        }

        public override Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            throw new NotImplementedException();
        }

        public override string Title
        {
            get
            {
                return "Reactor.cc";
            }
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                TypeName = typeof(ReactorSource).FullName,
                Descriptor = RootUrl
            };
        }

        // Static members

        private static Regex ImagePattern = new Regex(
            "class=\"prettyPhotoLink\" rel=\"prettyPhoto\"><img src=\"(?<preview>.+?)\".+?" +
            "\"headline\":.+?\"url\": \"(?<url>.+?)\"", RegexOptions.Singleline);
        private static Regex NextPagePattern = new Regex("<a href='/(?<page>[0-9]+?)' class='next'>");
    }
}

