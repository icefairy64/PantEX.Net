﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Breezy.PantEX.Images;
using System.Text.RegularExpressions;
using Breezy.Assistant;
using System.Linq;
using NLog;
using Newtonsoft.Json;
using System.IO;

namespace Breezy.PantEX.Sources
{
    public class LusciousSource : ImageSource
    {
        private string Uri;

        public LusciousSource(string uri)
        {
            Uri = uri;
        }

        public LusciousSource(ImageSourceDescriptor descriptor)
        {
            Uri = descriptor.Descriptor;
        }

        public override string Title => "Luscious";

        private string Convert(string thumbUri) {
            return ThumbConversionPattern.Replace(thumbUri, ".");
        }

        private EXImage CreateImage(ImageModel model)
        {
            var sourcePage = new Uri($"{Root}{model.SourcePage}");
            var img = new ExternalHttpImage(sourcePage, x => new Uri(x.Find(FullImagePattern).First().First()));
            img.Thumb = new HttpImage(model.Thumb);
            img.Filename = img.Thumb.Filename;
            img.AddTags(model.Tags.Select(x => x.Name), false);
            img.Source = sourcePage;
            return img;
        }

        private EXImage CreateImage(IDictionary<string, string> dict)
        {
            var sourcePage = new Uri($"{Root}{dict["source"]}");
            var img = new ExternalHttpImage(sourcePage, x => new Uri(x.Find(FullImagePattern).First().First()));
            img.Thumb = new HttpImage(dict["thumb"]);
            img.AddTags(dict["tags"].Find(TagPattern).Select(x => x.First()), false);
            img.Source = sourcePage;
            return img;
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = Uri,
                TypeName = typeof(LusciousSource).FullName
            };
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            var data = HttpSingleton.GetStringAsync(Uri.Replace("{0}", (page + 1).ToString())).Result;
            var json = data.Find(JsonPattern).First().First();
            var obj = JsonConvert.DeserializeObject<JsonModel>(json);
            return obj.Images.Select(CreateImage).ToList();
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            var data = await HttpSingleton.GetStringAsync(Uri.Replace("{0}", (page + 1).ToString()));
            var json = data.Find(JsonPattern).First().First();
            var obj = JsonConvert.DeserializeObject<JsonModel>(json);
            return obj.Images.Select(CreateImage).ToList();
        }

        // Static members

        private static readonly string Root = "http://luscious.net";
        private static readonly string SimpleUrl = "http://luscious.net/c/{1}/pictures/page/{0}/?style=thumbnails";
        private static readonly string AlbumUrl = "http://luscious.net/c/{1}/pictures/album/{2}/page/{0}/?style=thumbnails";

        private static readonly Regex LookupPattern = new Regex("tags\": \\[(?<tags>.+?)\\].+?\"view_page_url\": \"(?<source>.+?)\".+?\", \"small\": \"(?<thumb>.+?)\"", RegexOptions.Singleline);
        private static readonly Regex TagPattern = new Regex("\"name\": \"(.+?)\"", RegexOptions.Singleline);
        private static readonly Regex FullImagePattern = new Regex("a href=\"(.+?)\" class=\"icon-download\"");
        private static readonly Regex ThumbConversionPattern = new Regex("\\.\\d.+?x\\d.+?\\.");
        private static readonly Regex JsonPattern = new Regex("window.app.picture_listing = new window.Shana.PictureListingController\\((.+?)\\);", RegexOptions.Singleline);

        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public static readonly IDictionary<string, string> Categories;

        static LusciousSource() {
            var tmp = new Dictionary<string, string>();
            tmp.Add("hentai_manga", "Hentai manga");
            tmp.Add("hentai", "Hentai");
            tmp.Add("adult", "Adult");
            tmp.Add("western_manga", "Western manga");
            tmp.Add("furries", "Furries");
            tmp.Add("futanari_manga", "Futanari manga");
            tmp.Add("western_hentai", "Western hentai");
            tmp.Add("furry_comics", "Furry comics");
            tmp.Add("futanari", "Futanari hentai");
            tmp.Add("video_games", "Video games");
            tmp.Add("superhero_manga", "Superhero manga");
            tmp.Add("superheroes", "Superheroes");
            tmp.Add("3d_hentai", "3D hentai");
            tmp.Add("tentacle_hentai", "Tentacles");
            tmp.Add("yuri_manga", "Yuri manga");
            tmp.Add("monster_girls", "Monster girls");
            tmp.Add("incest_manga", "Incest manga");
            tmp.Add("cosplay", "Cosplay");
            tmp.Add("shemales", "Shemales");
            tmp.Add("cumshot", "Cumshot");
            tmp.Add("ethnic_girls", "Ethnic girls");
            tmp.Add("luscious", "Other");
            tmp.Add("bdsm", "BDSM");
            tmp.Add("bbw", "BBW");
            tmp.Add("trap_manga", "Trap manga");
            tmp.Add("my_little_pony_fim", "My Little Pony: FIM");
            tmp.Add("lesbian_pictures", "Lesbians");
            tmp.Add("traps", "Traps");
            tmp.Add("yaoi", "Yaoi");
            tmp.Add("erotica", "Erotica");
            tmp.Add("monster_girl_manga", "Monster girls manga");
            tmp.Add("video_game_manga", "Video game manga");
            tmp.Add("interracial", "Interracial");
            tmp.Add("hentai_movies", "Hentai movies");
            tmp.Add("pregnant", "Pregnant");
            tmp.Add("milfs", "MILFs");
            tmp.Add("cyber_sex", "Cyber sex");
            tmp.Add("roleplaying", "Roleplaying");
            tmp.Add("luscious_artists", "Luscious artists");
            tmp.Add("verified_amateurs", "Selfies");
            tmp.Add("water_sports", "Water sports");
            tmp.Add("interracial_comics", "Interracial comics");
            tmp.Add("celebrity_fakes", "Celebrity fakes");
            tmp.Add("fan_fiction", "Fan fiction");
            tmp.Add("my_little_pony_manga", "My Little Pony manga");
            tmp.Add("muscular_babes", "Myscular babes");
            tmp.Add("foot_fetish", "Foot fetish");
            tmp.Add("men", "Men");
            foreach (var entry in tmp)
                Logger.Info($"Adding category: {entry.Value} ({entry.Key})");
            Categories = tmp;
        }

        public static LusciousSource Browse(string category)
        {
            return new LusciousSource(SimpleUrl.Replace("{1}", category));
        }

        public static LusciousSource Album(string category, string album)
        {
            return new LusciousSource(AlbumUrl.Replace("{1}", category).Replace("{2}", album));
        }
    }

    [Serializable]
    class JsonModel
    {
        [JsonProperty("documents")]
        public List<ImageModel> Images;
    }

    [Serializable]
    class ImageModel
    {
        [JsonProperty("small")]
        public string Thumb;

        [JsonProperty("view_page_url")]
        public string SourcePage;

        [JsonProperty("tags")]
        public List<TagModel> Tags;
    }

    [Serializable]
    class TagModel
    {
        [JsonProperty("name")]
        public string Name;
    }
}
