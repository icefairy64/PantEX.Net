﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Breezy.PantEX.Images;
using System.Net.Http;

namespace Breezy.PantEX.Sources
{
    public abstract class AbstractDanbooruSource : ImageSource
    {
        protected readonly string Descriptor;
        protected readonly string RootUrl;
        protected readonly string FullFieldName;
        protected readonly string ThumbFieldName;
        protected readonly string TagsFieldName;
        protected readonly string IdFieldName;

        public AbstractDanbooruSource(string descriptor, string root, string fullField = "file_url", string thumbField = "preview_url", string tagsField = "tags", string idField = "id")
        {
            Descriptor = descriptor;
            RootUrl = root;
            FullFieldName = fullField;
            ThumbFieldName = thumbField;
            TagsFieldName = tagsField;
            IdFieldName = idField;
        }

        private IEnumerable<EXImage> Parse(string json)
        {
            var objects = JsonConvert.DeserializeObject<IList<IDictionary<string, object>>>(json);
            return objects.Select(x =>
                {
                    var id = x[IdFieldName] as string;
                    var img = new HttpImage(() => CreateRequest(x[FullFieldName] as string, $"{RootUrl}/post/show/{id}"), x[ThumbFieldName] as string);
                    img.AddTags((x[TagsFieldName] as string).Split(' ').Select(z => Tag.Create(z)), false);
                    return img;
                });
        }

        private HttpRequestMessage CreateRequest(string url, string referer)
        {
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Referrer = new Uri(referer);
            return req;
        }

        public override IEnumerable<EXImage> Load(int page)
        {
            var data = Common.Http.GetStringAsync(Descriptor.Replace("{0}", page.ToString())).Result;
            return Parse(data);
        }

        public override async Task<IEnumerable<EXImage>> LoadAsync(int page)
        {
            var data = await Common.Http.GetStringAsync(Descriptor.Replace("{0}", page.ToString()));
            return Parse(data);
        }

        public override ImageSourceDescriptor CreateDescriptor()
        {
            return new ImageSourceDescriptor
            {
                Descriptor = this.Descriptor,
                TypeName = typeof(AbstractDanbooruSource).FullName
            };
        }

        public override string Title => "Generic Danbooru";
    }
}

