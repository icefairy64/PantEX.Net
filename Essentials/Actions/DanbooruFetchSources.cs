﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Breezy.PantEX.GUI;
using Breezy.Assistant;
using Breezy.Assistant.Extensions;
using Breezy.PantEX.Sources;
using System.IO;
using Eto.Forms;

namespace Breezy.PantEX.Actions
{
    [Named("Danbooru: fetch sources")]
    public class DanbooruFetchSources : ICollectionAction
    {
        private Collection TargetCollection;

        public DanbooruFetchSources()
        {
        }

        public bool Configure(Collection collection)
        {
            TargetCollection = collection;
            return TargetCollection != null;
        }

        public async Task Perform(ProgressHandler progressHandler)
        {
            var iter = TargetCollection.Images.Where(x => x.Source == null);
            double c = iter.Count();
            int i = 0;
            foreach (var img in iter)
            {
                var md5 = Path.GetFileNameWithoutExtension(img.Filename).Right(32);
                var src = DanbooruSource.Search($"md5:{md5}");
                var remoteImg = (await src.LoadAsync(0)).FirstOrDefault();
                img.Source = remoteImg?.Source;

                if (this.IsOnUIThread())
                    progressHandler?.Invoke(i++ / c);
                else
                    Application.Instance.Invoke(() => progressHandler?.Invoke(i++ / c));
            }
        }

        public string Title
        {
            get
            {
                return $"Fetching image sources for collection {TargetCollection}";
            }
        }
    }
}

