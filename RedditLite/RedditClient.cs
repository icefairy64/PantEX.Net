﻿using System;

namespace Breezy.Reddit
{
    public static class RedditClient
    {
        public static SubredditFeedBuilder Subreddit(string name) => new SubredditFeedBuilder(name);
    }

    public class SubredditFeedBuilder
    {
        private readonly string Path;

        public SubredditFeedBuilder(string name)
        {
            Path = $"/r/{name}/";
        }

        public Feed Hot => new Feed(Path);
        public Feed New => new Feed(Path + "new/");
        public Feed Top => new Feed(Path + "top/");
        public Feed Rising => new Feed(Path + "rising/");
        public Feed Controversial => new Feed(Path + "controversial/");
    }
}

