﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Linq;
using Breezy.Assistant;
using Newtonsoft.Json;
using NLog;

namespace Breezy.Reddit
{
    public class Feed : PageIterator<PostData>
    {
        private readonly static ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly static JsonSerializer Json = new JsonSerializer();
        private readonly static string Root = "https://www.reddit.com";
        private readonly static HttpClient Http = new HttpClient();

        private string After;

        public string Path { get; private set; }

        public Feed(string path, bool raw = false)
        {
            Path = raw ? path : Root + path + ".json";
        }

        private IEnumerable<PostData> Load(string content)
        {
            var z = After.OrDefault("?after={0}", "");
            Logger.Trace($"Loading page {Path}{z}");
            using (var sr = new StringReader(content))
                using (var jr = new JsonTextReader(sr))
                {
                    var data = Json.Deserialize<Response>(jr);
                    After = data.Data.After;
                    return data.Data.Posts.Select(x => x.Data);
                }
        }

        public override IEnumerable<PostData> Load(int page)
        {
            return Load(Http.GetStringAsync(Path + After.OrDefault("?after={0}", "")).Result);
        }

        public override async Task<IEnumerable<PostData>> LoadAsync(int page)
        {
            return Load(await Http.GetStringAsync(Path + After.OrDefault("?after={0}", "")));
        }
    }

    [Serializable]
    internal class Response
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("data")]
        public ResponseData Data { get; set; }
    }

    [Serializable]
    internal class ResponseData
    {
        [JsonProperty("children")]
        public IEnumerable<Post> Posts { get; set; }
        [JsonProperty("after")]
        public string After { get; set; }
    }

    [Serializable]
    internal class Post
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        [JsonProperty("data")]
        public PostData Data { get; set; }
    }

    [Serializable]
    public class PostData
    {
        [JsonProperty("url")]
        public Uri Url { get; set; }
        [JsonProperty("thumbnail")]
        public Uri Thumbnail { get; set; }
        [JsonProperty("is_self")]
        public bool IsSelf { get; set; }
    }
}

